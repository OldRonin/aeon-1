package net.minecraft.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

public class NBTTagIntArray extends NBTBase
{
	/** The array of saved integers */
	private int[] intArray;
	private static final String __OBFID = "CL_00001221";

	NBTTagIntArray()
	{
	}

	public NBTTagIntArray(int[] p_i45132_1_)
	{
		this.intArray = p_i45132_1_;
	}

	/**
	 * Write the actual data contents of the tag, implemented in NBT extension
	 * classes
	 */
	@Override
	void write(DataOutput p_74734_1_) throws IOException
	{
		p_74734_1_.writeInt(this.intArray.length);
		for (final int element : this.intArray)
		{
			p_74734_1_.writeInt(element);
		}
	}

	@Override
	void func_152446_a(DataInput p_152446_1_, int p_152446_2_, NBTSizeTracker p_152446_3_) throws IOException
	{
		final int var4 = p_152446_1_.readInt();
		p_152446_3_.func_152450_a(32 * var4);
		this.intArray = new int[var4];
		for (int var5 = 0; var5 < var4; ++var5)
		{
			this.intArray[var5] = p_152446_1_.readInt();
		}
	}

	/**
	 * Gets the type byte for the tag.
	 */
	@Override
	 public byte getId()
	{
		return (byte) 11;
	}

	@Override
	 public String toString()
	{
		String var1 = "[";
		final int[] var2 = this.intArray;
		final int var3 = var2.length;
		for (int var4 = 0; var4 < var3; ++var4)
		{
			final int var5 = var2[var4];
			var1 = var1 + var5 + ",";
		}
		return var1 + "]";
	}

	/**
	 * Creates a clone of the tag.
	 */
	@Override
	 public NBTBase copy()
	{
		final int[] var1 = new int[this.intArray.length];
		System.arraycopy(this.intArray, 0, var1, 0, this.intArray.length);
		return new NBTTagIntArray(var1);
	}

	@Override
	 public boolean equals(Object p_equals_1_)
	{
		return super.equals(p_equals_1_) ? Arrays.equals(this.intArray, ((NBTTagIntArray) p_equals_1_).intArray) : false;
	}

	@Override
	 public int hashCode()
	{
		return super.hashCode() ^ Arrays.hashCode(this.intArray);
	}

	public int[] func_150302_c()
	{
		return this.intArray;
	}
}
