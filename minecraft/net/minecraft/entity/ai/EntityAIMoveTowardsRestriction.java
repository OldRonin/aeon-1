package net.minecraft.entity.ai;

import net.minecraft.entity.EntityCreature;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.Vec3;

public class EntityAIMoveTowardsRestriction extends EntityAIBase
{
	private final EntityCreature theEntity;
	private double movePosX;
	private double movePosY;
	private double movePosZ;
	private final double movementSpeed;
	private static final String __OBFID = "CL_00001598";

	public EntityAIMoveTowardsRestriction(EntityCreature p_i2347_1_, double p_i2347_2_)
	{
		this.theEntity = p_i2347_1_;
		this.movementSpeed = p_i2347_2_;
		this.setMutexBits(1);
	}

	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	@Override
	public boolean shouldExecute()
	{
		if (this.theEntity.isWithinHomeDistanceCurrentPosition())
		{
			return false;
		} else
		{
			final ChunkCoordinates var1 = this.theEntity.getHomePosition();
			final Vec3 var2 = RandomPositionGenerator.findRandomTargetBlockTowards(this.theEntity, 16, 7, Vec3.createVectorHelper(var1.posX, var1.posY, var1.posZ));
			if (var2 == null)
			{
				return false;
			} else
			{
				this.movePosX = var2.xCoord;
				this.movePosY = var2.yCoord;
				this.movePosZ = var2.zCoord;
				return true;
			}
		}
	}

	/**
	 * Returns whether an in-progress EntityAIBase should continue executing
	 */
	@Override
	public boolean continueExecuting()
	{
		return !this.theEntity.getNavigator().noPath();
	}

	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	@Override
	public void startExecuting()
	{
		this.theEntity.getNavigator().tryMoveToXYZ(this.movePosX, this.movePosY, this.movePosZ, this.movementSpeed);
	}
}
