package net.minecraft.entity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryMerchant;
import net.minecraft.item.ItemStack;
import net.minecraft.village.MerchantRecipe;
import net.minecraft.village.MerchantRecipeList;

public class NpcMerchant implements IMerchant
{
	/** Instance of Merchants Inventory. */
	private final InventoryMerchant theMerchantInventory;
	/** This merchant's current player customer. */
	private final EntityPlayer customer;
	/** The MerchantRecipeList instance. */
	private MerchantRecipeList recipeList;
	private static final String __OBFID = "CL_00001705";

	public NpcMerchant(EntityPlayer p_i1746_1_)
	{
		this.customer = p_i1746_1_;
		this.theMerchantInventory = new InventoryMerchant(p_i1746_1_, this);
	}

	@Override
	public EntityPlayer getCustomer()
	{
		return this.customer;
	}

	@Override
	public void setCustomer(EntityPlayer p_70932_1_)
	{
	}

	@Override
	public MerchantRecipeList getRecipes(EntityPlayer p_70934_1_)
	{
		return this.recipeList;
	}

	@Override
	public void setRecipes(MerchantRecipeList p_70930_1_)
	{
		this.recipeList = p_70930_1_;
	}

	@Override
	public void useRecipe(MerchantRecipe p_70933_1_)
	{
	}

	@Override
	public void func_110297_a_(ItemStack p_110297_1_)
	{
	}
}
