package net.minecraft.network;

import io.netty.util.concurrent.GenericFutureListener;

public class InboundHandlerTuplePacketListener
{
	public final Packet field_150774_a;
	public final GenericFutureListener[] field_150773_b;
	private static final String __OBFID = "CL_00001244";
	
	//Used to be public static class in NetworkManager.
	public InboundHandlerTuplePacketListener(Packet p_i45146_1_, GenericFutureListener... p_i45146_2_)
	{
		this.field_150774_a = p_i45146_1_;
		this.field_150773_b = p_i45146_2_;
	}
}
