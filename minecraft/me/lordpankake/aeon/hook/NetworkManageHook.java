package me.lordpankake.aeon.hook;

import java.net.InetAddress;
import java.net.SocketAddress;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelException;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.local.LocalChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.util.concurrent.GenericFutureListener;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.eventapi.EventManager;
import net.minecraft.network.EnumConnectionState;
import net.minecraft.network.InboundHandlerTuplePacketListener;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.util.MessageDeserializer;
import net.minecraft.util.MessageDeserializer2;
import net.minecraft.util.MessageSerializer;
import net.minecraft.util.MessageSerializer2;

public class NetworkManageHook extends NetworkManager
{
	public NetworkManageHook(boolean bool)
	{
		super(bool);
	}


	public void scheduleOutboundPacket(Packet pack, GenericFutureListener... gfl)
	{
		EventManager.eventPacketSend.call(pack);
		if (EventManager.eventPacketSend.storedPacket != null)
		{
			if (this.channel != null && this.channel.isOpen())
			{
				this.flushOutboundQueue();
				this.dispatchPacket(EventManager.eventPacketSend.storedPacket, gfl);
			} else
			{
				this.outboundPacketsQueue.add(new InboundHandlerTuplePacketListener(EventManager.eventPacketSend.storedPacket, gfl));
			}
		}
	}

	public void processReceivedPackets()
	{
		this.flushOutboundQueue();
		final EnumConnectionState connectionState = (EnumConnectionState) this.channel.attr(attrKeyConnectionState).get();
		if (this.connectionState != connectionState)
		{
			if (this.connectionState != null)
			{
				this.netHandler.onConnectionStateTransition(this.connectionState, connectionState);
			}
			this.connectionState = connectionState;
		}
		if (this.netHandler != null)
		{
			for (int i = 1000; !this.receivedPacketsQueue.isEmpty() && i >= 0; --i)
			{
				final Packet pack = (Packet) this.receivedPacketsQueue.poll();
				EventManager.eventPacketReceive.call(pack);
				if (EventManager.eventPacketReceive.storedPacket != null)
				{
					try
					{
						EventManager.eventPacketReceive.storedPacket.processPacket(this.netHandler);
					}
					catch(Exception e){}
				}
			}
			this.netHandler.onNetworkTick();
		}
		this.channel.flush();
	}
	
	/**
	 * Prepares a clientside NetworkManager: establishes a connection to the
	 * socket supplied and configures the channel pipeline. Returns the newly
	 * created instance.
	 */
	public static NetworkManageHook provideLocalClient(SocketAddress p_150722_0_)
	{
		final NetworkManageHook var1 = new NetworkManageHook(true);
		new Bootstrap().group(eventLoops).handler(new ChannelInitializer()
		{
			private static final String __OBFID = "CL_00001243";

			@Override
			protected void initChannel(Channel p_initChannel_1_)
			{
				p_initChannel_1_.pipeline().addLast("packet_handler", var1);
			}
		}).channel(LocalChannel.class).connect(p_150722_0_).syncUninterruptibly();
		return var1;
	}
	
	/**
	 * Prepares a clientside NetworkManager: establishes a connection to the
	 * address and port supplied and configures the channel pipeline. Returns
	 * the newly created instance.
	 */
	public static NetworkManageHook provideLanClient(InetAddress p_150726_0_, int p_150726_1_)
	{
		final NetworkManageHook var2 = new NetworkManageHook(true);
		new Bootstrap().group(eventLoops).handler(new ChannelInitializer()
		{
			private static final String __OBFID = "CL_00001242";

			@Override
			protected void initChannel(Channel p_initChannel_1_)
			{
				try
				{
					p_initChannel_1_.config().setOption(ChannelOption.IP_TOS, Integer.valueOf(24));
				} catch (final ChannelException var4)
				{
					;
				}
				try
				{
					p_initChannel_1_.config().setOption(ChannelOption.TCP_NODELAY, Boolean.valueOf(false));
				} catch (final ChannelException var3)
				{
					;
				}
				p_initChannel_1_.pipeline().addLast("timeout", new ReadTimeoutHandler(20)).addLast("splitter", new MessageDeserializer2()).addLast("decoder", new MessageDeserializer(NetworkManager.netStats)).addLast("prepender", new MessageSerializer2()).addLast("encoder", new MessageSerializer(NetworkManager.netStats)).addLast("packet_handler", var2);
			}
		}).channel(NioSocketChannel.class).connect(p_150726_0_, p_150726_1_).syncUninterruptibly();
		return var2;
	}
}
