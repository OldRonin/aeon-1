package me.lordpankake.aeon.hook;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.eventapi.EventManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.Entity;
import net.minecraft.stats.StatFileWriter;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Session;
import net.minecraft.world.World;

public class EntityClientPlayerMPHook extends EntityClientPlayerMP
{
	public EntityClientPlayerMPHook(Minecraft mc, World world, Session session, NetHandlerPlayClient netHandlerPlayClient, StatFileWriter statFileWriter)
	{
		super(mc, world, session, netHandlerPlayClient, statFileWriter);
	}

	@Override
	public void moveEntity(double motionX, double motionY, double motionZ)
	{
		EventManager.eventMove.call(motionX, motionY, motionZ);
		super.moveEntity(EventManager.eventMove.motionX, EventManager.eventMove.motionY, EventManager.eventMove.motionZ);
	}

	@Override
	public boolean isEntityInsideOpaqueBlock()
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().freecam.getState() || Aeon.getInstance().getUtils().getHack().getLoader().phase.getState())
		{
			return false;
		}
		return super.isEntityInsideOpaqueBlock();
	}

	@Override
	public boolean handleWaterMovement()
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().noslowdown.getState())
		{
			return false;
		}
		return super.handleWaterMovement();
	}

	@Override
	public boolean handleLavaMovement()
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().noslowdown.getState())
		{
			return false;
		}
		return super.handleLavaMovement();
	}

	@Override
	public boolean isInWater()
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().noslowdown.getState())
		{
			return false;
		}
		return super.isInWater();
	}

	@Override
	public boolean isPushedByWater()
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().noslowdown.getState())
		{
			return false;
		}
		return super.isPushedByWater();
	}

	@Override
	protected boolean pushOutOfBlocks(double p_145771_1_, double p_145771_3_, double p_145771_5_)
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().freecam.getState() && Aeon.getInstance().getUtils().getHack().getLoader().flight.getState())
		{
			return false;
		}
		if (Aeon.getInstance().getUtils().getHack().getLoader().phase.getState() )
		{
			return false;
		}
		return super.pushOutOfBlocks(p_145771_1_, p_145771_3_, p_145771_5_);
	}

	@Override
	public void sendChatMessage(String message)
	{
		if (message.startsWith("-"))
		{
			if (message.startsWith("-say "))
			{
				super.sendChatMessage(message.substring(5));
			} else
			{
				EventManager.eventCommand.call(message, "Aeon Client");
			}
		} else
		{
			EventManager.eventSendMessage.call(message);
			super.sendChatMessage(EventManager.eventSendMessage.message);
		}
	}

	@Override
	public void sendMotionUpdates()
	{
		final float oldRotationYaw = this.rotationYaw;
		final float oldRotationPitch = this.rotationPitch;
		EventManager.eventPreTick.call();
		if (!Aeon.getInstance().getUtils().getHack().getLoader().freecam.getState() && !EventManager.eventPreTick.isCancelled())
		{
			super.sendMotionUpdates();
		}
		this.rotationYaw = oldRotationYaw;
		this.rotationPitch = oldRotationPitch;
		EventManager.eventPostTick.call();
	}

	@Override
	public void addVelocity(double velocityX, double velocityY, double velocityZ)
	{
		EventManager.eventAddVelocity.call(velocityX, velocityY, velocityZ);
		if (Aeon.getInstance().getUtils().getHack().getLoader().antivelocity.getState())
		{
			super.addVelocity(0, 0, 0);
		} else
		{
			super.addVelocity(EventManager.eventAddVelocity.velocityX, EventManager.eventAddVelocity.velocityY, EventManager.eventAddVelocity.velocityZ);
		}
	}

	@Override
	public void setVelocity(double velocityX, double velocityY, double velocityZ)
	{
		EventManager.eventSetVelocity.call(velocityX, velocityY, velocityZ);
		super.setVelocity(EventManager.eventSetVelocity.velocityX, EventManager.eventSetVelocity.velocityY, EventManager.eventSetVelocity.velocityZ);
	}

	@Override
	public float getDistanceToEntity(Entity entity)
	{
		final float xDiff = (float) (this.posX - entity.posX);
		final float yDiff = (float) (this.posY + getEyeHeight() - (entity.posY + entity.getEyeHeight()));
		final float zDiff = (float) (this.posZ - entity.posZ);
		return MathHelper.sqrt_float(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
	}

	@Override
	public void setHealth(float health)
	{
		EventManager.eventHealthChange.call(health);
		super.setHealth(health);
	}

	@Override
	public boolean isInRangeToRender3d(double x, double y, double z)
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().norender.getState())
		{
			return false;
		}
		return super.isInRangeToRender3d(x, y, z);
	}

	@Override
	public boolean isInRangeToRenderDist(double dist)
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().norender.getState())
		{
			return false;
		}
		return super.isInRangeToRenderDist(dist);
	}
}
