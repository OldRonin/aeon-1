package me.lordpankake.aeon.hook;

import me.lordpankake.aeon.core.Aeon;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MovingObjectPosition;

public class RenderGlobalHook extends RenderGlobal
{
	public RenderGlobalHook(Minecraft par1Minecraft)
	{
		super(par1Minecraft);
	}

	@Override
	public void renderStars()
	{
		if (!Aeon.getInstance().getUtils().getHack().getLoader().norender.getState())
		{
			super.renderStars();
		}
	}

	@Override
	public void renderSky(float par1)
	{
		if (!Aeon.getInstance().getUtils().getHack().getLoader().norender.getState())
		{
			super.renderSky(par1);
		}
	}

	@Override
	public void renderClouds(float par1)
	{
		if (!Aeon.getInstance().getUtils().getHack().getLoader().norender.getState())
		{
			super.renderClouds(par1);
		}
	}

	@Override
	public void renderCloudsFancy(float par1)
	{
		if (!Aeon.getInstance().getUtils().getHack().getLoader().norender.getState())
		{
			super.renderCloudsFancy(par1);
		}
	}

	@Override
	public void drawSelectionBox(EntityPlayer par1EntityPlayer, MovingObjectPosition par2MovingObjectPosition, int par3, float par4)
	{
		if (!Aeon.getInstance().getUtils().getHack().getLoader().norender.getState() && !Aeon.getInstance().getUtils().getHack().getLoader().blockoverlay.getState())
		{
			super.drawSelectionBox(par1EntityPlayer, par2MovingObjectPosition, par3, par4);
		}
	}

	@Override
	public void spawnParticle(String par1Str, final double par2, final double par4, final double par6, double par8, double par10, double par12)
	{
		if (!Aeon.getInstance().getUtils().getHack().getLoader().norender.getState())
		{
			super.spawnParticle(par1Str, par2, par4, par6, par8, par10, par12);
		}
	}
}
