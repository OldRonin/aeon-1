package me.lordpankake.aeon.hook;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.eventapi.EventManager;
import me.lordpankake.aeon.modules.mod.player.Fastmine;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatFileWriter;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class PlayerControllerMPHook extends PlayerControllerMP
{
	private final Minecraft mc;
	private final NetHandlerPlayClient netClientHandler;

	public PlayerControllerMPHook(Minecraft minecraft, NetHandlerPlayClient netClientHandler)
	{
		super(minecraft, netClientHandler);
		this.mc = minecraft;
		this.netClientHandler = netClientHandler;
		Log.write("Initialized new " + getClass().getSimpleName() + ".");
	}

	@Override
	public void onPlayerDamageBlock(int x, int y, int z, int side)
	{
		EventManager.eventBlockDamage.call(x, y, z, side, this.curBlockDamageMP);
		super.onPlayerDamageBlock(EventManager.eventBlockDamage.x, EventManager.eventBlockDamage.y, EventManager.eventBlockDamage.z, EventManager.eventBlockDamage.side);
		boolean shouldReplaceHitDelay = false;
		final Block block = this.mc.theWorld.getBlock(x, y, z);
		if (Aeon.getInstance().getUtils().getHack().getLoader().fastmine.getState())
		{
			if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
			{
				this.curBlockDamageMP += block.getPlayerRelativeBlockHardness(this.mc.thePlayer, this.mc.thePlayer.worldObj, x, y, z) / 10;
			} else
			{
				if (Fastmine.mode.contains("auto"))
				{
					this.curBlockDamageMP += block.getPlayerRelativeBlockHardness(this.mc.thePlayer, this.mc.thePlayer.worldObj, x, y, z);
				} else
				{
					this.curBlockDamageMP += Fastmine.speed;
				}
			}
			if (Fastmine.mode.contains("delay"))
			{
				this.blockHitDelay = 1;
				shouldReplaceHitDelay = true;
			}
		}
		if (shouldReplaceHitDelay)
		{
			this.blockHitDelay = 0;
		}
	}

	@Override
	public void clickBlock(int x, int y, int z, int side)
	{
		EventManager.eventBlockClick.call(x, y, z, side);
		super.clickBlock(EventManager.eventBlockClick.x, EventManager.eventBlockClick.y, EventManager.eventBlockClick.z, EventManager.eventBlockClick.side);
	}
	
	@Override
	public void clickBlockCreative(Minecraft mc, PlayerControllerMP player, int x, int y, int z, int side)
	{
		super.clickBlockCreative(mc, player, x, y, z, side);
		EventManager.eventBlockDestroy.call(x, y, z);
	}

	@Override
	public void attackEntity(EntityPlayer player, Entity entity)
	{
		EventManager.eventEntityHit.call(entity);
		if (!EventManager.eventEntityHit.isCancelled())
		{
			super.attackEntity(player, EventManager.eventEntityHit.entity);
		}
		EventManager.eventPostHit.call(entity);
		// if (entity.isDead) {new EventKillEntity(entity)}
	}

	@Override
	public boolean onPlayerRightClick(EntityPlayer par1EntityPlayer, World par2World, ItemStack par3ItemStack, int x, int y, int z, int side, Vec3 par8Vec3)
	{
		EventManager.eventBlockPlace.call(x, y, z, side);
		if (!EventManager.eventBlockPlace.isCancelled())
		{
			return super.onPlayerRightClick(par1EntityPlayer, par2World, par3ItemStack, EventManager.eventBlockPlace.x, EventManager.eventBlockPlace.y, EventManager.eventBlockPlace.z, EventManager.eventBlockPlace.side, par8Vec3);
		} else
		{
			return false;
		}
	}

	@Override
	public PlayerHook func_147493_a(World world, StatFileWriter statFileWriter)
	{
		return new PlayerHook(this.mc, world, this.mc.getSession(), this.netClientHandler, statFileWriter);
	}
}