package me.lordpankake.aeon.hook;

import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.profiler.Profiler;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.WorldSettings;

public class WorldClientHook extends WorldClient
{
	public WorldClientHook(NetHandlerPlayClient nhpc, WorldSettings ws, int i, EnumDifficulty ed, Profiler prof)
	{
		super(nhpc, ws, i, ed, prof);
	}
}
