package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;
import net.minecraft.entity.projectile.EntityFishHook;

public class AutoFish extends Module
{
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();
	@AIgnore
	private boolean castNextTick;

	public AutoFish(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (!this.timer.check(750.0F))
		{
			return;
		}
		if (this.castNextTick)
		{
			grabRod();
			mc.onUseItem();
			this.castNextTick = false;
		} else if (mc.thePlayer.fishEntity != null)
		{
			if (isHooked(mc.thePlayer.fishEntity))
			{
				mc.onUseItem();
				this.castNextTick = true;
				this.timer.reset();
			}
		}
	}

	private boolean isHooked(EntityFishHook hook)
	{
		return hook.motionX == 0.0D && hook.motionZ == 0.0D && hook.motionY != 0.0D;
	}

	private void grabRod()
	{
		final int rodSlot = Aeon.getInstance().getUtils().getInventory().findHotbarItem(346);
		if (rodSlot != -1)
		{
			mc.thePlayer.inventory.currentItem = rodSlot;
			mc.playerController.updateController();
		}
	}
}
