package me.lordpankake.aeon.modules.mod.player;

import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import net.minecraft.util.Direction;
import net.minecraft.util.MathHelper;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPostTick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreTick;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;

public class Phase extends Module
{
	@AIgnore
	private double state;
	@AIgnore
	private boolean phasing = false;

	public Phase(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onPreTick(EventPreTick event)
	{
		double x = mc.thePlayer.posX;
		double bby = mc.thePlayer.boundingBox.minY;
		double y = mc.thePlayer.posY;
		double z = mc.thePlayer.posZ;
		float yaw = mc.thePlayer.rotationYaw;
		float pitch = mc.thePlayer.rotationPitch;
		int dirFace = MathHelper.floor_double((double) (mc.thePlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
		if (mc.thePlayer.isCollidedHorizontally && mc.thePlayer.onGround && !mc.thePlayer.isInWater())
		{
			mc.thePlayer.motionY = 0.0D;
			this.phasing = true;
			++this.state;
			if (this.state == 2.0D)
			{
				mc.thePlayer.noClip = true;
				for (int i = 0; i < 3; ++i)
				{
					mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, 2));
					mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, 1));
					mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, 2));
					mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, 1));
				}
				mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, bby, y, z, true));
				mc.thePlayer.setPosition(x + (double) Direction.offsetX[dirFace] * 0.4425D, y, z + (double) Direction.offsetZ[dirFace] * 0.4425D);
				if (!this.isMoving())
				{
					mc.thePlayer.setPosition(x + (double) Direction.offsetX[dirFace] * 0.44D, y, z + (double) Direction.offsetZ[dirFace] * 0.44D);
				}
			} else if (this.state >= 4.5D)
			{
				mc.thePlayer.noClip = false;
				mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, bby + 1.0D, y + 1.0D, z, false));
				mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, bby - 1.0D, y - 1.0D, z, true));
				mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, bby, y + 0.005D, z, false));
				mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, bby, y, z, true));
				this.state = 0.0D;
			}
		} else
		{
			mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, 2));
			this.phasing = false;
		}
	}

	private boolean isMoving()
	{
		return mc.thePlayer.motionX != 0.0D || mc.thePlayer.motionY != 0.0D || mc.thePlayer.motionZ != 0.0D;
	}
}
