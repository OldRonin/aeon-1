package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.IncludeGUI;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import net.minecraft.client.settings.GameSettings;

public class Flight extends Module
{
	@IncludeGUI(min = 1F, max = 10F)
	public static float speed = 3;

	public Flight(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}
	
	@Override
	public void onDisable()
	{
		mc.thePlayer.capabilities.isFlying = false;
	}

	@EventTarget
	@Override
	public void onGameTick(EventTick event)
	{
		handleFlight();
	}

	public void handleFlight()
	{
		if (mc.thePlayer.isInWater())
		{
			mc.thePlayer.capabilities.isFlying = true;
		} else
		{
			mc.thePlayer.capabilities.isFlying = false;
			mc.thePlayer.landMovementFactor = 0.5f;
			mc.thePlayer.jumpMovementFactor = 0.5f;
			mc.thePlayer.setSneaking(false);
			mc.thePlayer.motionX = 0;
			mc.thePlayer.motionY = 0;
			mc.thePlayer.motionZ = 0;
			if (mc.currentScreen == null)
			{
				if (GameSettings.isKeyDown(mc.gameSettings.keyBindJump))
				{
					mc.thePlayer.motionY = speed / 3;
				}
				if (GameSettings.isKeyDown(mc.gameSettings.keyBindSneak))
				{
					mc.thePlayer.motionY = -speed / 3;
				}
			}
			mc.thePlayer.jumpMovementFactor *= speed;
		}
	}
}
