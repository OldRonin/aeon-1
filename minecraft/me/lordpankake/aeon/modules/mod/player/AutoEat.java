package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.InventoryUtils;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class AutoEat extends Module
{
	@AIgnore
	private int oldItem = -1;
	@AIgnore
	private boolean eatPress, savedUseItem;
	public static int foodLevel = 17;

	public AutoEat(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		final InventoryUtils util = Aeon.getInstance().getUtils().getInventory();
		if (eatPress)
		{
			mc.gameSettings.keyBindUseItem.pressed = savedUseItem;
			eatPress = false;
		}
		if (!mc.thePlayer.capabilities.isCreativeMode && mc.thePlayer.getFoodStats().getFoodLevel() <= foodLevel)
		{
			final int food = util.findHotbarFood();
			if (food != -1)
			{
				if (this.oldItem == -1)
				{
					this.oldItem = mc.thePlayer.inventory.currentItem;
				}
				util.swap(food);
				eatPress = true;
				savedUseItem = mc.gameSettings.keyBindUseItem.pressed;
				mc.gameSettings.keyBindUseItem.pressed = true;
				// util.useItem(mc.thePlayer.inventory.getStackInSlot(food));
				return;
			}
		}
	}

	private boolean isShiftable(ItemStack preferedItem)
	{
		if (preferedItem == null)
		{
			return true;
		}
		for (int o = 9; o < 36; o++)
		{
			if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack())
			{
				final ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
				if (item == null)
				{
					return true;
				}
				if (Item.getIdFromItem(item.getItem()) == Item.getIdFromItem(preferedItem.getItem()) && item.stackSize + preferedItem.stackSize <= preferedItem.getMaxStackSize())
				{
					return true;
				}
			} else
			{
				return true;
			}
		}
		return false;
	}
}
