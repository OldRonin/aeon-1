package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.ReflectionUtils;
import me.lordpankake.aeon.util.TimerUtils;
import net.minecraft.entity.Entity;

public class NoSlowdown extends Module
{
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();

	public NoSlowdown(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (!mc.thePlayer.onGround)
		{
			ReflectionUtils.setField(Entity.class, mc.thePlayer, 27, false);
		}
	
	}
}
