package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreTick;
import me.lordpankake.aeon.modules.mod.world.Timer;
import org.lwjgl.input.Keyboard;

public class Sprint extends Module
{
	public Sprint(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onPreTick(EventPreTick event)
	{
		mc.thePlayer.setSprinting(canSprint());
		if (mc.thePlayer.movementInput.moveForward > 0.0F && isControlDown())
		{
			mc.timer.timerSpeed = Timer.speed;
		} else
		{
			mc.timer.timerSpeed = 1.0F;
		}
	}

	public boolean isControlDown()
	{
		return Keyboard.isKeyDown(29) || Keyboard.isKeyDown(157);
	}

	public boolean canSprint()
	{
		return !mc.thePlayer.isSneaking() && mc.thePlayer.movementInput.moveForward > 0.0F && !mc.thePlayer.isCollidedHorizontally && !mc.thePlayer.isInWater() && (mc.playerController.isInCreativeMode() ? true : mc.thePlayer.getFoodStats().getFoodLevel() > 6);
	}
	/*
	 * private boolean canSprint() { return !mc.thePlayer.isOnLadder() &&
	 * mc.thePlayer.onGround && !mc.thePlayer.isInWater() &&
	 * !mc.thePlayer.isInsideOfMaterial(Material.lava) &&
	 * !mc.thePlayer.isInsideOfMaterial(Material.vine) &&
	 * !mc.thePlayer.isSneaking() && !mc.thePlayer.isInWeb &&
	 * !mc.thePlayer.isAirBorne && mc.thePlayer.isEntityAlive() &&
	 * mc.thePlayer.getFoodStats().getFoodLevel() > 6 &&
	 * !mc.thePlayer.isBlocking() && !mc.thePlayer.isEating() &&
	 * mc.thePlayer.moveForward > 0.0F; }
	 */
}
