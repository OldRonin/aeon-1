package me.lordpankake.aeon.modules.mod.player;

import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.potion.Potion;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;

public class AntiFire extends Module
{
	public AntiFire(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public final void onGameTick(final EventTick event)
	{
		if ((mc.thePlayer.isBurning() && !mc.thePlayer.isPotionActive(Potion.fireResistance) && mc.thePlayer.onGround && !mc.thePlayer.isInWater()))
		{
			if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
			{
				for (int s = 0; s <= 10; s++)
				{
					mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
				}
			}else
			{
				for (int s = 0; s <= 30; s++)
				{
					mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
				}
			}
			
		}
	}
}
