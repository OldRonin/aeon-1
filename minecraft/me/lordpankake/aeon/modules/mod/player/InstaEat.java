package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreTick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import net.minecraft.item.ItemFood;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C09PacketHeldItemChange;

public class InstaEat extends Module
{
	public InstaEat(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onPreTick(EventPreTick event)
	{
		boolean ate = false;
		if (mc.thePlayer.isUsingItem() && !mc.thePlayer.isBlocking())
		{
			if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
			{
				mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(true));
			} else
			{
				final boolean safe = mc.thePlayer.onGround || mc.thePlayer.isInWater() || mc.thePlayer.isOnLadder();
				final boolean shouldInstant = mc.gameSettings.keyBindUseItem.isPressed() || mc.thePlayer.inventory.getCurrentItem().getItem() instanceof ItemFood;
				if (safe && shouldInstant)
				{
					ate = true;
					for (int s = 0; s <= 10; s++)
					{
						mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
					}
				}
			}
		}
		if (ate)
		{
			mc.thePlayer.motionY = 0.01;
		}
	}
}
