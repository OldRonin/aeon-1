package me.lordpankake.aeon.modules.mod.player;

import io.netty.util.concurrent.GenericFutureListener;
import java.util.ArrayList;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C0APacketAnimation;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPacketReceive;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPacketSend;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreTick;

public class Blink extends Module
{
	@AIgnore
	private ArrayList<Packet> delayedPackets = new ArrayList<Packet>();

	public Blink(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onDisable()
	{
		for (Packet packet : this.delayedPackets)
		{
			if (packet != null)
			{
				mc.thePlayer.sendQueue.addToSendQueue(packet);
			}
		}
		this.delayedPackets.clear();
	}

	@Override
	@EventTarget
	public void onPacketSend(EventPacketSend event)
	{
		this.delayedPackets.add(event.storedPacket);
		Log.write(event.storedPacket.toString());
		event.nullifyPacket();
	}
}
