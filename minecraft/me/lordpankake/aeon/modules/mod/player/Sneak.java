package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreTick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import net.minecraft.network.play.client.C0BPacketEntityAction;

public class Sneak extends Module
{
	public Sneak(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onDisable()
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
		{
			mc.gameSettings.keyBindSneak.pressed = false;
		} else
		{
		}
	}

	@Override
	@EventTarget
	public void onPreTick(EventPreTick event)
	{
		if (!Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
		{
			mc.thePlayer.sendQueue.addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, 2));
		}
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
		{
			mc.gameSettings.keyBindSneak.pressed = true;
		} else
		{
			mc.thePlayer.sendQueue.addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, 1));
		}
	}
}