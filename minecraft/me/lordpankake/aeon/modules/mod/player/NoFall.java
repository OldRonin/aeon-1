package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPacketSend;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPostTick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreTick;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.StringUtils;

public class NoFall extends Module
{
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();
	@AIgnore
	private boolean savedOnGround;

	public NoFall(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onPacketSend(EventPacketSend event)
	{
		if (event.storedPacket instanceof C03PacketPlayer)
		{
			((C03PacketPlayer)event.storedPacket).onGround = true;
		}
	}
}
