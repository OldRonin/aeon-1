package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;

public class Walk extends Module
{
	public Walk(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		mc.gameSettings.keyBindForward.pressed = true;
		if (mc.thePlayer.isCollidedHorizontally)
		{
			if (mc.thePlayer.onGround)
			{
				mc.gameSettings.keyBindJump.pressed = true;
			}
		} else
		{
			if (mc.gameSettings.keyBindJump.pressed == true)
			{
				mc.gameSettings.keyBindJump.pressed = false;
			}
		}
	}

	@Override
	public void onDisable()
	{
		mc.gameSettings.keyBindForward.pressed = false;
		mc.gameSettings.keyBindJump.pressed = false;
	}
}
