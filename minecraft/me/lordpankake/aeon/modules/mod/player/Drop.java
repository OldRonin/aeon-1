package me.lordpankake.aeon.modules.mod.player;

import net.minecraft.item.ItemStack;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.InventoryUtils;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;

public class Drop extends Module
{
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();
	@AIgnore
	private int invSpace;

	public Drop(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		invSpace = 9;
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
		{
			if (timer.check(1000F / 5F))
			{
				ItemStack item = mc.thePlayer.inventoryContainer.getSlot(invSpace).getStack();
				if (item != null)
				{
					InventoryUtils.clickSlot(invSpace, false);
					InventoryUtils.clickSlot(-999, false);
				}
				invSpace += 1;
				if (invSpace >= 45)
				{
					invSpace = 9;
				}
				this.timer.reset();
			}
		} else
		{
			for (int i = 9; i < 45; i++)
			{
				ItemStack item = mc.thePlayer.inventoryContainer.getSlot(i).getStack();
				if (item != null)
				{
					InventoryUtils.clickSlot(i, false);
					InventoryUtils.clickSlot(-999, false);
				}
			}
		}
	}
}
