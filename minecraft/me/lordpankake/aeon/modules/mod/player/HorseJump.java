package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;

public class HorseJump extends Module
{
	@AIgnore
	private boolean keyBindJump, doRebind;

	public HorseJump(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (mc.thePlayer.horseJumpPower >= 1F)
		{
			keyBindJump = mc.gameSettings.keyBindJump.pressed;
			mc.gameSettings.keyBindJump.pressed = false;
			doRebind = true;
		}
	}
}
