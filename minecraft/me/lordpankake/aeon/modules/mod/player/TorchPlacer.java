package me.lordpankake.aeon.modules.mod.player;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemFood;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventMouseClick;
import me.lordpankake.aeon.util.InventoryUtils;

public class TorchPlacer extends Module
{
	public TorchPlacer(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onMouseClick(EventMouseClick event)
	{
	
		if (event.isRightClick() && !(mc.thePlayer.inventory.getCurrentItem().getItem() instanceof ItemBlock) && !(mc.thePlayer.inventory.getCurrentItem().getItem() instanceof ItemFood) && mc.objectMouseOver.typeOfHit == MovingObjectType.BLOCK)
		{
			
			final InventoryUtils inventory = Aeon.getInstance().getUtils().getInventory();
			final int torchIndex = inventory.findHotbarItem(50);
			final int oldItem = mc.thePlayer.inventory.currentItem;
			
			if (torchIndex != -1 && Aeon.getInstance().getUtils().getBlock().canReach(mc.objectMouseOver.blockX, mc.objectMouseOver.blockY, mc.objectMouseOver.blockZ, 5F))
			{
				Log.write("LELEL");
				inventory.swap(torchIndex);
				inventory.useItem(mc.thePlayer.inventory.getStackInSlot(torchIndex));
				//mc.thePlayer.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(mc.objectMouseOver.blockX, mc.objectMouseOver.blockY+1, mc.objectMouseOver.blockZ, 1, mc.thePlayer.inventory.getCurrentItem(), 0, 0, 0));
				//inventory.swap(oldItem);
			}
		}
	}
}
