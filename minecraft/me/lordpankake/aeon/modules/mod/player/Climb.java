package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.MathHelper;

public class Climb extends Module
{
	public Climb(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (mc.thePlayer.isCollidedHorizontally)
		{
			if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
			{
				if (isOnLadder())
				{
					mc.thePlayer.motionY = 0.21;
					mc.thePlayer.onGround = true;
				}
			} else
			{
				mc.thePlayer.motionY = isShiftDown() ? 0 : 0.21;
				mc.thePlayer.onGround = true;
			}
		}
	}

	private boolean isOnLadder()
	{
		boolean good = false;
		final int x = MathHelper.floor_double(mc.thePlayer.posX);
		final int y = MathHelper.floor_double(mc.thePlayer.boundingBox.minY);
		final int z = MathHelper.floor_double(mc.thePlayer.posZ);
		final Block b1 = mc.theWorld.getBlock(x, y + 1, z);
		final Block b2 = mc.theWorld.getBlock(x, y + 2, z);
		if (b2 == Blocks.ladder || b2 == Blocks.vine || b1 == Blocks.ladder || b1 == Blocks.vine)
		{
			good = true;
		}
		return good;
	}

	public boolean isShiftDown()
	{
		return mc.gameSettings.keyBindSneak.getIsKeyPressed();
	}
}
