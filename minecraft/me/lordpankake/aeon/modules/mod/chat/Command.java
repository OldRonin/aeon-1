package me.lordpankake.aeon.modules.mod.chat;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.handling.module.Initlizer;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;
import net.minecraft.client.Minecraft;

public class Command
{
	public static final Object monitor = new Object();
	public static boolean monitorState = false;
	public static Minecraft mc = Minecraft.getMinecraft();
	protected String name;
	protected String desc;
	public static Initlizer hack;
	public static PlayerUtils chat;
	public static StringUtils util;

	public Command(String name, String desc)
	{
		this.name = name;
		this.desc = desc;
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				populateUtils();
			}

			private void populateUtils()
			{
				monitorState = true;
				while (monitorState)
				{
					synchronized (monitor)
					{
						try
						{
							monitor.wait();
						} catch (final Exception e)
						{
						}
					}
				}
				hack = Aeon.getInstance().getUtils().getHack().getLoader();
				chat = Aeon.getInstance().getUtils().getPlayer();
				util = Aeon.getInstance().getUtils().getString();
			}
		}).start();
	}

	public static void unlockMonitor()
	{
		synchronized (monitor)
		{
			monitorState = false;
			monitor.notifyAll();
		}
	}

	public void onFire(String[] strings)
	{
	}

	public String getName()
	{
		return this.name;
	}

	public String getDescription()
	{
		return this.desc;
	}
}
