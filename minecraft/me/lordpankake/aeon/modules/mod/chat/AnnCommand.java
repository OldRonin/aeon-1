package me.lordpankake.aeon.modules.mod.chat;

import java.lang.reflect.Field;

public class AnnCommand extends Command
{
	private final Field f;
	private final Class c;
	private final int parameters;

	public AnnCommand(String name, String desc, int p, Field f, Class c)
	{
		super(name, desc);
		this.parameters = p;
		this.f = f;
		this.c = c;
	}

	@Override
	public void onFire(String[] s)
	{
	}
}
