package me.lordpankake.aeon.modules.mod.chat.commands.settings.combat;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;

public class SetAutoBowSpeed extends Command
{
	public SetAutoBowSpeed(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (StringUtils.isFloat(s[1]))
			{
				hack.autobow.speed = Float.parseFloat(s[1]);
				hack.autobow.writeSettings();
				PlayerUtils.addChatMessage("AutoBow Speed: " + s[1]);
			}
		}
	}
}
