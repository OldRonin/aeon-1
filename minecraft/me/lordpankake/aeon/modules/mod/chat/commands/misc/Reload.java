package me.lordpankake.aeon.modules.mod.chat.commands.misc;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;

public class Reload extends Command
{
	public Reload(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		Aeon.getInstance().getUtils().reload();
		PlayerUtils.addChatMessage("Settings reloaded.");
	}
}
