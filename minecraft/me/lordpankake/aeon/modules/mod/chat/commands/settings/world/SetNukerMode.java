package me.lordpankake.aeon.modules.mod.chat.commands.settings.world;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.world.Nuker;
import me.lordpankake.aeon.util.PlayerUtils;

public class SetNukerMode extends Command
{
	public SetNukerMode(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		String temp = "";
		if (s.length >= 2)
		{
			if (s[1].contains("id") || s[1].contains("click") || s[1].contains("tick"))
			{
				temp += s[1];
			}
		}
		if (s.length >= 3)
		{
			if (s[2].contains("id") || s[2].contains("click") || s[2].contains("tick"))
			{
				temp += s[2];
			}
		}
		if (temp.length() >= 2)
		{
			Nuker.mode = temp;
		} else
		{
			Nuker.mode = "click";
		}
		PlayerUtils.addChatMessage("Nuker Mode: " + Nuker.mode);
	}
}
