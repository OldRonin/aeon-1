package me.lordpankake.aeon.modules.mod.chat.commands.settings.render;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;

public class SetWaypointTracers extends Command
{
	public SetWaypointTracers(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (s[1].contains("on"))
			{
				hack.waypoints.drawTracer = true;
			}
			if (s[1].contains("off"))
			{
				hack.waypoints.drawTracer = false;
			}
			hack.waypoints.saveWaypoints();
			PlayerUtils.addChatMessage("Waypoint tracers: " + (hack.waypoints.useTracer() ? "on" : "off"));
		} else
		{
			PlayerUtils.addChatMessage("Invalid syntax. Use -WT [on|off]");
		}
	}
}
