package me.lordpankake.aeon.modules.mod.chat.commands.settings.combat;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;

public class SetAuraSilenting extends Command
{
	public SetAuraSilenting(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (s[1].contains("on"))
			{
				hack.aura.silent = true;
				PlayerUtils.addChatMessage("Aura Silent Aiming: On");
			}
			if (s[1].contains("off"))
			{
				hack.aura.silent = false;
				PlayerUtils.addChatMessage("Aura Silent Aiming: Off");
			}
			hack.aura.writeSettings();
		}
	}
}
