package me.lordpankake.aeon.modules.mod.chat.commands.settings.combat;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.combat.AutoDisconnect;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;

public class SetDisconnectHealth extends Command
{
	public SetDisconnectHealth(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (StringUtils.isInteger(s[1]))
		{
			AutoDisconnect.health = Integer.parseInt(s[1]);
			PlayerUtils.addChatMessage("Disconnect Health: " + s[1]);
		}
	}
}
