package me.lordpankake.aeon.modules.mod.chat.commands.misc;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.FriendUtils;
import me.lordpankake.aeon.util.PlayerUtils;

public class Friend extends Command
{
	public Friend(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		final FriendUtils friend = Aeon.getInstance().getUtils().getFriend();
		if (s.length == 2)
		{
			if (s[1].contains("list"))
			{
				String temp = "";
				for (final String swellChap : friend.getFriends())
				{
					temp += swellChap + " ";
				}
				PlayerUtils.addChatMessage(temp);
				return;
			}
		}
		if (s.length >= 3)
		{
			if (s[1].contains("clear"))
			{
				friend.clearFriends();
				friend.saveFriends();
				PlayerUtils.addChatMessage("Friend list cleared.");
				return;
			}
			if (s[1].contains("add"))
			{
				friend.addFriend(s[2]);
				friend.saveFriends();
				PlayerUtils.addChatMessage(s[2] + " is now on the friends list.");
				return;
			}
			if (s[1].contains("rem") && friend.containsFriend(s[2]))
			{
				friend.removeFriend(s[2]);
				friend.saveFriends();
				PlayerUtils.addChatMessage(s[2] + " is no longer on the friends list.");
				return;
			}
		}
		PlayerUtils.addChatMessage("Error: Invalid Syntax. use -F [Add/Rem] [Username] without []'s");
	}
}
