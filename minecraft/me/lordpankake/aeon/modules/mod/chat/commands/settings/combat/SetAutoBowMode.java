package me.lordpankake.aeon.modules.mod.chat.commands.settings.combat;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;

public class SetAutoBowMode extends Command
{
	public SetAutoBowMode(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		String temp = "";
		if (s.length >= 2)
		{
			if (s[1].contains("auto") || s[1].contains("click"))
			{
				temp += s[1];
				hack.autobow.mode = temp;
				hack.autobow.writeSettings();
				PlayerUtils.addChatMessage("AutoBow Mode: " + temp);
			} else
			{
				PlayerUtils.addChatMessage("Invalid syntax. Parameters are [Auto|Click]");
			}
		} else
		{
			PlayerUtils.addChatMessage("Invalid syntax. Parameters are [Auto|Click]");
		}
	}
}