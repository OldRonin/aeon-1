package me.lordpankake.aeon.modules.mod.chat.commands.settings.render;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;

public class Waypoint extends Command
{
	public Waypoint(String name, String desc)
	{
		super("Point", "Makes a highlighted point in the world. -point add [name] or -point clear");
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (s.length >= 3)
			{
				if (s[1].contains("add"))
				{
					hack.waypoints.add(s[2]);
				}
			}
			if (s[1].contains("clear"))
			{
				hack.waypoints.clearPoints();
			}
			if (s[1].contains("load"))
			{
				hack.waypoints.loadWaypoints();
			}
		} else
		{
			PlayerUtils.addChatMessage("Invalid syntax: -point add [name] or -point clear");
		}
	}
}
