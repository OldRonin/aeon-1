package me.lordpankake.aeon.modules.mod.chat.commands.settings.player;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.player.Fastmine;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;

public class SetFastmineSpeed extends Command
{
	public SetFastmineSpeed(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (StringUtils.isFloat(s[1]))
			{
				if (Float.parseFloat(s[1]) > 1f)
				{
					Fastmine.speed = Float.parseFloat(s[1]);
				} else
				{
					Fastmine.speed = Float.parseFloat(s[1]);
				}
				PlayerUtils.addChatMessage("Fastmine Speed: " + s[1]);
			}
		}
	}
}
