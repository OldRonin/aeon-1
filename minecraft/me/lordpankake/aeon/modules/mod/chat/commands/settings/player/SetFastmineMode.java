package me.lordpankake.aeon.modules.mod.chat.commands.settings.player;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.player.Fastmine;
import me.lordpankake.aeon.util.PlayerUtils;

public class SetFastmineMode extends Command
{
	public SetFastmineMode(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		String temp = "";
		if (s.length >= 2)
		{
			if (s[1].contains("delay") || s[1].contains("set") || s[1].contains("auto"))
			{
				temp += s[1];
			}
		}
		if (s.length >= 3)
		{
			if (s[2].contains("delay") || s[2].contains("set") || s[2].contains("auto"))
			{
				temp += " " + s[2];
			}
		}
		if (temp.length() >= 3)
		{
			Fastmine.mode = temp;
		} else
		{
			Fastmine.mode = "auto";
		}
		PlayerUtils.addChatMessage("Fastmine Mode: " + Fastmine.mode);
	}
}
