package me.lordpankake.aeon.modules.mod.chat.commands.settings.combat;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;

public class SetAuraSpeed extends Command
{
	public SetAuraSpeed(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (StringUtils.isFloat(s[1]))
			{
				hack.aura.speed = Float.parseFloat(s[1]);
				hack.aura.writeSettings();
				PlayerUtils.addChatMessage("Aura Speed: " + s[1]);
			}
		}
	}
}
