package me.lordpankake.aeon.modules.mod.chat.commands.settings.render;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.render.Search;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;

public class SetSearchRange extends Command
{
	public SetSearchRange(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (StringUtils.isInteger(s[1]))
			{
				Search.range = Integer.parseInt(s[1]);
				hack.search.writeSettings();
				PlayerUtils.addChatMessage("Search range set to: " + s[1]);
			} else
			{
				PlayerUtils.addChatMessage("Parameter must be an integer!");
			}
		} else
		{
			PlayerUtils.addChatMessage("Invalid syntax. Use -SR [Integer] without []'s.");
		}
	}
}
