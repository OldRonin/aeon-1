package me.lordpankake.aeon.modules.mod.chat.commands.settings.combat;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.combat.Soup;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;

public class SetSoupHealth extends Command
{
	public SetSoupHealth(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (StringUtils.isFloat(s[1]))
			{
				Soup.healthLevel = Float.parseFloat(s[1]);
				hack.soup.writeSettings();
				PlayerUtils.addChatMessage("Soup Health: " + s[1]);
			}
		}
	}
}
