package me.lordpankake.aeon.modules.mod.chat.commands.misc;

import java.util.ArrayList;
import java.util.List;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class Help extends Command
{
	private final ArrayList HelpWithPages = new ArrayList<HelpPage>();
	private int lastPage;
	private int page = 1;
	private int counter = 0;

	public Help(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (HelpWithPages.size() < 2)
		{
			for (final Command com : Aeon.getInstance().getUtils().getHack().getLoader().commands.getCommands())
			{
				counter += 1;
				if (counter == 6)
				{
					page += 1;
					lastPage = page;
					counter = 0;
				}
				HelpWithPages.add(new HelpPage(com, page));
			}
		}
		if (s.length >= 2)
		{
			if (StringUtils.isInteger(s[1]))
			{
				final ChatComponentText splitter = new ChatComponentText("-----------------------------");
				splitter.getChatStyle().setColor(EnumChatFormatting.AQUA);
				mc.thePlayer.addChatComponentMessage(splitter);
				for (final HelpPage help : getPagedCommands())
				{
					if (help.getPage() == Integer.parseInt(s[1]) && help.com.getName() != "Help")
					{
						PlayerUtils.addChatMessage(help.com.getName() + " || " + help.com.getDescription());
						// chat.addChatMessage("");
					}
				}
			} else if (s[1].contains("list"))
			{
				for (final HelpPage help : getPagedCommands())
				{
					PlayerUtils.addChatMessage(help.com.getName());
				}
			}
		} else
		{
			PlayerUtils.addChatMessage("Please enter a valid page number. There are " + lastPage + " pages.");
		}
	}

	public List<HelpPage> getPagedCommands()
	{
		return HelpWithPages;
	}
	class HelpPage
	{
		public final int page;
		public final Command com;

		public HelpPage(Command com, int page)
		{
			this.page = page;
			this.com = com;
		}

		public int getPage()
		{
			return this.page;
		}
	}
}
