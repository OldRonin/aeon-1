package me.lordpankake.aeon.modules.mod.chat.commands.misc;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.FileUtils;
import net.minecraft.client.gui.GuiPlayerInfo;

public class Scrape extends Command
{
	public Scrape(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		final ArrayList<String> names = new ArrayList<String>();
		final List playerList = mc.thePlayer.sendQueue.playerInfoList;
		for (int x = 0; x < playerList.size(); x++)
		{
			final GuiPlayerInfo playerinfo = (GuiPlayerInfo) playerList.get(x);
			final String username = playerinfo.name;
			final String allowed = "abcdefghijklmnopqrstuvwxyz1234567890_";
			if (!allowed.contains(username.substring(0, 1).toLowerCase()))
			{
				names.add(username.substring(2));
			} else
			{
				names.add(username);
			}
		}
		final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		final File saveFile = new File("Aeon" + File.separator + "Scrapes" + File.separator + "scrape" + timeStamp + ".txt");
		Aeon.getInstance().getUtils().getFile();
		FileUtils.write(saveFile, names, true);
		final Desktop dk = Desktop.getDesktop();
		try
		{
			if (saveFile.exists())
			{
				dk.open(new File(saveFile.toString()));
			}
		} catch (final IOException e)
		{
			e.printStackTrace();
		}
	}
}
