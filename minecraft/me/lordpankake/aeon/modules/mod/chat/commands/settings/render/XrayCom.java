package me.lordpankake.aeon.modules.mod.chat.commands.settings.render;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.render.Xray;
import me.lordpankake.aeon.util.PlayerUtils;

public class XrayCom extends Command
{
	public XrayCom(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		final String temp = "";
		if (s.length >= 3)
		{
			final String mode = s[1];
			final String textureName = s[2];
			if (mode.equals("add"))
			{
				Xray.blockList.add(textureName);
			} else
			{
				if (Xray.blockList.contains(textureName))
				{
					Xray.blockList.remove(textureName);
				}
			}
			hack.xray.writeSettings();
			hack.xray.toggle();
			hack.xray.toggle();
		} else
		{
			PlayerUtils.addChatMessage("Invalid syntax! Use -Xray [Add|Rem] [textureName]");
		}
	}
}
