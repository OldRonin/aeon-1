package me.lordpankake.aeon.modules.mod.chat.commands.misc;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;

public class Enchant extends Command
{
	public Enchant(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (s[1].contains("list"))
			{
				for (final Enchantment enchantment : Enchantment.enchantmentsList)
				{
					if (enchantment == null)
					{
						continue;
					}
					PlayerUtils.addChatMessage(enchantment.getName().substring(12).toLowerCase());
				}
				return;
			}
		}
		boolean enchanted = false;
		if (!mc.playerController.isInCreativeMode())
		{
			PlayerUtils.addChatMessage("You need to be in creative mode!");
			return;
		}
		final ItemStack stack = mc.thePlayer.inventory.getCurrentItem();
		for (final Enchantment enchantment : Enchantment.enchantmentsList)
		{
			try
			{
				if (s.length >= 3 && StringUtils.isInteger(s[2]))
				{
					int lvl = Integer.parseInt(s[2]);
					if (lvl > 127)
					{
						lvl = 127;
					}
					if (lvl > 0)
					{
						if (enchantment == null)
						{
							continue;
						}
						if (enchantment.getName().substring(12).toLowerCase().contains(s[1]))
						{
							if (enchantment.type.canEnchantItem(stack.getItem()))
							{
								stack.addEnchantment(enchantment, lvl);
							}
						}
					}
					enchanted = true;
				}
				if (s.length >= 2 && StringUtils.isInteger(s[1]) && !enchanted)
				{
					int lvl = Integer.parseInt(s[1]);
					if (lvl > 127)
					{
						lvl = 127;
					}
					if (lvl > 0)
					{
						if (enchantment == null)
						{
							continue;
						}
						if (enchantment.type.canEnchantItem(stack.getItem()))
						{
							stack.addEnchantment(enchantment, lvl);
						}
					}
					enchanted = true;
				}
			} catch (final Exception e)
			{
			}
			if (!enchanted)
			{
				if (enchantment == null)
				{
					continue;
				}
				if (enchantment.type.canEnchantItem(stack.getItem()))
				{
					stack.addEnchantment(enchantment, enchantment.getMaxLevel());
				}
			}
		}
	}
}
