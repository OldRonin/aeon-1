package me.lordpankake.aeon.modules.mod.misc;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.ui.InGame;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeGUI;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiChat;
import org.lwjgl.input.Keyboard;

public class Keybinds extends Module
{
	public Keybinds(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@EventTarget
	@Override
	public void onGameTick(EventTick event)
	{
		if (!(mc.currentScreen instanceof GuiChat) && !(mc.currentScreen instanceof PankakeGUI))
		{
			if (mc.currentScreen != null)
			{
				return;
			}
		}
		final FontRenderer fr = mc.fontRenderer;
		int height = 13;
		for (final Module m : Aeon.getInstance().getUtils().getHack().getModules())
		{
			if (m.getType().toString().contains(InGame.getEnum()) && m.getKey() != Keyboard.CHAR_NONE)
			{
				fr.drawStringWithShadow("[" + Keyboard.getKeyName(m.getKey()) + "] " + m.getName(), 1, height, m.getColor());
				height += 10;
			}
		}
	}
}
