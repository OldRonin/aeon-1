package me.lordpankake.aeon.modules.mod.misc;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventMouseClick;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;

public class ClickFriends extends Module
{
	public ClickFriends(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@EventTarget
	@Override
	public void onMouseClick(EventMouseClick event)
	{
		if (event.isMiddleClick() && mc.objectMouseOver != null && mc.objectMouseOver.typeOfHit == MovingObjectType.ENTITY)
		{
			if (Aeon.getInstance().getUtils().getFriend().containsFriend(mc.objectMouseOver.entityHit.getCommandSenderName()))
			{
				Aeon.getInstance().getUtils().getFriend().removeFriend(mc.objectMouseOver.entityHit.getCommandSenderName());
			} else
			{
				Aeon.getInstance().getUtils().getFriend().addFriend(mc.objectMouseOver.entityHit.getCommandSenderName());
			}
			Aeon.getInstance().getUtils().getFriend().saveFriends();
		}
	}
}
