package me.lordpankake.aeon.modules.mod.misc;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;

public class ModDescriptions extends Module
{
	public ModDescriptions(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}
}
