package me.lordpankake.aeon.modules.mod.misc;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeGUI;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiChat;

public class Arraylist extends Module
{
	public boolean alphabetical = false;

	public Arraylist(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		loadSettings();
	}

	@EventTarget
	@Override
	public void onGameTick(EventTick event)
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().legit.getState())
		{
			return;
		}
		if (mc.currentScreen != null)
		{
			if (!(mc.currentScreen instanceof GuiChat) && !(mc.currentScreen instanceof PankakeGUI))
			{
				return;
			}
		}
		final FontRenderer fr = mc.fontRenderer;
		int height = 2;
		final int displayWidth = Aeon.getInstance().getUtils().getuScreen().getScreenWidth();
		if (alphabetical)
		{
			for (final Module t : Aeon.getInstance().getUtils().getHack().getModules())
			{
				if (t.getState() && t.getType() != ModuleType.Misc)
				{
					fr.drawStringWithShadow(t.getDisplayName(), displayWidth - Minecraft.getMinecraft().fontRenderer.getStringWidth(t.getName()) - 2, height, t.getColor());
					height += 10;
				}
			}
		} else
		{
			for (final ModuleType mt : ModuleType.values())
			{
				for (final Module t : Aeon.getInstance().getUtils().getHack().getModules())
				{
					if (t.getType() == mt && t.getState() && t.getType() != ModuleType.Misc)
					{
						fr.drawStringWithShadow(t.getName(), displayWidth - Minecraft.getMinecraft().fontRenderer.getStringWidth(t.getName()) - 2, height, t.getColor());
						height += 10;
					}
				}
			}
		}
	}
}
