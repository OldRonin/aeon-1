package me.lordpankake.aeon.modules.mod.world;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.ClientUtils;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventProcessMessage;
import me.lordpankake.aeon.util.PlayerUtils;

public class TPAccept extends Module
{
	public boolean friendsOnly = true;

	public TPAccept(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onReadMessage(EventProcessMessage event)
	{
		final ClientUtils util = Aeon.getInstance().getUtils();
		if (event.message.toLowerCase().contains("requested") && event.message.toLowerCase().contains("teleport"))
		{
			if (friendsOnly)
			{
				boolean doSend = false;
				for (final String friend : util.getFriend().getFriends())
				{
					if (event.message.contains(friend))
					{
						doSend = true;
					}
				}
				if (doSend)
				{
					util.getPlayer();
					PlayerUtils.sendChatMessage("/tpaccept");
				}
			} else
			{
				util.getPlayer();
				PlayerUtils.sendChatMessage("/tpaccept");
			}
		}
	}
}
