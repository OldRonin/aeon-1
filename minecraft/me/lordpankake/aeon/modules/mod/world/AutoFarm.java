package me.lordpankake.aeon.modules.mod.world;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreTick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.BlockItemUtils;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;
import me.lordpankake.aeon.util.other.BlockPlaceholder;
import net.minecraft.block.Block;

public class AutoFarm extends Module
{
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();
	@AIgnore
	private int side;
	@AIgnore
	private BlockPlaceholder blockCoordinates;
	@AIgnore
	private final int replantID = 392;

	public AutoFarm(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@EventTarget
	@Override
	public void onPreTick(EventPreTick event)
	{
		this.blockCoordinates = getClosestCrop();
		if (this.blockCoordinates != null && this.timer.check(100.0F))
		{
			Aeon.getInstance().getUtils().getBlock().faceBlock(this.blockCoordinates.getX() + 0.5F, this.blockCoordinates.getY() + 0.5F, this.blockCoordinates.getZ() + 0.5F);
		}
	}

	@EventTarget
	@Override
	public void onGameTick(EventTick event)
	{
		final BlockItemUtils util = Aeon.getInstance().getUtils().getBlock();
		if (this.blockCoordinates != null && this.timer.check(10F))
		{
			mc.thePlayer.swingItem();
			util.breakBlock(this.blockCoordinates, 0, this.side);
			mc.playerController.onPlayerDestroyBlock(this.blockCoordinates.getX(), this.blockCoordinates.getY(), this.blockCoordinates.getZ(), this.side);
			this.blockCoordinates = null;
			this.timer.reset();
		}
	}

	private BlockPlaceholder getClosestCrop()
	{
		final BlockItemUtils util = Aeon.getInstance().getUtils().getBlock();
		BlockPlaceholder closestBlock = null;
		final int radius = 4;
		for (int i = radius; i >= -radius; i--)
		{
			for (int j = -radius; j <= radius; j++)
			{
				for (int k = radius; k >= -radius; k--)
				{
					final int possibleX = (int) (mc.thePlayer.posX + i);
					final int possibleY = (int) (mc.thePlayer.posY + j);
					final int possibleZ = (int) (mc.thePlayer.posZ + k);
					final Block block = mc.theWorld.getBlock(possibleX, possibleY, possibleZ);
					final BlockPlaceholder hrvsBlock = new BlockPlaceholder(possibleX, possibleY, possibleZ, mc.theWorld.getBlock(possibleX, possibleY, possibleZ), mc.theWorld.getBlock(possibleX, possibleY, possibleZ).textureName);
					if (block != null && util.canReach(hrvsBlock, radius))
					{
						final int blockID = Aeon.getInstance().getUtils().getBlock().getBlockID(block);
						// MovingObjectPosition result = util.rayTrace(new
						// BlockPlaceholder(hrvsBlock.getX(), hrvsBlock.getY(),
						// hrvsBlock.getZ(),
						// mc.theWorld.getBlock(hrvsBlock.getX(),
						// hrvsBlock.getY(), hrvsBlock.getZ()),
						// mc.theWorld.getBlock(hrvsBlock.getX(),
						// hrvsBlock.getY(), hrvsBlock.getZ()).textureName));
						switch (blockID)
						{
						case 83:
							if (Aeon.getInstance().getUtils().getBlock().getBlockID(mc.theWorld.getBlock(possibleX, possibleY, possibleZ)) != 83)
							{
								continue;
							}
							// this.side = result != null ? result.sideHit : 0;
							if (closestBlock == null)
							{
								closestBlock = hrvsBlock;
							} else if (mc.thePlayer.getDistance(hrvsBlock.getX(), hrvsBlock.getY(), hrvsBlock.getZ()) < mc.thePlayer.getDistance(closestBlock.getX(), closestBlock.getY(), closestBlock.getZ()))
							{
								closestBlock = hrvsBlock;
							}
							break;
						case 59:
						case 115:
						case 127:
						case 141:
						case 142:
							if (mc.theWorld.getBlockMetadata(hrvsBlock.getX(), hrvsBlock.getY(), hrvsBlock.getZ()) < getMaxGrowthLevel(blockID))
							{
								continue;
							}
							// this.side = result != null ? result.sideHit : 0;
							if (closestBlock == null)
							{
								closestBlock = hrvsBlock;
							} else if (mc.thePlayer.getDistance(hrvsBlock.getX(), hrvsBlock.getY(), hrvsBlock.getZ()) < mc.thePlayer.getDistance(closestBlock.getX(), closestBlock.getY(), closestBlock.getZ()))
							{
								closestBlock = hrvsBlock;
							}
							break;
						default:
							break;
						}
					}
				}
			}
		}
		return closestBlock;
	}

	private int getMaxGrowthLevel(int blockID)
	{
		switch (blockID)
		{
		case 115:
			return 3;
		case 59:
		case 127:
		case 141:
		case 142:
			return 7;
		}
		return 0;
	}
}
