package me.lordpankake.aeon.modules.mod.world.radar;

import org.lwjgl.opengl.GL11;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;

public class JMTradar
{
	private int posX;
	private int posY;
	private final Minecraft mc;
	private final Gui g;
	// private EntityHandler eh;
	private final TerrainHandler th;
	private final JMTgui jg;
	private final ScaledResolution sr;
	public static boolean enabled = true;
	public static boolean entities = true;
	public static boolean terrain = true;
	public static int rotateMode = 1;

	public JMTradar(Minecraft mc, Gui g)
	{
		this.mc = mc;
		this.g = g;
		// eh = new EntityHandler(mc, g, 48);
		th = new TerrainHandler(mc, g, -51, 51);
		jg = new JMTgui(g);
		sr = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
	}

	public void setLocation(int x, int y)
	{
		this.posX = x;
		this.posY = y;
	}

	public void drawRadar()
	{
		final int displayWidth = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight).getScaledWidth();
		final int displayHeight = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight).getScaledHeight();
		GL11.glPushMatrix();
		GL11.glTranslatef(displayWidth - 62, displayHeight - 61, 0);
		if (rotateMode == 1)
		{
			GL11.glRotatef(-mc.thePlayer.rotationYaw, 0, 0, 1);
		}
		try
		{
			if (terrain)
			{
				th.drawTopLayer(0, 0);
			}
			if (entities)
			{
				// eh.renderSurroundingEntities(0, 0);
			}
		} catch (final Exception e)
		{
			// probably the nether...
		}
		if (rotateMode == 1)
		{
			JMTgui.drawHollowCircle(0, 0, 56, 360, (float) 8 * sr.scaleFactor, 0xff696969);
			JMTgui.drawHollowCircle(0, 0, 54, 360, (float) 8 * sr.scaleFactor, 0xff696969);
			JMTgui.drawHollowCircle(0, 0, 52, 360, (float) 8 * sr.scaleFactor, 0xff696969);
			JMTgui.drawHollowCircle(0, 0, 50, 360, (float) 8 * sr.scaleFactor, 0xff696969);
			JMTgui.drawHollowCircle(0, 0, 58, 360, (float) 8 * sr.scaleFactor, 0xffffffff);
			renderDirections();
		}
		if (rotateMode == 0)
		{
			JMTgui.drawHollowCircle(0, 0, 53, 360, (float) 8 * sr.scaleFactor, 0xffffffff);
			JMTgui.drawHollowCircle(0, 0, 51, 360, (float) 8 * sr.scaleFactor, 0xffffffff);
		}
		GL11.glRotatef(mc.thePlayer.rotationYaw, 0, 0, 1);
		JMTgui.drawIsoscolesTriangle(0, 0, 2, 0, 0xaaffff00);
		JMTgui.drawIsoscolesTriangleOutline(0, 0, 2, 0, 2, 0xffffff00);
		GL11.glTranslatef(-posX, -posY, 0);
		GL11.glPopMatrix();
	}

	private void renderDirections()
	{
		// needs work...lazy method
		GL11.glTranslatef(0, 53, 0);
		GL11.glRotatef(mc.thePlayer.rotationYaw, 0, 0, 1);
		g.drawCenteredString(mc.fontRenderer, "N", 0, -4, 0xffffff);
		GL11.glRotatef(-mc.thePlayer.rotationYaw, 0, 0, 1);
		GL11.glTranslatef(0, -53, 0);
		GL11.glTranslatef(53, 0, 0);
		GL11.glRotatef(mc.thePlayer.rotationYaw, 0, 0, 1);
		g.drawCenteredString(mc.fontRenderer, "W", 0, -4, 0xffffff);
		GL11.glRotatef(-mc.thePlayer.rotationYaw, 0, 0, 1);
		GL11.glTranslatef(-53, 0, 0);
		GL11.glTranslatef(0, -53, 0);
		GL11.glRotatef(mc.thePlayer.rotationYaw, 0, 0, 1);
		g.drawCenteredString(mc.fontRenderer, "S", 0, -4, 0xffffff);
		GL11.glRotatef(-mc.thePlayer.rotationYaw, 0, 0, 1);
		GL11.glTranslatef(0, 53, 0);
		GL11.glTranslatef(-53, 0, 0);
		GL11.glRotatef(mc.thePlayer.rotationYaw, 0, 0, 1);
		g.drawCenteredString(mc.fontRenderer, "E", 0, -4, 0xffffff);
		GL11.glRotatef(-mc.thePlayer.rotationYaw, 0, 0, 1);
		GL11.glTranslatef(53, 0, 0);
	}
}
