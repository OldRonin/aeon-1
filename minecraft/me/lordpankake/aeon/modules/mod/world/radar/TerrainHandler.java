package me.lordpankake.aeon.modules.mod.world.radar;

import me.lordpankake.aeon.core.Aeon;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.*;
import net.minecraft.client.gui.Gui;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.ColorizerFoliage;
import net.minecraft.world.ColorizerGrass;

public class TerrainHandler
{
	private final int lowerBound;
	private final int upperBound;
	private final Minecraft mc;
	private final Gui g;

	public TerrainHandler(Minecraft mc, Gui g, int lowerBound, int upperBound)
	{
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		this.mc = mc;
		this.g = g;
	}

	private void colorBasedOnMultiplier(int color, int y)
	{
		final float darkness = (100 - y) / 150F;
		final float f = (color >> 24 & 0xff) / 255F;
		final float f1 = (color >> 16 & 0xff) / 255F;
		final float f2 = (color >> 8 & 0xff) / 255F;
		final float f3 = (color & 0xff) / 255F;
		GL11.glColor4f(f1 - darkness, f2 - darkness, f3 - darkness, 1F);
	}

	public void drawTopLayer(int centerX, int centerY)
	{
		mc.renderEngine.bindTexture(new ResourceLocation("Aeon/terrain.png")); // binding
																				// terrain
																				// texture
		for (int as = lowerBound; as < upperBound; as++)
		{
			final int pX = as;
			for (int aa = lowerBound; aa < upperBound; aa++)
			{
				final int pZ = aa;
				final int bX = (int) (mc.thePlayer.posX + pX); // block posX
				final int bZ = (int) (mc.thePlayer.posZ + pZ); // block posZ
				final int bY = mc.theWorld.getHeightValue(bX, bZ) - 1; // formatted
																		// block
																		// posY
																		// (-1
																		// for
																		// compensation)
				final int blockID = Aeon.getInstance().getUtils().getBlock().getBlockID(mc.theWorld.getBlock(bX, bY, bZ));// block
																															// id
				final double d = MathHelper.clamp_float(mc.theWorld.getWorldChunkManager().getBiomeGenAt(bX, bY).getFloatTemperature(bX, bY, bZ), 0.0F, 1.0F); // biome
																																								// info
				final double d1 = MathHelper.clamp_float(mc.theWorld.getWorldChunkManager().getBiomeGenAt(bX, bY).getFloatRainfall(), 0.0F, 1.0F); // biome
																																					// info
				final double disx = mc.thePlayer.posX - bX;
				final double disz = mc.thePlayer.posZ - bZ;
				if ((disx - 0.5) * (disx - 0.5) + (disz - 0.5) * (disz - 0.5) > upperBound * upperBound)
				{
					continue;
				}
				if (blockID == 2)// grass
				{
					colorBasedOnMultiplier(ColorizerGrass.getGrassColor(d, d1), bY);
				} else if (blockID == 18 || blockID == 161 || blockID == 81)// trees
																			// n
																			// stuff
				{
					colorBasedOnMultiplier(ColorizerFoliage.getFoliageColor(d, d1), bY);
				} else if (blockID == 159)// stained clay
				{
					colorBasedOnMultiplier(0xffC28B74, bY);
				} else if (blockID == 12)// sand
				{
					colorBasedOnMultiplier(0xffFFFF66, bY);
				} else if (blockID == 103)// melon
				{
					colorBasedOnMultiplier(0xffADFF5C, bY);
				} else if (blockID == 128 || blockID == 24)// sandstone
				{
					colorBasedOnMultiplier(0xffFFE680, bY);
				} else if (blockID == 162 || blockID == 17 || blockID == 3)// wood
				{
					colorBasedOnMultiplier(0xffD29152, bY);
				} else if (blockID == 13)
				{
					colorBasedOnMultiplier(0xff778899, bY);
				} else if (blockID == 8 || blockID == 9)// water
				{
					colorBasedOnMultiplier(0xff000fff, bY);
				} else if (blockID == 10 || blockID == 11)// lava
				{
					colorBasedOnMultiplier(0xffFF5500, bY);
				} else
				{
					colorBasedOnMultiplier(0xffCCCCCC, bY);
				}
				g.drawTexturedModalRect((int) (disx - 1), (int) (disz - 1), 0, 0, 1, 1);
			}
		}
	}
}
