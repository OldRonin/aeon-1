package me.lordpankake.aeon.modules.mod.world;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventBlockPlace;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;

public class Build extends Module
{
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();
	public static int range = 3;
	public static float speed = 10;
	public static String mode = "floor"; // carpet//pole//floor//path

	public Build(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@EventTarget
	@Override
	public void onGameTick(EventTick event)
	{
		if (mode.contains("path"))
		{
			buildPath();
		}
		if (mode.contains("carpet"))
		{
			buildMagicCarpet();
		}
	}

	@EventTarget
	@Override
	public void onBlockPlace(EventBlockPlace event)
	{
		if (mode.contains("pole"))
		{
			buildPole(event.x, event.y, event.z);
		}
		if (mode.contains("floor"))
		{
			buildFloor(event.x, event.y, event.z);
		}
	}

	private void buildPole(int x, int y, int z)
	{
		final int playerX = (int) mc.thePlayer.posX;
		final int playerY = (int) mc.thePlayer.posY - 1;
		final int playerZ = (int) mc.thePlayer.posZ;
		final ItemStack SelectedItem = mc.thePlayer.inventory.getCurrentItem();
		if (SelectedItem != null)
		{
			final int blockX = playerX + x;
			final int blockY = playerY + y;
			final int blockZ = playerZ + z;
			for (int y3 = -range; y3 <= range; y3++)
			{
				mc.thePlayer.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(x, y + y3, z, 1, mc.thePlayer.inventory.getCurrentItem(), 0, 0, 0));
			}
		}
	}

	private void buildFloor(int x, int y, int z)
	{
		final int playerX = (int) mc.thePlayer.posX;
		final int playerY = (int) mc.thePlayer.posY - 1;
		final int playerZ = (int) mc.thePlayer.posZ;
		final ItemStack SelectedItem = mc.thePlayer.inventory.getCurrentItem();
		if (SelectedItem != null)
		{
			final int blockX = playerX + x;
			final int blockY = playerY + y;
			final int blockZ = playerZ + z;
			for (int x3 = -range; x3 <= range; x3++)
			{
				for (int z3 = -range; z3 <= range; z3++)
				{
					mc.thePlayer.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(x + x3, y, z + z3, 1, mc.thePlayer.inventory.getCurrentItem(), 0, 0, 0));
				}
			}
		}
	}

	private void buildMagicCarpet()
	{
		final int x = (int) mc.thePlayer.posX;
		final int y = (int) mc.thePlayer.posY - 1;
		final int z = (int) mc.thePlayer.posZ;
		final ItemStack SelectedItem = mc.thePlayer.inventory.getCurrentItem();
		if (SelectedItem != null)
		{
			for (int x3 = -range; x3 <= range; x3++)
			{
				for (int y3 = -range; y3 <= range; y3++)
				{
					for (int z3 = -range; z3 <= range; z3++)
					{
						final int blockX = x + x3;
						final int blockY = y + y3;
						final int blockZ = z + z3;
						if (!mc.gameSettings.keyBindSneak.isPressed())
						{
							if (!Aeon.getInstance().getUtils().getBlock().isBlockSolid(x + x3, y - 2, z + z3))
							{
								mc.thePlayer.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(x + x3, y - 2, z + z3, 1, mc.thePlayer.inventory.getCurrentItem(), 0, 0, 0));
							}
						} else if (mc.thePlayer.capabilities.isCreativeMode)
						{
							mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(0, x + x3, y, z + z3, 1));
							mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(2, x + x3, y, z + z3, 1));
							mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(0, x + x3, y - 1, z + z3, 1));
							mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(2, x + x3, y - 1, z + z3, 1));
							mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(0, x + x3, y - 2, z + z3, 1));
							mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(2, x + x3, y - 2, z + z3, 1));
						}
					}
				}
			}
		}
	}

	private void buildPath()
	{
		final int x = (int) mc.thePlayer.posX;
		final int y = (int) mc.thePlayer.posY - 1;
		final int z = (int) mc.thePlayer.posZ;
		final ItemStack SelectedItem = mc.thePlayer.inventory.getCurrentItem();
		if (SelectedItem != null)
		{
			for (int x3 = -range; x3 <= range; x3++)
			{
				for (int y3 = -range; y3 <= range; y3++)
				{
					for (int z3 = -range; z3 <= range; z3++)
					{
						final int blockX = x + x3;
						final int blockY = y + y3;
						final int blockZ = z + z3;
						final String SelectedItemName = SelectedItem.getItem().getIconString();
						if (mc.theWorld.getBlock(blockX, blockY, blockZ).textureName != SelectedItem.getItem().getIconString())
						{
							if (mc.theWorld.getBlock(blockX, blockY + 1, blockZ).getMaterial() == Material.air)
							{
								if (mc.theWorld.getBlock(blockX, blockY, blockZ).textureName != mc.theWorld.getBlock(blockX, blockY, blockZ).textureName)
								{
									mc.thePlayer.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(x + x3, y, z + z3, 1, mc.thePlayer.inventory.getCurrentItem(), 0, 0, 0));
								}
							}
						}
					}
				}
			}
		}
	}
}
