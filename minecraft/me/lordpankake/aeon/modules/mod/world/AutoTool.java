package me.lordpankake.aeon.modules.mod.world;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventBlockClick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventBlockDamage;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemStack;

public class AutoTool extends Module
{
	@AIgnore
	private int bestSlot;

	public AutoTool(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onBlockClick(EventBlockClick event)
	{
		if (mc.playerController.isInCreativeMode())
		{
			return;
		}
	}

	@Override
	@EventTarget
	public void onBlockDamage(EventBlockDamage event)
	{
		if (mc.playerController.isInCreativeMode())
		{
			return;
		}
		final int bestSlot = findBestTool(event.x, event.y, event.z);
		if (bestSlot != -666)
		{
			mc.thePlayer.inventory.currentItem = bestSlot;
			this.bestSlot = bestSlot;
		}
	}

	private int findBestTool(int x, int y, int z)
	{
		int bestItem = -666;
		float strength = 1.0F;
		final Block block = this.mc.theWorld.getBlock(x, y, z);
		if (block.getMaterial() == Material.air)
		{
			return -666;
		}
		for (int index = 0; index < 9; index++)
		{
			try
			{
				final ItemStack item = mc.thePlayer.inventory.getStackInSlot(index);
				if (item != null && item.func_150997_a(block) > strength)
				{
					strength = item.func_150997_a(block);
					bestItem = index;
				}
			} catch (final Exception localException)
			{
			}
		}
		return bestItem;
	}
}
