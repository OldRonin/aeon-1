package me.lordpankake.aeon.modules.mod.world;

import java.util.ArrayList;
import org.lwjgl.opengl.GL11;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.annotations.IncludeGUI;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventBlockClick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.RenderUtils;
import me.lordpankake.aeon.util.TimerUtils;
import me.lordpankake.aeon.util.other.BlockPlaceholder;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C0APacketAnimation;
import net.minecraft.util.MathHelper;

public class Nuker extends Module
{
	@AIgnore
	private int bestSlot;
	@AIgnore
	private ArrayList<BlockPlaceholder> nukeList = new ArrayList<BlockPlaceholder>();
	@AIgnore
	public static String idString = "stone";
	@AIgnore
	private final NanoTimerUtils timer = new NanoTimerUtils();
	@IncludeGUI(min = 0.1F, max = 15F)
	public float speed = 5.5F;
	@IncludeGUI(min = 0.1F, max = 15F)
	public static float radius = 5.2f;
	public static String mode = "tick";// click//tick/id

	public Nuker(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		nukeList.clear();
	}

	@Override
	public void onDisable()
	{
		nukeList.clear();
	}

	@EventTarget
	@Override
	public void onBlockClick(EventBlockClick event)
	{
		nukeList.clear();
		addBlocksToNukeList(event);
		idString = mc.theWorld.getBlock(event.x, event.y, event.z).textureName;
		if (mc.thePlayer.capabilities.isCreativeMode)
		{
			if (mode.contains("click"))
			{
				creativeNuke();
			}
		}
	}

	private void addBlocksToNukeList(EventBlockClick event)
	{
		int intRad = 2;// (int) radius;
		int intRad2 = 2;// (int) radius;
		for (int y = intRad; y >= -intRad; y--)
		{
			for (int z = -intRad2; z <= intRad2; z++)
			{
				for (int x = -intRad2; x <= intRad2; x++)
				{
					final int xPos = (int) Math.round(event.x + x);
					final int yPos = (int) Math.round(event.y + y);
					final int zPos = (int) Math.round(event.z + z);
					final BlockPlaceholder blahk = new BlockPlaceholder(xPos, yPos, zPos, mc.theWorld.getBlock(xPos, yPos, zPos), mc.theWorld.getBlock(xPos, yPos, zPos).textureName);
					if (blahk.getBlock().getMaterial() != Material.air)
					{
						nukeList.add(blahk);
					}
				}
			}
		}
	}

	@EventTarget
	@Override
	public void onGameTick(EventTick event)
	{
		// Add a timer to reduce lag?
		if (mc.thePlayer.capabilities.isCreativeMode)
		{
			if (mode.contains("tick"))
			{
				creativeNuke();
			}
		} else
		{
			survivalNuke();
		}
	}
	@AIgnore
	private int delay;

	private void survivalNuke()
	{
		ArrayList<BlockPlaceholder> temp = new ArrayList<BlockPlaceholder>();
		for (BlockPlaceholder b : nukeList)
		{
			float xDiff = (float) (mc.thePlayer.posX - b.getX());
			float yDiff = (float) (mc.thePlayer.posY - b.getY());
			float zDiff = (float) (mc.thePlayer.posZ - b.getZ());
			if (MathHelper.sqrt_float(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff) <= 3.5F)
			{
				delay++;
				final int bestSlot = findBestTool(b.getX(), b.getY(), b.getZ());
				if (bestSlot != -666)
				{
					mc.thePlayer.inventory.currentItem = bestSlot;
					this.bestSlot = bestSlot;
				}
				if (delay > this.speed * 10)// || true)
				{
					float diffX = xDiff;
					float diffY = yDiff + mc.thePlayer.getEyeHeight();
					float diffZ = zDiff;
					float dist = MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
					float yaw = (float) ((Math.atan2(diffZ, diffX) * 180.0D / 3.141592653589793D) - 90.0F);
					float pitch = (float) (Math.atan2(diffY, dist) * 180.0D / 3.141592653589793D);
					// mc.thePlayer.rotationPitch +=
					// MathHelper.wrapAngleTo180_float(pitch -
					// mc.thePlayer.rotationPitch);
					// mc.thePlayer.rotationYaw +=
					// MathHelper.wrapAngleTo180_float(yaw -
					// mc.thePlayer.rotationYaw);
					mc.playerController.curBlockDamageMP += b.getBlock().getBlockHardness(mc.theWorld, b.getX(), b.getY(), b.getZ());
					mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(0, b.getX(), b.getY(), b.getZ(), 0));
					mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(2, b.getX(), b.getY(), b.getZ(), 0));
					delay = 0;
				}
			}
			temp.add(b);
		}
		for (BlockPlaceholder b : temp)
		{
			if (b.isBlockNull())
			{
				nukeList.remove(b);
			}
		}
	}

	private BlockPlaceholder getBlockToNuke()
	{
		BlockPlaceholder returnBlack = null;
		for (BlockPlaceholder b : nukeList)
		{
			if (b.getBlock().getMaterial() != Material.air)
			{
				returnBlack = b;
			}
		}
		return returnBlack;
	}

	private void creativeNuke()
	{
		final int intRad = (int) radius;
		for (int y = intRad; y >= -intRad; y--)
		{
			for (int z = -intRad; z <= intRad; z++)
			{
				for (int x = -intRad; x <= intRad; x++)
				{
					final int xPos = (int) Math.round(Minecraft.getMinecraft().thePlayer.posX + x);
					final int yPos = (int) Math.round(Minecraft.getMinecraft().thePlayer.posY + y);
					final int zPos = (int) Math.round(Minecraft.getMinecraft().thePlayer.posZ + z);
					final BlockPlaceholder blahk = new BlockPlaceholder(xPos, yPos, zPos, mc.theWorld.getBlock(xPos, yPos, zPos), mc.theWorld.getBlock(xPos, yPos, zPos).textureName);
					if (Aeon.getInstance().getUtils().getBlock().getBlockID(blahk.getBlock()) != 0 && Aeon.getInstance().getUtils().getBlock().canReach(xPos, yPos, zPos, radius))
					{
						if (mode.contains("id"))
						{
							if (blahk.name.equalsIgnoreCase(idString))
							{
								mc.playerController.curBlockDamageMP += blahk.getBlock().getBlockHardness(mc.theWorld, blahk.getX(), blahk.getY(), blahk.getZ());
								mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(0, blahk.getX(), blahk.getY(), blahk.getZ(), 0));
								mc.playerController.curBlockDamageMP = 0.0F;
								mc.playerController.clickBlockCreative(mc, mc.playerController, blahk.getX(), blahk.getY(), blahk.getZ(), 0);

							}
						} else
						{
							mc.playerController.curBlockDamageMP += blahk.getBlock().getBlockHardness(mc.theWorld, blahk.getX(), blahk.getY(), blahk.getZ());
							mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(0, blahk.getX(), blahk.getY(), blahk.getZ(), 0));
							mc.playerController.curBlockDamageMP = 0.0F;
							mc.playerController.clickBlockCreative(mc, mc.playerController, blahk.getX(), blahk.getY(), blahk.getZ(), 0);

						}
					}
				}
			}
		}
	}

	private int findBestTool(int x, int y, int z)
	{
		int bestItem = -666;
		float strength = 1.0F;
		final Block block = this.mc.theWorld.getBlock(x, y, z);
		if (block.getMaterial() == Material.air)
		{
			return -666;
		}
		for (int index = 0; index < 9; index++)
		{
			try
			{
				final ItemStack item = mc.thePlayer.inventory.getStackInSlot(index);
				if (item != null && item.func_150997_a(block) > strength)
				{
					strength = item.func_150997_a(block);
					bestItem = index;
				}
			} catch (final Exception localException)
			{
			}
		}
		return bestItem;
	}
}