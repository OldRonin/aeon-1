package me.lordpankake.aeon.modules.mod.world;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;

public class PermanentTime extends Module
{
	@AIgnore
	public static long savedTime;

	public PermanentTime(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		savedTime = mc.theWorld.getWorldTime();
	}
}
