package me.lordpankake.aeon.modules.mod.render;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import me.lordpankake.aeon.util.RenderUtils;
import net.minecraft.block.material.Material;

public class Search extends Module
{
	public static int range = 20;
	public static float oreTransparency = 0.05f;
	public static float pureBlockTransparency = 0.15f;

	public Search(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		loadSettings();
	}

	@Override
	@EventTarget
	public void onRenderWorld(EventRenderWorld event)
	{
		for (int x = range; x > -range; x--)
		{
			for (int y = range; y > -range; y--)
			{
				for (int z = range; z > -range; z--)
				{
					final double d1 = mc.thePlayer.posX + x;
					final double d3 = mc.thePlayer.posY + y;
					final double d5 = mc.thePlayer.posZ + z;
					final int xx = (int) d1 - 2;
					final int yy = (int) d3;
					final int zz = (int) d5 - 2;
					if (mc.theWorld.getBlock(xx, yy, zz).getMaterial() != Material.air)
					{
						final SearchBlock block = new SearchBlock(xx, yy, zz, mc.theWorld.getBlock(xx, yy, zz).textureName);
						if (block.name == "diamond_block")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.1F, 0.5F, 1.0F, pureBlockTransparency);
						}
						if (block.name == "emerald_block")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.1F, 0.8F, 0.1F, pureBlockTransparency);
						}
						if (block.name == "gold_block")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.8F, 0.8F, 0.0F, pureBlockTransparency);
						}
						if (block.name == "redstone_block")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.9F, 0.1F, 0.0F, pureBlockTransparency);
						}
						if (block.name == "iron_block")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.8F, 0.8F, 0.8F, pureBlockTransparency);
						}
						if (block.name == "lapis_block")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.0F, 0.0F, 0.7F, pureBlockTransparency);
						}
						// ///////////////////////////////////////////////////
						if (block.name == "diamond_ore")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.2F, 0.7F, 1.0F, oreTransparency);
						}
						if (block.name == "emerald_ore")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.1F, 0.8F, 0.1F, oreTransparency);
						}
						if (block.name == "emerald_ore")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.1F, 0.8F, 0.1F, oreTransparency);
						}
						if (block.name == "quartz_ore")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.8F, 0.7F, 0.7F, oreTransparency);
						}
						if (block.name == "redstone_ore")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.8F, 0.1F, 0.1F, oreTransparency);
						}
						if (block.name == "lapis_ore")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.0F, 0.0F, 0.7F, oreTransparency);
						}
						if (block.name == "iron_ore")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.8F, 0.8F, 0.8F, oreTransparency);
						}
						if (block.name == "gold_ore")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.8F, 0.8F, 0.0F, oreTransparency);
						}
						if (block.name == "obsidian")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.5F, 0.1F, 0.5F, oreTransparency + 0.02f);
						}
						if (block.name == "coal_ore")
						{
							RenderUtils.drawBlockESP(block.x, block.y, block.z, 0.2F, 0.2F, 0.2F, oreTransparency + 0.02f);
						}
					}
				}
			}
		}
	}
	class SearchBlock
	{
		private final int x;
		private final int y;
		private final int z;
		private final String name;

		public SearchBlock(int x, int y, int z, String name)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.name = name;
		}
	}
}
