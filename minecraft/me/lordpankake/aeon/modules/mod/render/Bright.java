package me.lordpankake.aeon.modules.mod.render;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventManager;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;

public class Bright extends Module
{
	@AIgnore
	private float savedBrightness;

	public Bright(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onDisable()
	{
		mc.theWorld.provider.generateLightBrightnessTable();
	}

	@Override
	public void onEnable()
	{
		for (int i = mc.theWorld.provider.lightBrightnessTable.length - 1; i >= 0; i--)
		{
			mc.theWorld.provider.lightBrightnessTable[i] += 0.34F;
		}
	}
}
