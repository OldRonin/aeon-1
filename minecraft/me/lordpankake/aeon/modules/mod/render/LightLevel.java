package me.lordpankake.aeon.modules.mod.render;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import me.lordpankake.aeon.util.BlockItemUtils;
import me.lordpankake.aeon.util.RenderUtils;
import net.minecraft.block.material.Material;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.chunk.Chunk;

public class LightLevel extends Module
{
	public static float radius = 7f;
	public float red = 1F;
	public float green = 0.1F;
	public float blue = 0.1F;
	public float transparency = 0.08F;

	public LightLevel(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		loadSettings();
	}

	@EventTarget
	@Override
	public void onRenderWorld(EventRenderWorld event)
	{
		final BlockItemUtils util = Aeon.getInstance().getUtils().getBlock();
		final int range = (int) radius;
		for (int x = range; x > -range; x--)
		{
			for (int y = range; y > -range; y--)
			{
				for (int z = range; z > -range; z--)
				{
					final double d1 = mc.thePlayer.posX + x;
					final double d3 = mc.thePlayer.posY + y;
					final double d5 = mc.thePlayer.posZ + z;
					final int xx = (int) d1 - 2;
					final int yy = (int) d3;
					final int zz = (int) d5 - 2;
					if (util.canReach(xx, yy, zz, 7F))
					{
						final Chunk chnk = this.mc.theWorld.getChunkFromBlockCoords(xx, zz);
						if (mc.thePlayer.posY > 3 && chnk.getSavedLightValue(EnumSkyBlock.Block, xx & 15, yy + 1, zz & 15) <= 7 && mc.theWorld.getBlock(xx, yy + 1, zz).getMaterial() == Material.air && mc.theWorld.getBlock(xx, yy, zz).getMaterial() != Material.air)
						{
							RenderUtils.drawLightLevel(xx, yy, zz, red, green, blue, transparency);
						}
					}
				}
			}
		}
	}
}
