package me.lordpankake.aeon.modules.mod.render;

import java.util.ArrayList;
import java.util.List;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.RenderUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import net.minecraft.client.renderer.entity.RenderManager;

public class Trail extends Module
{
	@AIgnore
	private final List<Vector3f> vertexes = new ArrayList();
	@AIgnore
	private int movementCount = 0;

	public Trail(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	public void clearTrail()
	{
		vertexes.clear();
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		final Vector3f position = new Vector3f((float) mc.thePlayer.posX, (float) mc.thePlayer.posY, (float) mc.thePlayer.posZ);
		if (this.vertexes.isEmpty())
		{
			this.vertexes.add(position);
		} else
		{
			final Vector3f lastPosition = this.vertexes.get(this.vertexes.size() - 1);
			final double diffX = mc.thePlayer.prevPosX - mc.thePlayer.posX;
			final double diffY = mc.thePlayer.prevPosY - mc.thePlayer.posY;
			final double diffZ = mc.thePlayer.prevPosZ - mc.thePlayer.posZ;
			if (diffX != 0.0D || diffY != 0.0D || diffZ != 0.0D)
			{
				this.movementCount += 1;
				if (this.movementCount >= 5)
				{
					this.vertexes.add(position);
					this.movementCount = 0;
				}
			}
		}
	}

	@Override
	@EventTarget
	public void onRenderWorld(EventRenderWorld event)
	{
		RenderUtils.beginSmoothLine();
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glBegin(GL11.GL_LINE_STRIP);
		GL11.glColor4f(0.4F, 0.6F, 0.9F, 0.8F);
		GL11.glVertex3d(0.0D, -mc.thePlayer.height, 0.0D);
		for (int i = this.vertexes.size() - 1; i >= 0; i--)
		{
			final Vector3f vertex = this.vertexes.get(i);
			GL11.glVertex3d(vertex.x - RenderManager.renderPosX, vertex.y - RenderManager.renderPosY - mc.thePlayer.height / 2, vertex.z - RenderManager.renderPosZ);
		}
		GL11.glEnd();
		RenderUtils.endSmoothLine();
	}
}
