package me.lordpankake.aeon.modules.mod.render.overlay;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2d;
import java.awt.Color;
import org.lwjgl.opengl.GL11;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeDraw;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeGUI;
import me.lordpankake.aeon.util.RenderUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;

public class Armor extends Module
{
	@AIgnore
	private final RenderItem itemRenderer = new RenderItem();

	public Armor(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (mc.currentScreen != null && !(mc.currentScreen instanceof GuiChat))
		{
			return;
		}
		final int height = Aeon.getInstance().getUtils().getuScreen().getScreenHeight();
		int y = 75;
		int x = 1;
		if (mc.currentScreen instanceof GuiChat)
		{
			final short var1 = 320;
			final byte var2 = 40;
			x = MathHelper.floor_float(mc.gameSettings.chatWidth * (var1 - var2) + var2) + 6;
			y += 10;
		}
		for (int i = 3; i > -1; i--)
		{
			final ItemStack is = mc.thePlayer.inventory.armorInventory[i];
			if (is != null)
			{
				draw(is, x, height - y);
			}
			y -= 15;
		}
	}

	public void draw(ItemStack is, int x1, int y1)
	{
		double x = x1;
		double y = y1;
		double maxDamage = is.getMaxDamage() + 1;
		double damage = maxDamage - is.getItemDamage();
		GL11.glDisable(GL11.GL_LIGHTING);
		double sliderPercentage = (damage / maxDamage);
		PankakeDraw.enableNoTextures();
		RenderUtils.setColor(new Color(22, 22, 22, 100));
		// Log.write(maxDamage + ":" + damage + "//" + (damage / maxDamage)) ;
		x = x + 19;
		y = y + 4;
		glBegin(GL_QUADS);
		{
			glVertex2d(x - 18, y - 3);
			glVertex2d(x + 44, y - 3);
			glVertex2d(x + 44, y + 12);
			glVertex2d(x - 18, y + 12);
		}
		glEnd();
		RenderUtils.setColor(new Color(55, 55, 55, 100));
		glBegin(GL_QUADS);
		{
			glVertex2d(x, y );
			glVertex2d(x + 41, y);
			glVertex2d(x + 41, y + 10);
			glVertex2d(x , y + 10);
		}
		glEnd();
		RenderUtils.setColor(new Color((int) (255 - (sliderPercentage * 200)), (int) (sliderPercentage * 200), 0, 200));
		y = y - 0.5;
		glBegin(GL_QUADS);
		{
			glVertex2d(x, y);
			glVertex2d(x + (sliderPercentage * 40) + 1, y);
			glVertex2d(x + (sliderPercentage * 40) + 1, y + 10);
			glVertex2d(x, y + 10);
		}
		glEnd();
		PankakeDraw.disableNoTextures();
		
		x = x - 17;
		y = y - 3.5;
		itemRenderer.renderItemIntoGUI(mc.fontRenderer, mc.getTextureManager(), new ItemStack(is.getItem(), 1, 0), (int)x, (int)y);
		final FontRenderer fr = mc.fontRenderer;
		
		x = x + 17;
		y = y + 4;
		String text = damage + "/" + maxDamage;
		GL11.glDisable(GL11.GL_LIGHTING);
		fr.drawStringWithShadow(text.replace(".0", ""), (int)x , (int)y, 0xffffff);
		GL11.glEnable(GL11.GL_LIGHTING);
	}
}
