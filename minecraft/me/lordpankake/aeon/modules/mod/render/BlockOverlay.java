package me.lordpankake.aeon.modules.mod.render;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import me.lordpankake.aeon.util.RenderUtils;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockOverlay extends Module
{
	public float transparency = 0.25F;
	public float red = 0.1F;
	public float green = 0.5F;
	public float blue = 1.0F;

	public BlockOverlay(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		loadSettings();
	}

	@Override
	@EventTarget
	public void onRenderWorld(EventRenderWorld event)
	{
		final Block block = mc.theWorld.getBlock(mc.objectMouseOver.blockX, mc.objectMouseOver.blockY, mc.objectMouseOver.blockZ);
		if (block.getMaterial() != Material.air)
		{
		//	final int blockX = (int) mc.thePlayer.lastTickPosX - (int) (mc.thePlayer.posX - mc.thePlayer.lastTickPosX);
		//	final int blockY = (int) mc.thePlayer.lastTickPosY - (int) (mc.thePlayer.posY - mc.thePlayer.lastTickPosY);
		//	final int blockZ = (int) mc.thePlayer.lastTickPosZ - (int) (mc.thePlayer.posZ - mc.thePlayer.lastTickPosZ);
			RenderUtils.drawBlockESP(mc.objectMouseOver.blockX, mc.objectMouseOver.blockY, mc.objectMouseOver.blockZ, 0.1F, 0.5F, 1.0F, transparency);
		}
	}
}
