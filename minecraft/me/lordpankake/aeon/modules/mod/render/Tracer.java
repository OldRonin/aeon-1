package me.lordpankake.aeon.modules.mod.render;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import me.lordpankake.aeon.util.RenderUtils;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;

public class Tracer extends Module
{
	public static String mode = "player";
	@AIgnore
	private float partialTicks;
	@AIgnore
	private double x;
	@AIgnore
	private double y;
	@AIgnore
	private double z;

	public Tracer(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		loadSettings();
	}

	@Override
	@EventTarget
	public void onRenderWorld(EventRenderWorld event)
	{
		if (mode.contains("player"))
		{
			for (final EntityPlayer player : Aeon.getInstance().getUtils().getEntity().getPlayers())
			{
				if (player != mc.thePlayer && !player.isDead)
				{
					final float renderX = (float) (this.interpolate(player.prevPosX, player.posX) - RenderManager.renderPosX);
					final float renderY = (float) (this.interpolate(player.prevPosY, player.posY) - RenderManager.renderPosY);
					final float renderZ = (float) (this.interpolate(player.prevPosZ, player.posZ) - RenderManager.renderPosZ);
					final float renderWidth = player.width / 1.5F;
					final float renderHeight = player.height;
					final float distance = mc.thePlayer.getDistanceToEntity(player);
					final boolean isFriend = Aeon.getInstance().getUtils().getFriend().containsFriend(player.getCommandSenderName());
					Aeon.getInstance().getUtils().getRender();
					RenderUtils.beginSmoothLine();
					if (isFriend)
					{
						GL11.glColor3f(0.0F + distance / 10.0F, 0.4F + distance / 16.0F, 0.8F);
					} else
					{
						GL11.glColor3f(0.0F + distance / 16.0F, 0.0F + distance / 16.0F, 0.8F);
					}
					GL11.glBegin(1);
					GL11.glVertex3d(0.0D, 0.0D, 0.0D);
					GL11.glVertex3d(renderX, renderY + 1, renderZ);
					GL11.glEnd();
					Aeon.getInstance().getUtils().getRender();
					RenderUtils.endSmoothLine();
				}
			}
		}
		if (mode.contains("mob"))
		{
			for (final EntityLivingBase entity : Aeon.getInstance().getUtils().getEntity().getLivingEntities())
			{
				if (entity != mc.thePlayer && !entity.isDead && !(entity instanceof EntityOtherPlayerMP))
				{
					final float renderX = (float) (this.interpolate(entity.prevPosX, entity.posX) - RenderManager.renderPosX);
					final float renderY = (float) (this.interpolate(entity.prevPosY, entity.posY) - RenderManager.renderPosY);
					final float renderZ = (float) (this.interpolate(entity.prevPosZ, entity.posZ) - RenderManager.renderPosZ);
					final float renderWidth = entity.width / 1.5F;
					final float renderHeight = entity.height;
					final float distance = mc.thePlayer.getDistanceToEntity(entity);
					final boolean isFriend = Aeon.getInstance().getUtils().getFriend().containsFriend(entity.getCommandSenderName());
					Aeon.getInstance().getUtils().getRender();
					RenderUtils.beginSmoothLine();
					if (isFriend)
					{
						GL11.glColor3f(0.0F + distance / 16.0F, 0.4F + distance / 16.0F, 0.8F);
					} else
					{
						GL11.glColor3f(0.0F + distance / 16.0F, 0.0F + distance / 16.0F, 0.8F);
					}
					GL11.glBegin(1);
					GL11.glVertex3d(0.0D, 0.0D, 0.0D);
					GL11.glVertex3d(renderX, renderY + 0.75f, renderZ);
					GL11.glEnd();
					Aeon.getInstance().getUtils().getRender();
					RenderUtils.endSmoothLine();
				}
			}
		}
	}

	public double interpolate(double prev, double cur)
	{
		return prev + (cur - prev) * partialTicks;
	}
}
