package me.lordpankake.aeon.modules.mod.render;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import me.lordpankake.aeon.util.RenderUtils;
import org.lwjgl.opengl.GL11;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;

public class Trajectories extends Module
{
	@AIgnore
	public double pX = -9000, pY = -9000, pZ = -9000;

	public Trajectories(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onRenderWorld(EventRenderWorld event)
	{
		final ItemStack is = mc.thePlayer.inventory.getCurrentItem();
		if (is != null)
		{
			final String id = is.getItem().getUnlocalizedName().toLowerCase();
			boolean drawPrediction = true;
			float var4 = 0.05F;
			float var5 = 3.2F;
			float var6 = 0.0F;
			final boolean var7 = false;
			if (id.contains("bow") && !id.contains("bowl"))
			{
				var4 = 0.05F;
				var5 = 3.2F;
			} else if (id.contains("enderpearl"))
			{
				var4 = 0.03F;
				var5 = 1.5F;
			} else if (id.contains("snowball"))
			{
				var4 = 0.03F;
				var5 = 1.5F;
			} else if (id.contains("fishingrod"))
			{
				var5 = 1.5F;
			} else if (id.contains("egg") && !id.contains("monsterstoneegg"))
			{
				var4 = 0.03F;
				var5 = 1.5F;
			} else if ((is.getItemDamage() & 0x4000) > 0)
			{
				var4 = 0.05F;
				var5 = 0.5F;
				var6 = -20F;
			} else
			{
				drawPrediction = false;
			}
			if (drawPrediction)
			{
				final AxisAlignedBB var9 = AxisAlignedBB.getBoundingBox(0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
				final float var10 = (float) mc.thePlayer.posX;
				final float var11 = (float) mc.thePlayer.posY;
				final float var12 = (float) mc.thePlayer.posZ;
				final float var13 = mc.thePlayer.rotationYaw;
				final float var14 = mc.thePlayer.rotationPitch;
				final float var15 = var13 / 180.0F * (float) Math.PI;
				final float var16 = MathHelper.sin(var15);
				final float var17 = MathHelper.cos(var15);
				final float var18 = var14 / 180.0F * (float) Math.PI;
				final float var19 = MathHelper.cos(var18);
				final float var20 = (var14 + var6) / 180.0F * (float) Math.PI;
				final float var21 = MathHelper.sin(var20);
				float var22 = var10 - var17 * 0.16F;
				float var23 = var11 - 0.1F;
				float var24 = var12 - var16 * 0.16F;
				float var25 = -var16 * var19 * 0.4F;
				float var26 = -var21 * 0.4F;
				float var27 = var17 * var19 * 0.4F;
				final float var28 = MathHelper.sqrt_double(var25 * var25 + var26 * var26 + var27 * var27);
				var25 /= var28;
				var26 /= var28;
				var27 /= var28;
				var25 *= var5;
				var26 *= var5;
				var27 *= var5;
				float var29 = var22;
				float var30 = var23;
				float var31 = var24;
				GL11.glPushMatrix();
				GL11.glColor4f(0.25F, 0.65F, 0.95F, 0.7F);
				Aeon.getInstance().getUtils().getRender();
				RenderUtils.beginSmoothLine();
				GL11.glBegin(GL11.GL_LINES);
				int var32 = 0;
				boolean var33 = true;
				while (var33)
				{
					++var32;
					var29 += var25;
					var30 += var26;
					var31 += var27;
					final Vec3 var34 = Vec3.createVectorHelper(var22, var23, var24);
					final Vec3 var35 = Vec3.createVectorHelper(var29, var30, var31);
					final MovingObjectPosition var8 = mc.theWorld.func_147447_a(var34, var35, false, true, false);
					if (var8 != null)
					{
						var22 = (float) var8.hitVec.xCoord;
						var23 = (float) var8.hitVec.yCoord;
						var24 = (float) var8.hitVec.zCoord;
						var33 = false;
					} else if (var32 > 200)
					{
						var33 = false;
					} else
					{
						this.drawLine3D(var22 - var10, var23 - var11, var24 - var12, var29 - var10, var30 - var11, var31 - var12);
						if (var7)
						{
							var9.setBounds(var22 - 0.125F, var23, var24 - 0.125F, var22 + 0.125F, var23 + 0.25F, var24 + 0.125F);
							var4 = 0.0F;
							float var37;
							for (int var36 = 0; var36 < 5; ++var36)
							{
								var37 = (float) (var9.minY + (var9.maxY - var9.minY) * var36 / 5.0D);
								final float var38 = (float) (var9.minY + (var9.maxY - var9.minY) * (var36 + 1) / 5.0D);
								final AxisAlignedBB var39 = AxisAlignedBB.getBoundingBox(var9.minX, var37, var9.minZ, var9.maxX, var38, var9.maxZ);
								if (mc.theWorld.isAABBInMaterial(var39, Material.water))
								{
									var4 += 0.2F;
								}
							}
							final float var40 = var4 * 2.0F - 1.0F;
							var26 += 0.04F * var40;
							var37 = 0.92F;
							if (var4 > 0.0F)
							{
								var37 *= 0.9F;
								var26 *= 0.8F;
							}
							var25 *= var37;
							var26 *= var37;
							var27 *= var37;
						} else
						{
							var25 *= 0.99F;
							var26 *= 0.99F;
							var27 *= 0.99F;
							var26 -= var4;
						}
						var22 = var29;
						var23 = var30;
						var24 = var31;
					}
					if (!var33)
					{
						pX = var22;
						pY = var23;
						pZ = var24;
						GL11.glVertex3d(pX - var10 - 0.1F, pY - var11, pZ - var12 - 0.1F);
						GL11.glVertex3d(pX - var10 - 0.1F, pY - var11, pZ - var12 + 0.1F);
						GL11.glVertex3d(pX - var10 + 0.1F, pY - var11, pZ - var12 + 0.1F);
						GL11.glVertex3d(pX - var10 + 0.1F, pY - var11, pZ - var12 - 0.1F);
						GL11.glVertex3d(pX - var10 + 0.1F, pY - var11, pZ - var12 + 0.1F);
						GL11.glVertex3d(pX - var10 - 0.1F, pY - var11, pZ - var12 + 0.1F);
						GL11.glVertex3d(pX - var10 - 0.1F, pY - var11, pZ - var12 - 0.1F);
						GL11.glVertex3d(pX - var10 + 0.1F, pY - var11, pZ - var12 - 0.1F);
						GL11.glVertex3d(pX - var10 - 0.1F, pY - var11, pZ - var12 - 0.1F);
						/*
						 * GL11.glVertex3d(pX - var10 + 0.5F, pY - var11, pZ -
						 * var12 + 0.5F); GL11.glVertex3d(pX - var10 - 0.5F, pY
						 * - var11, pZ - var12 + 0.5F); GL11.glVertex3d(pX -
						 * var10 + 0.5F, pY - var11, pZ - var12 - 0.5F);
						 */
					} else
					{
					}
				}
				GL11.glEnd();
				Aeon.getInstance().getUtils().getRender();
				RenderUtils.endSmoothLine();
				GL11.glPopMatrix();
			}
		}
	}

	public void drawLine3D(float var1, float var2, float var3, float var4, float var5, float var6)
	{
		GL11.glVertex3d(var1, var2, var3);
		GL11.glVertex3d(var4, var5, var6);
	}
}
