package me.lordpankake.aeon.modules.mod.render;

import java.util.ArrayList;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;

public class Xray extends Module
{
	public static int opacity = 130;// 130+ or Block.java method
	@AIgnore
	private float savedBrightness;
	public static ArrayList blockList = new ArrayList<String>();

	public Xray(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
		populateBlockList();
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (!Aeon.getInstance().getUtils().getHack().getLoader().bright.getState())
		{
			mc.gameSettings.gammaSetting = 8f;
		}
	}

	@Override
	public void onEnable()
	{
		loadSettings();
		mc.renderGlobal.loadRenderers();
		if (!Aeon.getInstance().getUtils().getHack().getLoader().bright.getState())
		{
			if (mc.gameSettings.gammaSetting < 1.0f)
			{
				savedBrightness = mc.gameSettings.gammaSetting;
			} else
			{
				savedBrightness = 1f;
			}
		}
	}

	@Override
	public void onDisable()
	{
		mc.renderGlobal.loadRenderers();
		if (!Aeon.getInstance().getUtils().getHack().getLoader().bright.getState())
		{
			mc.gameSettings.gammaSetting = savedBrightness;
		}
	}

	private void populateBlockList()
	{
		blockList.add("water_flow");
		blockList.add("water_still");
		blockList.add("lava_flow");
		blockList.add("lava_still");
		blockList.add("gold_ore");
		blockList.add("iron_ore");
		blockList.add("coal_ore");
		blockList.add("lapis_ore");
		blockList.add("blockLapis");
		blockList.add("gold_block");
		blockList.add("lapis_block");
		blockList.add("iron_block");
		blockList.add("obsidian");
		blockList.add("redstone_block");
		blockList.add("coal_block");
		blockList.add("mob_spawner");
		blockList.add("cobblestone_mossy");
		blockList.add("diamond_block");
		blockList.add("diamond_ore");
		blockList.add("redstone_ore");
		blockList.add("coal_ore");
		blockList.add("quartz_ore");
		blockList.add("repeater_on");
		blockList.add("repeater_off");
		blockList.add("enchanting_table");
		blockList.add("endframe");
		blockList.add("redstone_lamp_on");
		blockList.add("redstone_lamp_off");
		blockList.add("dragon_egg");
		blockList.add("command_block");
		blockList.add("emerald_block");
		blockList.add("emerald_ore");
		blockList.add("anvil");
		blockList.add("quartz_ore");
	}
}
