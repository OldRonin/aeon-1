package me.lordpankake.aeon.modules.mod.render;

import java.util.Iterator;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import me.lordpankake.aeon.util.RenderUtils;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.AxisAlignedBB;

public class ChestESP extends Module
{
	public ChestESP(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onRenderWorld(EventRenderWorld event)
	{
		final RenderUtils util = Aeon.getInstance().getUtils().getRender();
		for (final Iterator localIterator = mc.theWorld.field_147482_g.iterator(); localIterator.hasNext();)
		{
			final Object o = localIterator.next();
			if (o instanceof TileEntityChest)
			{
				final TileEntityChest chest = (TileEntityChest) o;
				final double renderX = chest.field_145851_c - RenderManager.renderPosX;
				final double renderY = chest.field_145848_d - RenderManager.renderPosY;
				final double renderZ = chest.field_145849_e - RenderManager.renderPosZ;
				Aeon.getInstance().getUtils().getRender();
				RenderUtils.beginSmoothLine();
				GL11.glPushMatrix();
				GL11.glTranslated(renderX, renderY, renderZ);
				GL11.glColor3f(0.2F, 0.7F, 1.0F);
				if (chest.field_145990_j != null)
				{
					final AxisAlignedBB boundingBox = AxisAlignedBB.getBoundingBox(0.0D, 0.0D, 0.0D, 2.0D, 1.0D, 1.0D);
					RenderUtils.drawOutlinedBox(boundingBox);
					GL11.glColor4f(0.1F, 0.6F, 1.0F, 0.1F);
					RenderUtils.drawBox(boundingBox);
				} else if (chest.field_145988_l != null)
				{
					final AxisAlignedBB boundingBox = AxisAlignedBB.getBoundingBox(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 2.0D);
					RenderUtils.drawOutlinedBox(boundingBox);
					GL11.glColor4f(0.1F, 0.6F, 1.0F, 0.1F);
					RenderUtils.drawBox(boundingBox);
				} else if (chest.field_145990_j == null && chest.field_145988_l == null && chest.field_145991_k == null && chest.field_145992_i == null)
				{
					final AxisAlignedBB boundingBox = AxisAlignedBB.getBoundingBox(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
					RenderUtils.drawOutlinedBox(boundingBox);
					GL11.glColor4f(0.1F, 0.6F, 1.0F, 0.1F);
					RenderUtils.drawBox(boundingBox);
				}
				GL11.glPopMatrix();
				Aeon.getInstance().getUtils().getRender();
				RenderUtils.endSmoothLine();
			}
		}
	}
}
