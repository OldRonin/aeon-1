package me.lordpankake.aeon.modules.mod.render;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import me.lordpankake.aeon.util.FileUtils;
import me.lordpankake.aeon.util.RenderUtils;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.AxisAlignedBB;

public class Waypoints extends Module
{
	@AIgnore
	public ArrayList waypoints = new ArrayList<Waypoint>();
	public boolean drawTracer;

	public Waypoints(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		loadWaypoints();
	}

	@Override
	@EventTarget
	public void onRenderWorld(EventRenderWorld event)
	{
		final RenderUtils util = Aeon.getInstance().getUtils().getRender();
		for (final Waypoint waypoint : this.getWaypoints())
		{
			if (waypoint.isOnServer() && waypoint.shouldRender())
			{
				final float distance = (float) mc.thePlayer.getDistance(waypoint.getX(), waypoint.getY(), waypoint.getZ());
				float renderCutoff = distance / 10.0F;
				if (renderCutoff > 1.0F)
				{
					renderCutoff = 1.0F;
				}
				final double renderX = waypoint.getX() - RenderManager.renderPosX;
				final double renderY = waypoint.getY() - RenderManager.renderPosY - 0.5;
				final double renderZ = waypoint.getZ() - RenderManager.renderPosZ;
				final double renderSize = 1.0D;
				GL11.glColor4f(waypoint.getColor().getRed() / 255.0F, waypoint.getColor().getGreen() / 255.0F, waypoint.getColor().getBlue() / 255.0F, 0.6F * renderCutoff);
				final AxisAlignedBB box = AxisAlignedBB.getBoundingBox(renderX - renderSize / 2.0D, renderY - renderSize / 2.0D, renderZ - renderSize / 2.0D, renderX + renderSize / 2.0D, renderY + renderSize / 2.0D, renderZ + renderSize / 2.0D);
				GL11.glLineWidth(1.5F);
				Aeon.getInstance().getUtils().getRender();
				RenderUtils.beginSmoothLine();
				if (this.drawTracer)
				{
					GL11.glBegin(1);
					GL11.glVertex3d(0.0D, 0.0D, 0.0D);
					GL11.glVertex3d(renderX, renderY, renderZ);
					GL11.glEnd();
				}
				RenderUtils.drawOutlinedBox(box);
				GL11.glColor4f(waypoint.getColor().getRed() / 255.0F, waypoint.getColor().getGreen() / 255.0F, waypoint.getColor().getBlue() / 255.0F, 0.4F * renderCutoff);
				RenderUtils.drawBox(box);
				Aeon.getInstance().getUtils().getRender();
				RenderUtils.endSmoothLine();
				GL11.glPushMatrix();
				final float scale = 4.0F;
				if (distance / scale > 1.0F)
				{
					GL11.glScaled(distance / scale, distance / scale, distance / scale);
				}
				RenderUtils.draw3DStringCentered(mc.fontRenderer, waypoint.getName() + " [" + (int) distance + "]", (float) renderX, (float) renderY + 0.3F + (float) renderSize / 2.0F, (float) renderZ, 16777215);
				GL11.glPopMatrix();
			}
		}
	}

	public void add(String waypointName)
	{
		final Waypoint point = new Waypoint(waypointName, mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ);
		for (final Waypoint ways : getWaypoints())
		{
			if (ways.getName().contains(waypointName))
			{
				return;
			}
		}
		waypoints.add(point);
		saveWaypoints();
	}

	public void remove(String waypoint)
	{
	}

	public void clearPoints()
	{
		waypoints.clear();
		saveWaypoints();
	}

	public List<Waypoint> getWaypoints()
	{
		return waypoints;
	}

	public void saveWaypoints()
	{
		final File saveFile = new File("Aeon" + File.separator + "Hacks" + File.separator + "waypoints.txt");
		final ArrayList<String> content = new ArrayList<String>();
		for (final Waypoint way : getWaypoints())
		{
			content.add(way.getName() + ":" + way.getServer() + ":" + way.getX() + ":" + way.getY() + ":" + way.getZ());
		}
		content.add("RenderTracer:" + drawTracer);
		FileUtils.write(saveFile, content, true);
	}

	public void loadWaypoints()
	{
		final File settings = new File("Aeon" + File.separator + "Hacks" + File.separator + "waypoints.txt");
		Aeon.getInstance().getUtils().getFile();
		final ArrayList<String> content = (ArrayList<String>) FileUtils.read(settings);
		if (waypoints.size() <= 0)
		{
			for (final String s : content)
			{
				final String info[] = s.split(":");
				if (s.startsWith("RenderTracer"))
				{
					if (info[1].toLowerCase().contains("true"))
					{
						drawTracer = true;
					}
					if (info[1].toLowerCase().contains("false"))
					{
						drawTracer = false;
					}
				} else
				{
					double x = 0;
					double y = 0;
					double z = 0;
					try
					{
						x = Double.parseDouble(info[2]);
						y = Double.parseDouble(info[3]);
						z = Double.parseDouble(info[4]);
					} catch (final Exception e)
					{
						e.printStackTrace();
					}
					final Waypoint point = new Waypoint(info[0], info[1], x, y, z);
					waypoints.add(point);
				}
			}
		}
	}
	public class Waypoint
	{
		private final String server;
		private String name;
		private double x;
		private double y;
		private double z;
		private Color color;
		private boolean render = true;

		public Waypoint(String name, String server, double x, double y, double z, Color color)
		{
			this.name = name;
			this.x = x;
			this.y = y;
			this.z = z;
			this.color = color;
			this.server = server;
		}

		public Waypoint(String name, String server, double x, double y, double z)
		{
			this(name, mc.isSingleplayer() ? mc.getIntegratedServer().getWorldName() : mc.currentServerData.serverIP, x, y, z, Color.CYAN);
		}

		public Waypoint(String name, double x, double y, double z, Color color)
		{
			this(name, mc.isSingleplayer() ? mc.getIntegratedServer().getWorldName() : mc.currentServerData.serverIP, x, y, z, color);
		}

		public Waypoint(String name, double x, double y, double z)
		{
			this(name, x, y, z, Color.CYAN);
		}

		public String getName()
		{
			return this.name;
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public Color getColor()
		{
			return this.color;
		}

		public void setColor(Color color)
		{
			this.color = color;
		}

		public double getX()
		{
			return this.x;
		}

		public void setX(double x)
		{
			this.x = x;
		}

		public double getY()
		{
			return this.y;
		}

		public void setY(double y)
		{
			this.y = y;
		}

		public double getZ()
		{
			return this.z;
		}

		public void setZ(double z)
		{
			this.z = z;
		}

		public boolean shouldRender()
		{
			return this.render;
		}

		public void setRender(boolean render)
		{
			this.render = render;
		}

		public String getServer()
		{
			return this.server;
		}

		public boolean isOnServer()
		{
			if (mc.isSingleplayer())
			{
				return mc.getIntegratedServer().getWorldName().equals(this.server);
			}
			return mc.currentServerData.serverIP.equals(this.server);
		}
	}

	public boolean useTracer()
	{
		return this.drawTracer;
	}
}
