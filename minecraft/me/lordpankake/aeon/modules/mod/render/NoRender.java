package me.lordpankake.aeon.modules.mod.render;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;

public class NoRender extends Module
{
	public NoRender(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}
}
