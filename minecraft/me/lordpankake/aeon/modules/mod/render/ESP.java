package me.lordpankake.aeon.modules.mod.render;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import me.lordpankake.aeon.util.RenderUtils;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;

public class ESP extends Module
{
	public static String mode = "player";// player/mob
	@AIgnore
	private float partialTicks;
	@AIgnore
	private double x;
	@AIgnore
	private double y;
	@AIgnore
	private double z;

	public ESP(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		loadSettings();
	}

	@Override
	@EventTarget
	public void onRenderWorld(EventRenderWorld event)
	{
		if (mode.contains("player"))
		{
			for (final EntityPlayer player : Aeon.getInstance().getUtils().getEntity().getPlayers())
			{
				if (player != mc.thePlayer && !player.isDead)
				{
					final float renderX = (float) (this.interpolate(player.prevPosX, player.posX) - RenderManager.renderPosX);
					final float renderY = (float) (this.interpolate(player.prevPosY, player.posY) - RenderManager.renderPosY);
					final float renderZ = (float) (this.interpolate(player.prevPosZ, player.posZ) - RenderManager.renderPosZ);
					final float renderWidth = player.width / 1.5F;
					final float renderHeight = player.height;
					final float distance = mc.thePlayer.getDistanceToEntity(player);
					final boolean isFriend = Aeon.getInstance().getUtils().getFriend().containsFriend(player.getCommandSenderName());
					if (isFriend)
					{
						GL11.glColor4f(0.0F + distance / 16.0F, 1.0F + distance / 16.0F, 0.8F, 0.7F);
					} else
					{
						GL11.glColor4f(0.0F + distance / 16.0F, 0.0F + distance / 16.0F, 0.6F, 0.6F);
					}
					Aeon.getInstance().getUtils().getRender();
					RenderUtils.beginSmoothLine();
					GL11.glPushMatrix();
					GL11.glTranslatef(renderX, renderY, renderZ);
					GL11.glRotatef(-player.rotationYaw, 0.0F, 1.0F, 0.0F);
					final AxisAlignedBB boundingBox = AxisAlignedBB.getBoundingBox(-renderWidth, 0.0D, -renderWidth, renderWidth, renderHeight, renderWidth);
					RenderUtils.drawOutlinedBox(boundingBox);
					if (isFriend)
					{
						GL11.glColor4f(0.0F + distance / 16.0F, 0.5F + distance / 16.0F, 0.8F, 0.15F);
					} else
					{
						GL11.glColor4f(0.0F + distance / 16.0F, 0.0F + distance / 16.0F, 0.8F, 0.15F);
					}
					RenderUtils.drawBox(boundingBox);
					GL11.glPopMatrix();
					Aeon.getInstance().getUtils().getRender();
					RenderUtils.endSmoothLine();
				}
			}
		}
		if (mode.contains("mob"))
		{
			for (final EntityLivingBase entity : Aeon.getInstance().getUtils().getEntity().getLivingEntities())
			{
				if (entity != mc.thePlayer && !entity.isDead && !(entity instanceof EntityOtherPlayerMP))
				{
					final float renderX = (float) (this.interpolate(entity.prevPosX, entity.posX) - RenderManager.renderPosX);
					final float renderY = (float) (this.interpolate(entity.prevPosY, entity.posY) - RenderManager.renderPosY);
					final float renderZ = (float) (this.interpolate(entity.prevPosZ, entity.posZ) - RenderManager.renderPosZ);
					final float renderWidth = entity.width / 1.5F;
					final float renderHeight = entity.height;
					final float distance = mc.thePlayer.getDistanceToEntity(entity);
					final boolean isFriend = Aeon.getInstance().getUtils().getFriend().containsFriend(entity.getCommandSenderName());
					if (isFriend)
					{
						GL11.glColor3f(0.0F + distance / 16.0F, 1.0F + distance / 16.0F, 0.8F);
					} else
					{
						GL11.glColor3f(0.0F + distance / 16.0F, 0.0F + distance / 16.0F, 0.6F);
					}
					Aeon.getInstance().getUtils().getRender();
					RenderUtils.beginSmoothLine();
					GL11.glPushMatrix();
					GL11.glTranslatef(renderX, renderY, renderZ);
					GL11.glRotatef(-entity.rotationYaw, 0.0F, 1.0F, 0.0F);
					final AxisAlignedBB boundingBox = AxisAlignedBB.getBoundingBox(-renderWidth, 0.0D, -renderWidth, renderWidth, renderHeight, renderWidth);
					RenderUtils.drawOutlinedBox(boundingBox);
					if (isFriend)
					{
						GL11.glColor4f(0.0F + distance / 16.0F, 0.5F + distance / 16.0F, 0.8F, 0.3F);
					} else
					{
						GL11.glColor4f(0.0F + distance / 16.0F, 0.0F + distance / 16.0F, 0.8F, 0.3F);
					}
					RenderUtils.drawBox(boundingBox);
					GL11.glPopMatrix();
					Aeon.getInstance().getUtils().getRender();
					RenderUtils.endSmoothLine();
				}
			}
		}
	}

	private void moveLines(int type, float distance)
	{
		switch (type)
		{
		case 1:
			x -= distance * (float) Math.sin(Math.toRadians(mc.thePlayer.rotationYaw));
			z += distance * (float) Math.cos(Math.toRadians(mc.thePlayer.rotationYaw));
			break;
		case 2:
			x += distance * (float) Math.sin(Math.toRadians(mc.thePlayer.rotationYaw));
			z -= distance * (float) Math.cos(Math.toRadians(mc.thePlayer.rotationYaw));
			break;
		case 3:
			x += distance * (float) Math.sin(Math.toRadians(mc.thePlayer.rotationYaw - 90.0F));
			z -= distance * (float) Math.cos(Math.toRadians(mc.thePlayer.rotationYaw - 90.0F));
			break;
		case 4:
			x += distance * (float) Math.sin(Math.toRadians(mc.thePlayer.rotationYaw + 90.0F));
			z -= distance * (float) Math.cos(Math.toRadians(mc.thePlayer.rotationYaw + 90.0F));
		}
	}

	public double interpolate(double prev, double cur)
	{
		return prev + (cur - prev) * partialTicks;
	}
}
