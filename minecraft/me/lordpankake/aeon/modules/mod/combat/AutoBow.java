package me.lordpankake.aeon.modules.mod.combat;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventMouseClick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemBow;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;

public class AutoBow extends Module
{
	public float speed = 8F;
	@AIgnore
	private boolean canFire = true;
	@AIgnore
	private boolean isBursting;
	public int burstCount = 0;
	public String mode = "auto";
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();

	public AutoBow(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	public boolean holdingBow()
	{
		final ItemStack itemstack = mc.thePlayer.getCurrentEquippedItem();
		return itemstack != null && itemstack.getItem() instanceof ItemBow;
	}

	@Override
	@EventTarget
	public void onMouseClick(EventMouseClick event)
	{
		if (event.isRightClick())
		{
			canFire = true;
			if (mode.contains("burst"))
			{
				if (this.burstCount >= 5)
				{
					this.burstCount = 0;
				}
			} else
			{
				this.burstCount = 0;
			}
		}
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (mode.contains("auto"))
		{
			FireMode_Auto();
		}
		if (mode.contains("click"))
		{
			FireMode_Click();
		}
		if (mode.contains("burst"))
		{
			FireMode_Burst();
		}
	}

	// Fix burst mode!
	private void FireMode_Burst()
	{
		final C08PacketPlayerBlockPlacement packet = new C08PacketPlayerBlockPlacement(-1, -1, -1, 255, mc.thePlayer.inventory.getCurrentItem(), 0, 0, 0);
		if (packet instanceof C08PacketPlayerBlockPlacement && holdingBow())
		{
			if (this.burstCount >= 5 || !canFire || packet.func_149576_c() != -1 || packet.func_149571_d() != -1 || packet.func_149570_e() != -1 || packet.func_149568_f() != 255)
			{
				return;
			}
			mc.thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(Minecraft.getMinecraft().thePlayer.inventory.currentItem));
			for (int i = 0; i < 35; i++)
			{
				mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(false));
			}
			mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(5, 0, 0, 0, 255));
			this.burstCount += 1;
			if (this.burstCount >= 5)
			{
				canFire = false;
				mc.thePlayer.stopUsingItem();
			}
		}
	}

	private void FireMode_Click()
	{
		final C08PacketPlayerBlockPlacement packet = new C08PacketPlayerBlockPlacement(-1, -1, -1, 255, mc.thePlayer.inventory.getCurrentItem(), 0, 0, 0);
		if (packet instanceof C08PacketPlayerBlockPlacement && holdingBow() && mc.thePlayer.isUsingItem() && canFire)
		{
			if (packet.func_149576_c() != -1 || packet.func_149571_d() != -1 || packet.func_149570_e() != -1 || packet.func_149568_f() != 255)
			{
				return;
			}
			mc.thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(Minecraft.getMinecraft().thePlayer.inventory.currentItem));
			for (int i = 0; i < 35; i++)
			{
				mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(false));
			}
			mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(5, 0, 0, 0, 255));
			canFire = false;
		}
	}

	private void FireMode_Auto()
	{
		final C08PacketPlayerBlockPlacement packet = new C08PacketPlayerBlockPlacement(-1, -1, -1, 255, mc.thePlayer.inventory.getCurrentItem(), 0, 0, 0);
		if (holdingBow() && mc.thePlayer.isUsingItem())
		{
			if (packet.func_149576_c() != -1 || packet.func_149571_d() != -1 || packet.func_149570_e() != -1 || packet.func_149568_f() != 255)
			{
				return;
			}
			//mc.thePlayer.sendQueue.addToSendQueue(new C09PacketHeldItemChange(Minecraft.getMinecraft().thePlayer.inventory.currentItem));
			
			if (this.timer.check(1000.0F / this.speed))
			{
				for (int i = 0; i < 2; i++)
				{
					mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(false));
				}
				mc.thePlayer.sendQueue.addToSendQueue(new C07PacketPlayerDigging(5, 0, 0, 0, 255));
				mc.thePlayer.stopUsingItem();
				this.timer.reset();
			}
		}
	}

	@Override
	public void onEnable()
	{
		loadSettings();
	}
}