package me.lordpankake.aeon.modules.mod.combat;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;

public class Respawn extends Module
{
	public Respawn(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (mc.thePlayer.isDead)
		{
			mc.thePlayer.respawnPlayer();
			mc.displayGuiScreen(null);
		}
	}
}
