package me.lordpankake.aeon.modules.mod.combat;

import java.util.ArrayList;
import java.util.List;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.InventoryUtils;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class AutoArmor extends Module
{
	@AIgnore
	private int clickSlot = -1;
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();

	public AutoArmor(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (!Soup.souping)
		{
			final InventoryUtils inventory = Aeon.getInstance().getUtils().getInventory();
			if (mc.currentScreen != null || !timer.check(150.0F))
			{
				return;
			}
			if (clickSlot != -1)
			{
				inventory.clickSlot(clickSlot, 0, false);
				clickSlot = -1;
				timer.reset();
				return;
			}
			final Integer[] armors = getArmor();
			Integer[] arrayOfInteger1;
			final int j = (arrayOfInteger1 = armors).length;
			for (int i = 0; i < j; i++)
			{
				final int x = arrayOfInteger1[i].intValue();
				final Slot slot = mc.thePlayer.inventoryContainer.getSlot(x);
				final int replacement = getReplacementSlot(slot);
				if (replacement != -1)
				{
					inventory.clickSlot(slot.slotNumber, 0, false);
					clickSlot = replacement;
					timer.reset();
					break;
				}
			}
		}
	}

	private int getReplacementSlot(Slot slot)
	{
		final InventoryUtils inventory = Aeon.getInstance().getUtils().getInventory();
		final ItemArmor armor = (ItemArmor) slot.getStack().getItem();
		final Slot wearingSlot = inventory.getWearingArmor(armor.armorType);
		if (wearingSlot.getHasStack())
		{
			final ItemArmor wearingArmor = (ItemArmor) wearingSlot.getStack().getItem();
			if (armor.getArmorMaterial().compareTo(wearingArmor.getArmorMaterial()) > 0)
			{
				return wearingSlot.slotNumber;
			}
		} else
		{
			return wearingSlot.slotNumber;
		}
		return -1;
	}

	private Integer[] getArmor()
	{
		final List tempList = new ArrayList();
		for (int o = 9; o < 44; o++)
		{
			if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack())
			{
				final ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
				if (item != null && item.getItem() instanceof ItemArmor)
				{
					tempList.add(Integer.valueOf(o));
				}
			}
		}
		return (Integer[]) tempList.toArray(new Integer[tempList.size()]);
	}
}
