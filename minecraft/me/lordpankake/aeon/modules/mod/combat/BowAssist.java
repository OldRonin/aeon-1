package me.lordpankake.aeon.modules.mod.combat;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.EntityUtils;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;

public class BowAssist extends Module
{
	@AIgnore
	private EntityLivingBase target;
	private final float range = 10F;

	public BowAssist(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		final EntityUtils util = Aeon.getInstance().getUtils().getEntity();
		final ItemStack is = mc.thePlayer.inventory.getCurrentItem();
		if (is != null)
		{
			final String id = is.getItem().getUnlocalizedName().toLowerCase();
			if (id.contains("bow") && !id.contains("bowl"))
			{
				this.target = util.getClosestEntity(true, range, 2.5F);
				if (this.target != null && mc.thePlayer.canEntityBeSeen(this.target))
				{
					if (Aeon.getInstance().getUtils().getFriend().containsFriend(this.target.getCommandSenderName()))
					{
						return;
					}
					final int diffInItemUse = is.getMaxItemUseDuration() - is.getItemDamage();
					float compensatedVelocity = diffInItemUse / 20.0F;
					compensatedVelocity = (compensatedVelocity * compensatedVelocity + compensatedVelocity * 2.0F) / 3.0F;
					if (compensatedVelocity >= 1.0F)
					{
						compensatedVelocity = 1.0F;
					}
					final double v = compensatedVelocity * 3.0F;
					final double g = 0.0501F;
					setAngles(this.target, v, g);
				}
			}
		}
	}

	private double theta(double v, double g, double x, double y)
	{
		final double yv = 2 * y * (v * v);
		final double gx = g * (x * x);
		final double g2 = g * (gx + yv);
		final double insqrt = v * v * v * v - g2;
		final double sqrt = Math.sqrt(insqrt);
		final double numerator = v * v + sqrt;
		final double numerator2 = v * v - sqrt;
		final double atan1 = Math.atan2(numerator, g * x);
		final double atan2 = Math.atan2(numerator2, g * x);
		return Math.min(atan1, atan2);
	}

	private double time(double v, double g, float launchAngle)
	{
		double v1 = v * Math.sin(launchAngle);
		v1 += v1;
		return v1 / g;
	}

	public float getRange()
	{
		return this.range;
	}

	private float getLaunchAngle(EntityLivingBase targetEntity, double v, double g)
	{
		final double yDif = targetEntity.posY + targetEntity.getEyeHeight() / 2 - (mc.thePlayer.posY + mc.thePlayer.getEyeHeight());
		final double xDif = targetEntity.posX - mc.thePlayer.posX;
		final double zDif = targetEntity.posZ - mc.thePlayer.posZ;
		final double xCoord = Math.sqrt(xDif * xDif + zDif * zDif);
		final float angle = (float) theta(v, g, xCoord, yDif);
		return angle;
	}

	private void setAngles(EntityLivingBase entityLiving, double v, double g)
	{
		final double deg = -Math.toDegrees(getLaunchAngle(entityLiving, v, g));
		if (Double.isNaN(deg))
		{
			return;
		}
		mc.thePlayer.rotationPitch = (float) deg;
		final double difX = entityLiving.posX - mc.thePlayer.posX, difY = entityLiving.posY - mc.thePlayer.posY + entityLiving.getEyeHeight() / 1.2F, difZ = entityLiving.posZ - mc.thePlayer.posZ;
		final float yaw = (float) (Math.atan2(difZ, difX) * 180 / Math.PI) - 90;
		mc.thePlayer.rotationYaw = yaw;
		mc.thePlayer.rotationYawHead = yaw;
	}
}
