package me.lordpankake.aeon.modules.mod.combat;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreTick;
import net.minecraft.network.play.client.C03PacketPlayer;

public class Regeneration extends Module
{
	public Regeneration(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onPreTick(EventPreTick event)
	{
		final boolean canHeal = mc.thePlayer.onGround || mc.thePlayer.isInWater() || mc.thePlayer.isOnLadder();
		final boolean shouldHeal = mc.thePlayer.getHealth() <= 19.0F && mc.thePlayer.getFoodStats().getFoodLevel() > 17;
		if (canHeal && shouldHeal)
		{
			for (int s = 0; s <= 10; s++)
			{
				mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
			}
		}
	}
}
