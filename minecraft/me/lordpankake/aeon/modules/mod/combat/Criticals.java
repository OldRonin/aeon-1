package me.lordpankake.aeon.modules.mod.combat;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPostEntityHit;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreEntityHit;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPostTick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreTick;
import net.minecraft.block.material.Material;
import net.minecraft.util.MovingObjectPosition.MovingObjectType;

public class Criticals extends Module
{
	@AIgnore
	private double oldY;
	@AIgnore
	private boolean savedOnGround;

	public Criticals(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		mc.thePlayer.motionY = 0.1;
	}

	@EventTarget
	@Override
	public void onPreTick(EventPreTick event)
	{
		savedOnGround = mc.thePlayer.onGround;
		if (!mc.gameSettings.keyBindJump.pressed)
		{
			mc.thePlayer.onGround = false;
		}
		
		if (shouldCritical())
		{
			crit();
		}
	}

	@EventTarget
	@Override
	public void onPreEntityHit(EventPreEntityHit event)
	{
		// event.setCancelled();
		 mc.thePlayer.motionY = 0.01;
		crit();
	}

	@EventTarget
	@Override
	public void onPostEntityHit(EventPostEntityHit event)
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
		{
			if (!mc.thePlayer.onGround)
			{
				mc.thePlayer.posY = oldY;
			}
		}
		if (!mc.thePlayer.onGround && (Aeon.getInstance().getUtils().getHack().getLoader().aura.target == null && mc.objectMouseOver.typeOfHit != MovingObjectType.ENTITY && Aeon.getInstance().getUtils().getHack().getLoader().lockview.target == null))
		{
			//
		}
		mc.thePlayer.onGround = savedOnGround;
	}

	private void crit()
	{
	
		if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
		{
			oldY = mc.thePlayer.posY;
		//	mc.thePlayer.boundingBox.offset(0D, 0.005D, 0.0D);
			mc.thePlayer.boundingBox.offset(0D, 0.01D, 0.0D);
			mc.thePlayer.motionY = -0.003;
		}
	}

	private boolean shouldCritical()
	{
		if (mc.thePlayer.onGround && !mc.thePlayer.isInWater() && !mc.thePlayer.isInsideOfMaterial(Material.lava) && !mc.gameSettings.keyBindJump.pressed)
		{
			if (Aeon.getInstance().getUtils().getHack().getLoader().aura.target != null || mc.objectMouseOver.typeOfHit == MovingObjectType.ENTITY || Aeon.getInstance().getUtils().getHack().getLoader().lockview.target != null)
			{
				Log.write("CRITING:" + Aeon.getInstance().getUtils().getHack().getLoader().aura.target.getCommandSenderName());
				return true;
			}
		}
		return false;
	}
}
