package me.lordpankake.aeon.modules.mod.combat;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreEntityHit;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;

public class AutoWeapon extends Module
{
	public AutoWeapon(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onPreEntityHit(EventPreEntityHit event)
	{
	
		if (!Soup.souping)
		{
			switchToWeapon(mc.thePlayer.inventory.currentItem);
		}
	}

	public void switchToWeapon(int i)
	{
		final ItemStack itemstack;
		mc.thePlayer.inventory.currentItem = 0;
		for (int k = 0; k < 9; k++)
		{
			mc.thePlayer.inventory.currentItem = k;
			if (holdingSword())
			{
				return;
			}
		}
		if (!holdingSword())
		{
			for (int k = 0; k < 9; k++)
			{
				mc.thePlayer.inventory.currentItem = k;
				if (holdingAxe())
				{
					return;
				}
			}
		}
		mc.thePlayer.inventory.currentItem = i;
	}

	public boolean holdingSword()
	{
		final ItemStack itemstack = mc.thePlayer.getCurrentEquippedItem();
		return itemstack != null && itemstack.getItem() instanceof ItemSword;
	}

	public boolean holdingAxe()
	{
		final ItemStack itemstack = mc.thePlayer.getCurrentEquippedItem();
		return itemstack != null && itemstack.getItem() instanceof ItemAxe;
	}
}
