package me.lordpankake.aeon.modules.mod.combat;

import net.minecraft.network.play.server.S12PacketEntityVelocity;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPacketReceive;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventVelocity;

public class AntiVelocity extends Module
{
	public AntiVelocity(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}
	
	@Override
	@EventTarget
	public void onPacketReceive(EventPacketReceive event)
	{
		if (event.storedPacket instanceof S12PacketEntityVelocity)
		{
			event.nullifyPacket();
		}
	}

	@Override
	@EventTarget
	public void onVelocitySet(EventVelocity event)
	{
		event.velocityX = 0;
		event.velocityY = 0;
		event.velocityZ = 0;
	}
}
