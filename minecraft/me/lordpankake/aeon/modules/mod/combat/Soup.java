package me.lordpankake.aeon.modules.mod.combat;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.annotations.IncludeGUI;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventHealthChange;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPostTick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreTick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.InventoryUtils;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Soup extends Module
{
	public static float healthLevel = 14f;
	public static boolean drop = false;
	@AIgnore
	public static boolean souping;
	public boolean safeSoup = false;
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();
	@IncludeGUI(min = 0.1F, max = 10F)
	public float speed = 5.5F;

	public Soup(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onHealthChange(EventHealthChange event)
	{
		if (!mc.thePlayer.capabilities.isCreativeMode && event.health <= healthLevel)
		{
			souping = true;
		} else
		{
			souping = false;
		}
		Log.write(event.health + " Fhealth");
	}

	@Override
	@EventTarget
	public void onPostTick(EventPostTick event)
	{
		if (!souping)
		{
			return;
		}
		if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
		{
			if (this.timer.check(this.speed * 50))
			{
				soup();
				if (!safeSoup)
				{
					restockHotbar();
				}
				this.timer.reset();
			}
		} else
		{
			if (this.timer.check(this.speed * 40))
			{
				soup();
				if (!safeSoup)
				{
					restockHotbar();
				}
				this.timer.reset();
			}
		}
	}

	private void soup()
	{
		final InventoryUtils inventory = Aeon.getInstance().getUtils().getInventory();
		final int soupIndex = inventory.findHotbarItem(282);
		if (soupIndex != -1)
		{
			inventory.swap(soupIndex);
			inventory.useItem(mc.thePlayer.inventory.getStackInSlot(soupIndex));
			return;
		}
	}

	private void restockHotbar()
	{
		final InventoryUtils inventory = Aeon.getInstance().getUtils().getInventory();
		final int avSlot = inventory.findAvailableSlotInventory(new int[]
		{ 281 });
		final int replacementSoup = inventory.findInventoryItem(282);
		if (replacementSoup != -1 && avSlot != -1)
		{
			final ItemStack badItem = mc.thePlayer.inventoryContainer.getSlot(avSlot).getStack();
			if (isShiftable(badItem))
			{
				inventory.clickSlot(avSlot, true);
				inventory.clickSlot(replacementSoup, true);
			} else
			{
				inventory.clickSlot(replacementSoup, false);
				inventory.clickSlot(avSlot, false);
			}
		}
	}

	@Override
	public String getDisplayName()
	{
		if (getState())
		{
			final InventoryUtils inventory = Aeon.getInstance().getUtils().getInventory();
			final int soups = inventory.getItemCountInventory(282) + inventory.getItemCountHotbar(282);
			return "Soup" + " [" + soups + "]";
		}
		return dispName;
	}

	@Override
	public String getName()
	{
		if (getState())
		{
			final InventoryUtils inventory = Aeon.getInstance().getUtils().getInventory();
			final int soups = inventory.getItemCountInventory(282) + inventory.getItemCountHotbar(282);
			return "Soup" + " [" + soups + "]";
		}
		return name;
	}

	private boolean isShiftable(ItemStack preferedItem)
	{
		if (preferedItem == null)
		{
			return true;
		}
		for (int o = 9; o < 36; o++)
		{
			if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack())
			{
				final ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
				if (item == null)
				{
					return true;
				}
				if (Item.getIdFromItem(item.getItem()) == Item.getIdFromItem(preferedItem.getItem()) && item.stackSize + preferedItem.stackSize <= preferedItem.getMaxStackSize())
				{
					return true;
				}
			} else
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public void onEnable()
	{
		loadSettings();
	}
}
