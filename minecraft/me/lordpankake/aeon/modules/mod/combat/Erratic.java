package me.lordpankake.aeon.modules.mod.combat;

import java.util.Random;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.EntityUtils;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;
import net.minecraft.entity.EntityLivingBase;

public class Erratic extends Module
{
	public int radius = 4;
	public float speed = 7f;
	@AIgnore
	private EntityLivingBase target;
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();

	public Erratic(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		final EntityUtils util = Aeon.getInstance().getUtils().getEntity();
		this.target = util.getClosestEntity(true, 3F, 1F);
		if (this.target != null && mc.thePlayer.getDistanceToEntity(this.target) <= 4.3F)
		{
			final double tx = this.target.posX;
			final double tz = this.target.posZ;
			final double x = generatRandomPositiveNegitiveValue(radius);
			final double z = generatRandomPositiveNegitiveValue(radius);
			if (this.timer.check(1000.0F / speed))
			{
				mc.thePlayer.setPosition(tx + x, mc.thePlayer.posY, tz + z);
				this.timer.reset();
			}
		}
	}

	public static double generatRandomPositiveNegitiveValue(int range)
	{
		final Random r = new Random();
		final double ii = -r.nextDouble() * range + r.nextDouble() * range;
		return ii;
	}
}
