package me.lordpankake.aeon.modules;
public enum ModuleType
{
	Player, Combat, Render, World, Auto, Misc
}
