package me.lordpankake.aeon.modules.eventapi.events.aeon;

import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.network.Packet;

public class EventDisplayScreen extends NeuEvent
{
	public GuiScreen screen;

	public void call(GuiScreen scr)
	{
		this.screen = scr;
		super.call();
	}

}