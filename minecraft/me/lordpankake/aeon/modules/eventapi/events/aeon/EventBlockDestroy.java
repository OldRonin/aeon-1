package me.lordpankake.aeon.modules.eventapi.events.aeon;

import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;

public class EventBlockDestroy extends NeuEvent
{
	public int x, y, z;

	public void call(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		super.call();
	}
}