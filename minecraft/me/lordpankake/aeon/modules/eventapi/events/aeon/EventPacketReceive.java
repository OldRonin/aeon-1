package me.lordpankake.aeon.modules.eventapi.events.aeon;

import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;
import net.minecraft.network.Packet;

public class EventPacketReceive extends NeuEvent
{
	public Packet storedPacket;

	public void call(Packet pack)
	{
		this.storedPacket = pack;
		super.call();
	}

	public void nullifyPacket()
	{
		this.storedPacket = null;
	}
}
