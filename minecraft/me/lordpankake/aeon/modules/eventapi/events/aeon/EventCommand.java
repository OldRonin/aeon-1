package me.lordpankake.aeon.modules.eventapi.events.aeon;

import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;
import me.lordpankake.aeon.modules.mod.chat.CommandListener;

public class EventCommand extends NeuEvent
{
	private String message;
	private String sender;

	public String getMessage()
	{
		return this.message;
	}

	public String getSender()
	{
		return this.sender;
	}

	public boolean isAeonCommand()
	{
		if (this.message.startsWith(CommandListener.commandKey) && this.sender.toLowerCase().contains("aeon"))
		{
			return true;
		}
		return false;
	}

	public void call(String text, String user)
	{
		this.message = text;
		this.sender = user;
		super.call();
	}
}
