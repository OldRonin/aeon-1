package me.lordpankake.aeon.modules.eventapi.events.aeon;

import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;

public class EventSendMessage extends NeuEvent
{
	public String message;

	public void call(String message)
	{
		this.message = message;
		super.call();
	}
}
