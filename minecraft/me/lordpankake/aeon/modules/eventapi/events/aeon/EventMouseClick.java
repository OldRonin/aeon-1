package me.lordpankake.aeon.modules.eventapi.events.aeon;

import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;

public class EventMouseClick extends NeuEvent
{
	private int mouseButtonClicked;

	public int getMouseClickID()
	{
		return this.mouseButtonClicked;
	}

	public boolean isLeftClick()
	{
		if (this.mouseButtonClicked == 0)
		{
			return true;
		}
		return false;
	}

	public boolean isRightClick()
	{
		if (this.mouseButtonClicked == 1)
		{
			return true;
		}
		return false;
	}

	public boolean isMiddleClick()
	{
		if (this.mouseButtonClicked == 2)
		{
			return true;
		}
		return false;
	}

	public void call(int mouseButtonID)
	{
		this.mouseButtonClicked = mouseButtonID;
		super.call();
	}
}
