package me.lordpankake.aeon.modules.eventapi.events.aeon;

import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;

public class EventVelocity extends NeuEvent
{
	public double velocityX, velocityY, velocityZ;

	public void call(double velocityX, double velocityY, double velocityZ)
	{
		this.velocityX = velocityX;
		this.velocityY = velocityY;
		this.velocityZ = velocityZ;
		super.call();
	}
}
