package me.lordpankake.aeon.modules.eventapi.events.aeon;

import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;

public class EventHealthChange extends NeuEvent
{
	public float health;

	public void call(float health)
	{
		this.health = health;
		super.call();
	}
}
