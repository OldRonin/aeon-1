package me.lordpankake.aeon.modules.eventapi.events.aeon;

import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;

public class EventBlockDamage extends NeuEvent
{
	public int x, y, z, side;
	public float damage;

	public void call(int x, int y, int z, int side, float curBlockDamageMP)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.side = side;
		this.damage = curBlockDamageMP;
		super.call();
	}
}
