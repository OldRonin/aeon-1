package me.lordpankake.aeon.modules.eventapi.events.aeon;

import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;
import me.lordpankake.aeon.util.FriendUtils;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;

public class EventPostEntityHit extends NeuEvent
{
	public Entity entity;

	public boolean isMob()
	{
		if (this.entity instanceof EntityMob)
		{
			return true;
		}
		return false;
	}

	public boolean isAnimal()
	{
		if (this.entity instanceof EntityAnimal)
		{
			return true;
		}
		return false;
	}

	public boolean isPlayer()
	{
		if (this.entity instanceof EntityPlayer)
		{
			return true;
		}
		return false;
	}

	public boolean isFriend()
	{
		if (isPlayer())
		{
			EntityPlayer ep = null;
			ep = (EntityPlayer) this.entity;
			if (FriendUtils.friends.contains(ep.getCommandSenderName()))
			{
				return true;
			}
		}
		return false;
	}

	public void call(Entity entity)
	{
		this.entity = entity;
		super.call();
	}
}
