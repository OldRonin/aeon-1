package me.lordpankake.aeon.modules.eventapi.annotations;
public @interface ACommand
{
	String commandName() default "Command";

	String description() default "Command description.";

	int parameters() default 0;
}
