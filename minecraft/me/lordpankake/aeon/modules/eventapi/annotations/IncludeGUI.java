package me.lordpankake.aeon.modules.eventapi.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

;
@Retention(RetentionPolicy.RUNTIME)
public @interface IncludeGUI
{
    float min();// default 5F; 
	float max();// default 5F; 
}