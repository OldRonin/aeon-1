package me.lordpankake.aeon.modules;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.eventapi.EventManager;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.util.FileUtils;
import me.lordpankake.aeon.util.ScreenUtils;
import me.lordpankake.aeon.util.StringUtils;
import net.minecraft.client.Minecraft;

public class Module extends ModuleMethods
{
	public Minecraft mc = Minecraft.getMinecraft();
	protected String name, dispName, description;
	protected int key;
	protected ModuleType type;
	protected boolean state;
	protected boolean inUI;

	public Module(String name, int keybind, ModuleType type, String description)
	{
		this.dispName = name;
		this.name = name;
		this.key = keybind;
		this.type = type;
		this.inUI = true;
		this.description = description;
	}

	public void toggle()
	{
		state = !state;
		if (state == true)
		{
			baseEnable();
			onEnable();
		} else
		{
			baseDisable();
			onDisable();
		}
	}

	public String getName()
	{
		return name;
	}

	public String getDisplayName()
	{
		return dispName;
	}

	public int getKey()
	{
		return key;
	}

	public void setKey(int key)
	{
		this.key = key;
	}

	public ModuleType getType()
	{
		return type;
	}

	public int getColor()
	{
		if (getType() == ModuleType.Combat)
		{
			return 0xFF1919;
		}
		if (getType() == ModuleType.Player)
		{
			return 0xB8E6B8;
		}
		if (getType() == ModuleType.World)
		{
			return 0xFFFF33;
		}
		if (getType() == ModuleType.Render)
		{
			return 0x00B8E6;
		}
		return 0xFFFFFF;
	}

	public boolean getState()
	{
		return state;
	}

	public void setState(boolean state)
	{
		this.state = state;
	}

	public boolean shouldDrawToUI()
	{
		return this.inUI;
	}

	public void setDrawToUI(boolean should)
	{
		this.inUI = should;
	}

	protected void baseEnable()
	{
		EventManager.register(this);
		Aeon.getInstance().getUtils().getHack().addToRegistered(this);
	}

	protected void baseDisable()
	{
		EventManager.unregister(this);
		Aeon.getInstance().getUtils().getHack().removeFromRegistered(this);
	}

	public void buttons(boolean state)
	{
		/*Aeon.getInstance().getUtils().getuScreen();
		for (final DSFrame f : ScreenUtils.getGuiManager(0).getFrames())
		{
			if (f.getTitle().toLowerCase().contains(getType().toString().toLowerCase()))
			{
				for (DSComponent comp : f.getChildren())
				{
					if (comp instanceof BasicButton)
					{
						final BasicButton b = (BasicButton) comp;
						if (getName().contains(b.getText()))
						{
							b.setPressedState(state);
							comp = b;
						}
					}
				}
			}
		}*/
	}

	public void writeSettings()
	{
		if (getName().contains("ChatCom") || getName().contains("Wayp"))// Lrn2SaveWaypoints
		{
			return;
		}
		final Module m = this;
		final File saveFile = new File("Aeon" + File.separator + "Hacks" + File.separator + m.getClass().getSimpleName() + ".txt");
		final ArrayList<String> content = new ArrayList<String>();
		boolean doWrite = false;
		if (m.getClass().getDeclaredFields().length > 0)
		{
			for (final Field field : m.getClass().getDeclaredFields())
			{
				try
				{
					if (!field.isAnnotationPresent(AIgnore.class))
					{
						doWrite = true;
						// field.get(objectInstance)
						// If you get() on an instance of the object, it'll work
						// for nonstatic things....
						field.setAccessible(true);
						final Object value = field.get(m);
						content.add(field.getName() + ":" + value);
						// Log.write(m.getClass().getSimpleName() + ":" +
						// field.getName() + ":" + value);
					}
				} catch (final IllegalArgumentException e)
				{
					 e.printStackTrace();
				} catch (final IllegalAccessException e)
				{
					 e.printStackTrace();
				}
			}
			if (doWrite)
			{
				FileUtils.write(saveFile, content, true);
			}
		}
	}

	public void loadSettings()
	{
		if (!(this.getClass().getDeclaredFields().length > 0) || getName().contains("ChatCom"))
		{
			return;
		}
		final ArrayList<String> content = (ArrayList<String>) FileUtils.read(new File("Aeon" + File.separator + "Hacks" + File.separator + this.getClass().getSimpleName() + ".txt"));
		for (final String field : content)
		{
			try
			{
				final Field f = this.getClass().getDeclaredField(field.substring(0, field.indexOf(":")));
				f.setAccessible(true);
				if (!f.isAnnotationPresent(AIgnore.class))
				{
					// Log.write(this.getName() + " " +
					// this.getClass().getDeclaredField(field.substring(0,
					// field.indexOf(":"))));
					final String text = field.substring(field.indexOf(":") + 1);
					Object value = text;
					if (StringUtils.isBoolean(text))
					{
						value = Boolean.parseBoolean(text);
					}
					if (StringUtils.isDouble(text))
					{
						value = Double.parseDouble(text);
					}
					if (StringUtils.isFloat(text))
					{
						value = Float.parseFloat(text);
					}
					if (StringUtils.isInteger(text))
					{
						value = Integer.parseInt(text);
					}
					if (StringUtils.isArraylist(text))
					{
						value = StringUtils.getArraylist(text);
					}
					f.set(this, value);
					Log.write("~~Set " + f.getName() + ":" + value);
				}
			} catch (final Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public String getDescription()
	{
		return this.description;
	}
}
