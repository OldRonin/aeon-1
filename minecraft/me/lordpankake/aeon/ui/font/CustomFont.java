/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele **Totally didn't steal
 * this from godshawk**
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package me.lordpankake.aeon.ui.font;

import net.minecraft.client.renderer.texture.TextureUtil;
import org.lwjgl.opengl.GL11;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;

/**
 * Modified CustomFont API to have more functionality/features
 *
 * @author Richmond Steele
 * @since 9/29/13 All rights Reserved Please read included LICENSE file
 */
public class CustomFont
{
	protected final int IMAGE_WIDTH = 512;
	protected final int IMAGE_HEIGHT = 512;
	protected CharData[] charDatas = new CharData[256];
	protected int fontHeight = -1;
	protected float charOffset = 0;
	protected boolean antiAlias;
	protected boolean fractionalMetrics;
	protected int textureID;
	protected Font font;

	public CustomFont(Font font, boolean antiAlias, boolean fractionalMetrics, float charOffset)
	{
		this(font, antiAlias, fractionalMetrics);
		this.charOffset = charOffset;
	}

	public CustomFont(Font font, boolean antiAlias, boolean fractionalMetrics)
	{
		this.font = font;
		this.antiAlias = antiAlias;
		this.fractionalMetrics = fractionalMetrics;
		setupGraphics();
	}

	public void setupGraphics()
	{
		final BufferedImage bufferedImage = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, 2);
		final Graphics2D g = (Graphics2D) bufferedImage.getGraphics();
		g.setFont(font);
		g.setColor(new Color(255, 255, 255, 0));
		g.fillRect(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
		g.setColor(Color.WHITE);
		g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, fractionalMetrics ? RenderingHints.VALUE_FRACTIONALMETRICS_ON : RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, antiAlias ? RenderingHints.VALUE_TEXT_ANTIALIAS_ON : RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, antiAlias ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF);
		final FontMetrics fontMetrics = g.getFontMetrics();
		int charHeight = 0;
		int positionX = 0;
		int positionY = 0;
		for (int i = 0; i < charDatas.length; i++)
		{
			final char ch = (char) i;
			final CharData charData = new CharData();
			final Rectangle2D dimensions = fontMetrics.getStringBounds(String.valueOf(ch), g);
			charData.width = dimensions.getBounds().width + 8;
			charData.height = dimensions.getBounds().height;
			if (positionX + charData.width >= 512)
			{
				positionX = 0;
				positionY += charHeight;
				charHeight = 0;
			}
			if (charData.height > charHeight)
			{
				charHeight = charData.height;
			}
			charData.storedX = positionX;
			charData.storedY = positionY;
			if (charData.height > this.fontHeight)
			{
				this.fontHeight = charData.height;
			}
			charDatas[i] = charData;
			g.drawString(String.valueOf(ch), positionX + 2, positionY + fontMetrics.getAscent());
			positionX += charData.width;
		}
		try
		{
			this.textureID = TextureUtil.uploadTextureImageAllocate(GL11.glGenTextures(), bufferedImage, false, false);
		} catch (final NullPointerException e)
		{
			e.printStackTrace();
		}
	}

	protected void drawString(String text, float x, float y, Color color)
	{
		x *= 2;
		y = (y - 3) * 2;
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.textureID);
		final float alpha = color.getAlpha() / 255.0F;
		final float red = color.getRed() / 255.0F;
		final float green = color.getGreen() / 255.0F;
		final float blue = color.getBlue() / 255.0F;
		GL11.glColor4f(red, green, blue, alpha);
		GL11.glBegin(GL11.GL_TRIANGLES);
		for (int i = 0; i < text.length(); i++)
		{
			try
			{
				final char c = text.charAt(i);
				if (c < this.charDatas.length && c >= 0)
				{
					drawChar(c, x, y);
					x += this.charDatas[c].width - 8 + this.charOffset;
				}
			} catch (final ArrayIndexOutOfBoundsException indexException)
			{
				indexException.printStackTrace();
			}
		}
		GL11.glEnd();
		GL11.glScalef(2.0F, 2.0F, 2.0F);
	}

	protected void drawChar(char c, float x, float y) throws ArrayIndexOutOfBoundsException
	{
		try
		{
			drawQuad(x, y, charDatas[c].width, charDatas[c].height, charDatas[c].storedX, charDatas[c].storedY, charDatas[c].width, charDatas[c].height);
		} catch (final Exception e)
		{
			e.printStackTrace();
		}
	}

	protected void drawQuad(float x, float y, float width, float height, float srcX, float srcY, float srcWidth, float srcHeight)
	{
		final float renderSRCX = srcX / 512.0F;
		final float renderSRCY = srcY / 512.0F;
		final float renderSRCWidth = srcWidth / 512.0F;
		final float renderSRCHeight = srcHeight / 512.0F;
		GL11.glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
		GL11.glVertex2d(x + width, y);
		GL11.glTexCoord2f(renderSRCX, renderSRCY);
		GL11.glVertex2d(x, y);
		GL11.glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
		GL11.glVertex2d(x, y + height);
		GL11.glTexCoord2f(renderSRCX, renderSRCY + renderSRCHeight);
		GL11.glVertex2d(x, y + height);
		GL11.glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY + renderSRCHeight);
		GL11.glVertex2d(x + width, y + height);
		GL11.glTexCoord2f(renderSRCX + renderSRCWidth, renderSRCY);
		GL11.glVertex2d(x + width, y);
	}

	public int getStringHeight(String text)
	{
		int height = 0;
		for (final char c : text.toCharArray())
		{
			if (c < this.charDatas.length && c >= 0 && this.charDatas[c].height - 8 > height)
			{
				height = this.charDatas[c].height - 8;
			}
		}
		return height / 2;
	}

	public int getHeight()
	{
		return (this.fontHeight - 8) / 2;
	}

	public int getStringWidth(String text)
	{
		int width = 0;
		for (final char c : text.toCharArray())
		{
			if (c < this.charDatas.length && c >= 0)
			{
				width += this.charDatas[c].width - 8 + this.charOffset;
			}
		}
		return width / 2;
	}

	public boolean isAntiAlias()
	{
		return this.antiAlias;
	}

	public void setAntiAlias(boolean antiAlias)
	{
		if (this.antiAlias != antiAlias)
		{
			this.antiAlias = antiAlias;
			setupGraphics();
		}
	}

	public boolean isFractionalMetrics()
	{
		return this.fractionalMetrics;
	}

	public void setFractionalMetrics(boolean fractionalMetrics)
	{
		if (this.fractionalMetrics != fractionalMetrics)
		{
			this.fractionalMetrics = fractionalMetrics;
			setupGraphics();
		}
	}

	public Font getFont()
	{
		return this.font;
	}

	public void setFont(Font font)
	{
		this.font = font;
		setupGraphics();
	}

	public List<String> listFormattedStringToWidth(String s, int width)
	{
		return Arrays.asList(wrapFormattedStringToWidth(s, width).split("\n"));
	}

	String wrapFormattedStringToWidth(String s, float width)
	{
		final int wrapWidth = sizeStringToWidth(s, width);
		if (s.length() <= wrapWidth)
		{
			return s;
		}
		final String split = s.substring(0, wrapWidth);
		final String split2 = getFormatFromString(split) + s.substring(wrapWidth + (s.charAt(wrapWidth) == ' ' ? 1 : 0));
		System.out.println(split + "\n" + split2);
		return split + "\n" + wrapFormattedStringToWidth(split2, width);
	}

	private int sizeStringToWidth(String par1Str, float par2)
	{
		final int var3 = par1Str.length();
		float var4 = 0.0F;
		int var5 = 0;
		int var6 = -1;
		for (boolean var7 = false; var5 < var3; var5++)
		{
			final char var8 = par1Str.charAt(var5);
			switch (var8)
			{
			case '\n':
				var5--;
				break;
			case '\247':
				if (var5 < var3 - 1)
				{
					var5++;
					final char var9 = par1Str.charAt(var5);
					if (var9 != 'l' && var9 != 'L')
					{
						if (var9 == 'r' || var9 == 'R' || isFormatColor(var9))
						{
							var7 = false;
						}
					} else
					{
						var7 = true;
					}
				}
				break;
			case ' ':
			case '-':
			case '_':
			case ':':
				var6 = var5;
			default:
				final String text = String.valueOf(var8);
				var4 += getStringWidth(text);
				if (var7)
				{
					var4 += 1.0F;
				}
				break;
			}
			if (var8 == '\n')
			{
				var5++;
				var6 = var5;
			} else
			{
				if (var4 > par2)
				{
					break;
				}
			}
		}
		return var5 != var3 && var6 != -1 && var6 < var5 ? var6 : var5;
	}

	private String getFormatFromString(String par0Str)
	{
		String var1 = "";
		int var2 = -1;
		final int var3 = par0Str.length();
		while ((var2 = par0Str.indexOf('\247', var2 + 1)) != -1)
		{
			if (var2 < var3 - 1)
			{
				final char var4 = par0Str.charAt(var2 + 1);
				if (isFormatColor(var4))
				{
					var1 = "\247" + var4;
				} else if (isFormatSpecial(var4))
				{
					var1 = var1 + "\247" + var4;
				}
			}
		}
		return var1;
	}

	private boolean isFormatColor(char par0)
	{
		return par0 >= '0' && par0 <= '9' || par0 >= 'a' && par0 <= 'f' || par0 >= 'A' && par0 <= 'F';
	}

	private boolean isFormatSpecial(char par0)
	{
		return par0 >= 'k' && par0 <= 'o' || par0 >= 'K' && par0 <= 'O' || par0 == 'r' || par0 == 'R';
	}
	protected class CharData
	{
		public int width;
		public int height;
		public int storedX;
		public int storedY;

		protected CharData()
		{
		}
	}
}
