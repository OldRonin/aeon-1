/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele **Totally didn't steal
 * this from godshawk**
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package me.lordpankake.aeon.ui.font;

import me.lordpankake.aeon.core.Log;
import org.lwjgl.opengl.GL11;
import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Allows more capability such as bold and italic fonts
 *
 * @author Richmond Steele
 * @since 9/29/13 All rights Reserved Please read included LICENSE file
 */
public class CustomFontRenderer
{
	// private static final PrefixedLogger logger = new
	// PrefixedLogger("Font Renderer");
	public static final char[] allowedCharacters = new char[]
	{ '/', '\n', '\r', '\t', '\u0000', '\f', '`', '?', '*', '\\', '<', '>', '|', '\"', ':' };
	private final Pattern patternFormattingCode = Pattern.compile("(?i)\\u00A7[0-9A-FK-OR]");
	private final int[] colorCode = new int[32];
	private final String formattingCodes = "0123456789abcdefklmnor";
	private final Random random = new Random();
	private Font baseFont;
	private CustomFont font;
	private CustomFont boldFont;
	private CustomFont italicFont;
	private CustomFont boldItalicFont;
	private final float fontSize;
	private final float spacing;
	private final boolean antiAlias;
	private final boolean fractionalMetrics;

	public CustomFontRenderer(Object font, float fontSize, float spacing, boolean antiAlias, boolean fractionalMetrics)
	{
		this.fontSize = fontSize;
		this.spacing = spacing;
		this.antiAlias = antiAlias;
		this.fractionalMetrics = fractionalMetrics;
		generateFonts(font, fontSize, spacing, antiAlias, fractionalMetrics);
		setupMinecraftColorCodes();
	}

	private boolean containsNewLineChar(final String text)
	{
		return text.contains("\n") || text.contains("\r");
	}

	private List<String> parseNewLineChars(String text)
	{
		final List<String> array = new ArrayList<String>();
		if (!containsNewLineChar(text))
		{
			array.add(text);
			return array;
		}
		String placement = "";
		for (int i = 0; i < text.length(); ++i)
		{
			final char c = text.charAt(i);
			if (c == '\n' || c == '\r')
			{
				array.add(placement);
				placement = "";
			}
			placement += c;
		}
		array.add(placement);
		return array;
	}

	private FontFormat currentFormat(FontFormat... formats)
	{
		FontFormat fontFormat = FontFormat.NONE;
		for (final FontFormat format : formats)
		{
			if (format == FontFormat.NONE)
			{
				fontFormat = FontFormat.NONE;
				break;
			}
			if (format == FontFormat.BOLD)
			{
				if (fontFormat == FontFormat.NONE)
				{
					fontFormat = FontFormat.BOLD;
				} else if (fontFormat == FontFormat.BOLD_ITALIC)
				{
					continue;
				} else
				{
					fontFormat = FontFormat.BOLD_ITALIC;
				}
			}
			if (format == FontFormat.ITALIC)
			{
				if (fontFormat == FontFormat.NONE)
				{
					fontFormat = FontFormat.ITALIC;
				} else if (fontFormat == FontFormat.BOLD_ITALIC)
				{
					continue;
				} else
				{
					fontFormat = FontFormat.BOLD_ITALIC;
				}
			}
			if (format == FontFormat.BOLD_ITALIC)
			{
				if (fontFormat != FontFormat.BOLD_ITALIC)
				{
					fontFormat = FontFormat.BOLD_ITALIC;
				}
			}
		}
		return fontFormat;
	}

	public void drawParsedString(String text, double x, double y, FontStyle style, int color1, int color2, FontFormat... formats)
	{
		boolean additive = false;
		for (final FontFormat format : formats)
		{
			if (format == FontFormat.UNDERLINE)
			{
				additive = true;
			}
		}
		int yOffset = 0;
		for (final String s : parseNewLineChars(text))
		{
			this.drawString(s, (float) x, (float) y + yOffset, style, color1, color2, formats);
			yOffset += this.stringHeight(s, FontFormat.NONE, additive ? FontFormat.UNDERLINE : FontFormat.NONE);
		}
	}

	public void drawWrappedString(int width, String text, double x, double y, FontStyle style, int color1, int color2, FontFormat... formats)
	{
		boolean additive = false;
		for (final FontFormat format : formats)
		{
			if (format == FontFormat.UNDERLINE)
			{
				additive = true;
			}
		}
		int yOffset = 0;
		for (final String s : this.listFormattedStringToWidth(text, width, this.currentFormat(formats)))
		{
			this.drawString(s, (float) x, (float) y + yOffset, style, color1, color2, formats);
			yOffset += this.stringHeight(s, FontFormat.NONE, additive ? FontFormat.UNDERLINE : FontFormat.NONE);
		}
	}

	public void drawCenteredString(String text, double x, double y, FontStyle style, int color1, int color2, FontFormat... formats)
	{
		FontFormat fontFormat = FontFormat.NONE;
		for (final FontFormat format : formats)
		{
			switch (format)
			{
			case BOLD:
				if (fontFormat == FontFormat.ITALIC)
				{
					fontFormat = FontFormat.BOLD_ITALIC;
					break;
				}
				fontFormat = FontFormat.BOLD;
				break;
			case ITALIC:
				if (fontFormat == FontFormat.BOLD)
				{
					fontFormat = FontFormat.BOLD_ITALIC;
					break;
				}
				fontFormat = FontFormat.ITALIC;
				break;
			case BOLD_ITALIC:
				fontFormat = FontFormat.BOLD_ITALIC;
				break;
			}
			if (fontFormat == FontFormat.BOLD_ITALIC)
			{
				break;
			}
		}
		final double drawX = x - stringWidth(text, fontFormat) / 2;
		drawString(text, (float) drawX, (float) y, style, color1, color2, formats);
	}

	public float drawString(String text, float x, float y, FontStyle style, int color1, int color2, FontFormat... formats)
	{
		return drawString(text, x, y, style, color1, color2, false, formats);
	}

	public float drawString(String text, float x, float y, FontStyle style, int color1, int color2, boolean mcFont, FontFormat... formats)
	{
		boolean strikethrough = false;
		boolean underline = false;
		text = text.replace("\f", "").replace("\b", "").replace("\t", "    ");
		float alpha1 = (color1 >> 24 & 0xFF) / 255.0F;
		if (mcFont && alpha1 == 0)
		{
			alpha1 = 1;
		}
		final float red1 = (color1 >> 16 & 0xFF) / 255.0F;
		final float green1 = (color1 >> 8 & 0xFF) / 255.0F;
		final float blue1 = (color1 & 0xFF) / 255.0F;
		final Color firstColor = new Color(red1, green1, blue1, alpha1);
		final float red2 = (color2 >> 16 & 0xFF) / 255.0F;
		final float green2 = (color2 >> 8 & 0xFF) / 255.0F;
		final float blue2 = (color2 & 0xFF) / 255.0F;
		final Color secondColor = new Color(red2, green2, blue2, alpha1);
		CustomFont currentFont = this.font;
		for (final FontFormat format : formats)
		{
			if (format == FontFormat.NONE)
			{
				currentFont = this.font;
				break;
			}
			if (format == FontFormat.BOLD)
			{
				if (currentFont == this.font)
				{
					currentFont = this.boldFont;
				} else if (currentFont == this.boldItalicFont)
				{
					continue;
				} else
				{
					currentFont = this.boldItalicFont;
				}
			}
			if (format == FontFormat.ITALIC)
			{
				if (currentFont == this.font)
				{
					currentFont = this.italicFont;
				} else if (currentFont == this.boldItalicFont)
				{
					continue;
				} else
				{
					currentFont = this.boldItalicFont;
				}
			}
			if (format == FontFormat.BOLD_ITALIC)
			{
				if (currentFont != this.boldItalicFont)
				{
					currentFont = this.boldItalicFont;
				}
			}
			if (format == FontFormat.STRIKETHROUGH)
			{
				strikethrough = true;
			}
			if (format == FontFormat.UNDERLINE)
			{
				underline = true;
			}
		}
		float f;
		switch (style)
		{
		case NORMAL:
			f = renderString(currentFont, text, x, y, firstColor, true);
			drawLines(strikethrough, underline, text, x, y, currentFont, color1);
			break;
		case THIN_SHADOW:
			renderString(currentFont, text, x + 0.5F, y + 0.5F, secondColor, false);
			f = renderString(currentFont, text, x, y, firstColor, true);
			drawLines(strikethrough, underline, text, x, y, currentFont, color1);
			break;
		case THICK_SHADOW:
			renderString(currentFont, text, x + 1.0F, y + 1.0F, secondColor, false);
			f = renderString(currentFont, text, x, y, firstColor, true);
			drawLines(strikethrough, underline, text, x, y, currentFont, color1);
			break;
		case THIN_OUTLINE:
			renderString(currentFont, text, x - 0.5F, y, secondColor, false);
			renderString(currentFont, text, x + 0.5F, y, secondColor, false);
			renderString(currentFont, text, x, y - 0.5F, secondColor, false);
			renderString(currentFont, text, x, y + 0.5F, secondColor, false);
			f = renderString(currentFont, text, x, y, firstColor, true);
			drawLines(strikethrough, underline, text, x, y, currentFont, color1);
			break;
		case THICK_OUTLINE:
			renderString(currentFont, text, x - 1.0F, y, secondColor, false);
			renderString(currentFont, text, x + 1.0F, y, secondColor, false);
			renderString(currentFont, text, x, y - 1.0F, secondColor, false);
			renderString(currentFont, text, x, y + 1.0F, secondColor, false);
			f = renderString(currentFont, text, x, y, firstColor, true);
			drawLines(strikethrough, underline, text, x, y, currentFont, color1);
			break;
		case BOTTOM_EMBOSS:
			renderString(currentFont, text, x, y + 0.5F, secondColor, false);
			f = renderString(currentFont, text, x, y, firstColor, true);
			drawLines(strikethrough, underline, text, x, y, currentFont, color1);
			break;
		case TOP_EMBOSS:
			renderString(currentFont, text, x, y - 0.5F, secondColor, false);
			f = renderString(currentFont, text, x, y, firstColor, true);
			drawLines(strikethrough, underline, text, x, y, currentFont, color1);
			break;
		case LEFT_EMBOSS:
			renderString(currentFont, text, x - 0.5F, y, secondColor, false);
			f = renderString(currentFont, text, x, y, firstColor, true);
			drawLines(strikethrough, underline, text, x, y, currentFont, color1);
			break;
		case RIGHT_EMBOSS:
			renderString(currentFont, text, x + 0.5F, y, secondColor, false);
			f = renderString(currentFont, text, x, y, firstColor, true);
			drawLines(strikethrough, underline, text, x, y, currentFont, color1);
			break;
		default:
			f = renderString(currentFont, text, x, y, firstColor, true);
			drawLines(strikethrough, underline, text, x, y, currentFont, color1);
			break;
		}
		return f;
	}

	private void drawLines(boolean strikethrough, boolean underline, String text, float x, float y, CustomFont currentFont, int color)
	{
		if (strikethrough)
		{
			GL11.glColor4f((color >> 16 & 0xFF) / 255.0F, (color >> 8 & 0xFF) / 255.0F, (color & 0xFF) / 255.0F, (color >> 24 & 0xFF) / 255.0F);
			drawLine(x + 1f, y + currentFont.getStringHeight(text) / 2 + 1, x + 1.5f + currentFont.getStringWidth(text), y + currentFont.getStringHeight(text) / 2 + 1, 1.5F);
		}
		if (underline)
		{
			drawLine(x + 1f, y + currentFont.getStringHeight(text) + 0.5f, x + 1.5f + currentFont.getStringWidth(text), y + currentFont.getStringHeight(text) + 0.5f, 1.5F);
		}
		GL11.glColor4f(0, 0, 0, 0);
	}

	private float renderString(CustomFont currentFont, String text, float x, float y, Color color, boolean colorCode)
	{
		if (text == null)
		{
			return 0;
		}
		GL11.glPushMatrix();
		final boolean prevBlending = GL11.glGetBoolean(GL11.GL_BLEND);
		final boolean prevAlpha = GL11.glGetBoolean(GL11.GL_ALPHA_TEST);
		GL11.glHint(GL11.GL_POLYGON_SMOOTH_HINT, GL11.GL_NICEST);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		int width = 0;
		GL11.glTranslatef(x, y, 0.0F);
		if (text.contains("\247"))
		{
			final String[] parts = text.split("\247");
			Color currentColor = color;
			boolean randomCase = false;
			boolean bold = false;
			boolean italic = false;
			boolean strikethrough = false;
			boolean underline = false;
			for (int index = 0; index < parts.length; index++)
			{
				if (parts[index].length() > 0)
				{
					if (index == 0)
					{
						currentFont.drawString(parts[index], width, 0.0F, currentColor);
						width += currentFont.getStringWidth(parts[index]);
					} else
					{
						final String input = parts[index].substring(1);
						final char type = parts[index].charAt(0);
						final int colorIndex = this.formattingCodes.indexOf(type);
						if (colorIndex != -1)
						{
							if (colorIndex < 16)
							{
								if (colorCode)
								{
									currentColor = getColor(this.colorCode[colorIndex], color.getAlpha() / 255.0F);
								}
								bold = false;
								italic = false;
								randomCase = false;
								underline = false;
								strikethrough = false;
							} else if (colorIndex == 16)
							{
								randomCase = true;
							} else if (colorIndex == 17)
							{
								bold = true;
							} else if (colorIndex == 18)
							{
								strikethrough = true;
							} else if (colorIndex == 19)
							{
								underline = true;
							} else if (colorIndex == 20)
							{
								italic = true;
							} else if (colorIndex == 21)
							{
								bold = false;
								italic = false;
								randomCase = false;
								underline = false;
								strikethrough = false;
								currentColor = color;
							}
						}
						if (bold && italic)
						{
							this.boldItalicFont.drawString(randomCase ? randomString(input) : input, width, 0.0F, currentColor);
							currentFont = this.boldItalicFont;
						} else if (bold)
						{
							this.boldFont.drawString(randomCase ? randomString(input) : input, width, 0.0F, currentColor);
							currentFont = this.boldFont;
						} else if (italic)
						{
							this.italicFont.drawString(randomCase ? randomString(input) : input, width, 0.0F, currentColor);
							currentFont = this.italicFont;
						} else
						{
							this.font.drawString(randomCase ? randomString(input) : input, width, 0.0F, currentColor);
							currentFont = this.font;
						}
						if (strikethrough)
						{
							drawLine(width, currentFont.getStringHeight(input) / 2 + 1, width + currentFont.getStringWidth(input), currentFont.getStringHeight(input) / 2 + 1, 1.5F);
						}
						if (underline)
						{
							drawLine(width, currentFont.getStringHeight(input) + 0.5f, width + currentFont.getStringWidth(input), currentFont.getStringHeight(input) + 0.5f, 1.5F);
						}
						width += currentFont.getStringWidth(input);
					}
				}
			}
		} else
		{
			currentFont.drawString(text, 0.0F, 0.0F, color);
		}
		if (!prevBlending)
		{
			GL11.glDisable(GL11.GL_BLEND);
		}
		if (!prevAlpha)
		{
			GL11.glDisable(GL11.GL_ALPHA_TEST);
		}
		GL11.glPopMatrix();
		return x + (width == 0 ? stringWidth(text, FontFormat.NONE) : width / 2);
	}

	public void generateFonts(Object font, float fontSize, float spacing, boolean antiAlias, boolean fractionalMetrics)
	{
		try
		{
			if (font instanceof Font)
			{
				this.baseFont = (Font) font;
			} else if (font instanceof File)
			{
				this.baseFont = Font.createFont(0, (File) font).deriveFont(fontSize);
			} else if (font instanceof InputStream)
			{
				this.baseFont = Font.createFont(0, (InputStream) font).deriveFont(fontSize);
			} else if (font instanceof String)
			{
				this.baseFont = new Font((String) font, 0, Math.round(fontSize));
			} else
			{
				this.baseFont = new Font("Tomaha", 0, Math.round(fontSize));
			}
		} catch (final Exception e)
		{
			e.printStackTrace();
			this.baseFont = new Font("Tomaha", 0, Math.round(fontSize));
		}
		Log.write("Font Generated: " + this.baseFont.getName());
		synchronized (this)
		{
			this.font = new CustomFont(this.baseFont, antiAlias, fractionalMetrics, spacing);
			this.boldFont = new CustomFont(this.baseFont.deriveFont(Font.BOLD), antiAlias, fractionalMetrics, spacing);
			this.italicFont = new CustomFont(this.baseFont.deriveFont(Font.ITALIC), antiAlias, fractionalMetrics, spacing);
			this.boldItalicFont = new CustomFont(this.baseFont.deriveFont(Font.BOLD | Font.ITALIC), antiAlias, fractionalMetrics, spacing);
		}
	}

	private void setupMinecraftColorCodes()
	{
		for (int index = 0; index < 32; index++)
		{
			final int alpha = (index >> 3 & 0x1) * 85;
			int red = (index >> 2 & 0x1) * 170 + alpha;
			int green = (index >> 1 & 0x1) * 170 + alpha;
			int blue = (index & 0x1) * 170 + alpha;
			if (index == 6)
			{
				red += 85;
			}
			if (index >= 16)
			{
				red /= 4;
				green /= 4;
				blue /= 4;
			}
			this.colorCode[index] = (red & 0xFF) << 16 | (green & 0xFF) << 8 | blue & 0xFF;
		}
	}

	public Color getColor(int colorCode, float alpha)
	{
		return new Color((colorCode >> 16 & 0xFF) / 255.0F, (colorCode >> 8 & 0xFF) / 255.0F, (colorCode & 0xFF) / 255.0F, alpha);
	}

	public void setAntiAlias(boolean antiAlias)
	{
		this.font.setAntiAlias(antiAlias);
		this.boldFont.setAntiAlias(antiAlias);
		this.italicFont.setAntiAlias(antiAlias);
		this.boldItalicFont.setAntiAlias(antiAlias);
	}

	private String randomString(String input)
	{
		String output = "";
		for (final char c : input.toCharArray())
		{
			if (Arrays.asList(allowedCharacters).indexOf(c) > 0)
			{
				final int index = this.random.nextInt(allowedCharacters.length);
				output = output + allowedCharacters[index];
			}
		}
		return output;
	}

	private void drawLine(double x, double y, double x1, double y1, float width)
	{
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glLineWidth(width);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x1, y1);
		GL11.glEnd();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

	public float stringWidth(String input, FontFormat format)
	{
		if (input == null)
		{
			return 0;
		}
		int width = 0;
		CustomFont currentFont;
		switch (format)
		{
		case BOLD:
			currentFont = this.boldFont;
			break;
		case ITALIC:
			currentFont = this.italicFont;
			break;
		case NONE:
			currentFont = this.font;
			break;
		case BOLD_ITALIC:
			currentFont = this.boldItalicFont;
			break;
		default:
			currentFont = this.font;
			break;
		}
		boolean bold = false;
		boolean italic = false;
		final int size = input.length();
		for (int i = 0; i < size; i++)
		{
			final char character = input.charAt(i);
			if (character == '\247' && i < size)
			{
				final int colorIndex = "0123456789abcdefklmnor".indexOf(character);
				if (colorIndex < 16)
				{
					bold = false;
					italic = false;
				} else if (colorIndex == 17)
				{
					bold = true;
					if (italic)
					{
						currentFont = this.boldItalicFont;
					} else
					{
						currentFont = this.boldFont;
					}
				} else if (colorIndex == 20)
				{
					italic = true;
					if (bold)
					{
						currentFont = this.boldItalicFont;
					} else
					{
						currentFont = this.italicFont;
					}
				} else if (colorIndex == 21)
				{
					bold = false;
					italic = false;
					currentFont = this.font;
				}
				i++;
			} else if (character < currentFont.charDatas.length && character >= 0)
			{
				width += currentFont.charDatas[character].width - 8 + currentFont.charOffset;
			}
		}
		return width / 2;
	}

	public float stringHeight(String input, FontFormat format, FontFormat... formats)
	{
		float additive = 0;
		if (format == FontFormat.UNDERLINE)
		{
			additive = 2f;
		}
		for (final FontFormat fontFormat : formats)
		{
			if (fontFormat == FontFormat.UNDERLINE)
			{
				additive = 2f;
			}
		}
		switch (format)
		{
		case BOLD:
			return this.boldFont.getStringHeight(input) + additive;
		case ITALIC:
			return this.italicFont.getStringHeight(input) + additive;
		case NONE:
			return this.font.getStringHeight(input) + additive;
		case BOLD_ITALIC:
			return this.boldItalicFont.getStringHeight(input) + additive;
		default:
			return this.font.getStringHeight(input) + additive;
		}
	}

	public List<String> listFormattedStringToWidth(String s, int width, FontFormat format)
	{
		return Arrays.asList(wrapFormattedStringToWidth(s, width, format).split("\n"));
	}

	public String wrapFormattedStringToWidth(String s, float width, FontFormat format)
	{
		switch (format)
		{
		case BOLD:
			return this.boldFont.wrapFormattedStringToWidth(s, width);
		case ITALIC:
			return this.italicFont.wrapFormattedStringToWidth(s, width);
		case NONE:
			return this.font.wrapFormattedStringToWidth(s, width);
		case BOLD_ITALIC:
			return this.boldItalicFont.wrapFormattedStringToWidth(s, width);
		default:
			return this.font.wrapFormattedStringToWidth(s, width);
		}
	}

	public void setFont(Font f)
	{
		generateFonts(f, f.getSize(), this.spacing, this.antiAlias, this.fractionalMetrics);
	}
}
