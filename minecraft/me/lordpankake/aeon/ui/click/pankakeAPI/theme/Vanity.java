package me.lordpankake.aeon.ui.click.pankakeAPI.theme;

import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_LINE_LOOP;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2d;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeDraw;
import me.lordpankake.aeon.ui.click.pankakeAPI.ThemeBase;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ModuleSlider;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Button;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Frame;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Slider;
import me.lordpankake.aeon.util.RenderUtils;
import net.minecraft.client.Minecraft;

public class Vanity extends ThemeBase
{
	public Vanity()
	{
		frameGripHeight = 15;
		buttonHeight = 15;
		frameWidth = 80;
		sliderHeight = 11;
		toggleRect = new Rectangle(2, 2, frameGripHeight - 2, frameGripHeight - 2);
	
			this.setFonts(new Font("Segoe UI", Font.PLAIN, 18), new Font("Segoe UI", Font.PLAIN, 20), new Font("Segoe UI", Font.PLAIN, 15));
		
	}

	@Override
	public void onFrameRender(Frame f)
	{
		PankakeDraw.enableNoTextures();
		RenderUtils.setColor(new Color(77, 77, 77, 222));
		glBegin(GL_QUADS);
		{
			glVertex2d(f.getArea().x, f.getArea().y);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y + f.getArea().height);
			glVertex2d(f.getArea().x, f.getArea().y + f.getArea().height);
		}
		glEnd();
		RenderUtils.setColor(new Color(115, 115, 115));
		glBegin(GL_LINE_LOOP);// GL_LINE_LOOP
		{
			glVertex2d(f.getX() + f.getWidth() - toggleRect.x, f.getArea().y + toggleRect.y);
			glVertex2d(f.getX() + f.getWidth() - toggleRect.x - toggleRect.width, f.getArea().y + toggleRect.y);
			glVertex2d(f.getX() + f.getWidth() - toggleRect.x - toggleRect.width, f.getArea().y + toggleRect.y + toggleRect.height);
			glVertex2d(f.getX() + f.getWidth() - toggleRect.x, f.getArea().y + toggleRect.x + toggleRect.height);
		}
		glEnd();
		RenderUtils.setColor(new Color(115, 115, 115, 2));
		glBegin(GL_QUADS);// GL_LINE_LOOP
		{
			glVertex2d(f.getX() + f.getWidth() - 4, f.getArea().y);
			glVertex2d(f.getX() + f.getWidth() - 18, f.getArea().y);
			glVertex2d(f.getX() + f.getWidth() - 18, f.getArea().y + frameGripHeight);
			glVertex2d(f.getX() + f.getWidth() - 4, f.getArea().y + frameGripHeight);
		}
		glEnd();
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(f.getArea().x, f.getArea().y);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y + f.getArea().height);
			glVertex2d(f.getArea().x, f.getArea().y + f.getArea().height);
		}
		glEnd();
		glBegin(GL_LINES);
		{
			glVertex2d(f.getArea().x, f.getArea().y + frameGripHeight);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y + frameGripHeight);
		}
		glEnd();
		PankakeDraw.disableNoTextures();
		PankakeDraw.drawLargeString(f.getName(), f.getX() + 2.5F, f.getY() + 3.5F, 0x222222);
		PankakeDraw.drawLargeString(f.getName(), f.getX() + 2, f.getY() + 3, 0xffffff);
		if (f.getOpened())
		{
			PankakeDraw.drawString("O", f.getX() + f.getWidth() - toggleRect.x - toggleRect.width + 2F, f.getY() + toggleRect.y + 3F, 0xffffff, false);
		} else
		{
			PankakeDraw.drawString("_", f.getX() + f.getWidth() - toggleRect.x - toggleRect.width + 3.5F, f.getY() - toggleRect.y + 2.5F, 0xffffff, false);
		}
	}

	@Override
	protected void onButtonRender(Button b)
	{
		int offsetX = b.getParent().getX() + b.getX();
		int offsetY = b.getParent().getY();
		PankakeDraw.enableNoTextures();
		if (b.getState())
		{
			RenderUtils.setColor(new Color(83, 83, 83, 200));
		} else
		{
			RenderUtils.setColor(new Color(66, 66, 66, 200));
		}
		glBegin(GL_QUADS);
		{
			glVertex2d(offsetX, offsetY + b.getArea().y);
			glVertex2d(offsetX + b.getArea().width, offsetY + b.getArea().y);
			glVertex2d(offsetX + b.getArea().width, offsetY + b.getArea().y + b.getArea().height);
			glVertex2d(offsetX, offsetY + b.getArea().y + b.getArea().height);
		}
		glEnd();
		RenderUtils.setColor(new Color(115, 115, 115));
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(offsetX, offsetY + b.getArea().y);
			glVertex2d(offsetX + b.getArea().width, offsetY + b.getArea().y);
			glVertex2d(offsetX + b.getArea().width, offsetY + b.getArea().y + b.getArea().height);
			glVertex2d(offsetX, offsetY + b.getArea().y + b.getArea().height);
		}
		glEnd();
		final int centeredX = offsetX + b.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getStringWidth(b.getName()) / 2;
		final int centeredY = b.getY() + offsetY + b.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		if (b.getState())
		{
			PankakeDraw.drawString(b.getName(), centeredX, centeredY, RenderUtils.toRGBA(new Color(10, 218, 10, 244)), false);
		} else
		{
			PankakeDraw.drawString(b.getName(), centeredX, centeredY, RenderUtils.toRGBA(new Color(0, 10, 218, 244)), false);
		}
	}

	@Override
	protected void onSliderRender(Slider s)
	{
		int offsetX = s.getParent().getX();
		int offsetY = s.getParent().getY();
		PankakeDraw.enableNoTextures();
		RenderUtils.setColor(new Color(82, 83, 84, 160));
		glBegin(GL_QUADS);
		{
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y + s.getArea().height);
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y + s.getArea().height);
		}
		glEnd();
		final double sliderPercentage = (s.getValue() - s.getMin()) / (s.getMax() - s.getMin());
		RenderUtils.setColor(new Color(133, 133, 133));
		glBegin(GL_QUADS);
		{
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + (s.getArea().width * sliderPercentage), offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + (s.getArea().width * sliderPercentage), offsetY + s.getArea().y + s.getArea().height);
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y + s.getArea().height);
		}
		glEnd();
		RenderUtils.setColor(new Color(144, 144, 144));
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y + s.getArea().height);
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y + s.getArea().height);
		}
		glEnd();
		final int centeredX = s.getX() + offsetX + s.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(s.getName()) / 2;
		final int centeredY = s.getY() + (-1) + offsetY + s.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		if (s instanceof ModuleSlider)
		{
			PankakeDraw.drawSmallString(((ModuleSlider) s).getModule().getName() + ":" + s.getName(), offsetX + 3, centeredY + 2, 0xffffff);
		} else
		{
			PankakeDraw.drawSmallString(s.getName(), offsetX + 2, centeredY + 2, 0xffffff);
		}
		// String percent = ((this.value / this.max) * 100) + "";
		String dispText = ((s.getValue() - s.getMin()) / (s.getMax() - s.getMin())) * 100 + "";
		if (s.drawPercent && dispText.length() > 5)
		{
			dispText = dispText.substring(0, 5) + "%";
		} else
		{
			dispText = s.getValue() + "";
			if (dispText.length() > 5)
			{
				dispText = dispText.substring(0, 5);
			}
		}
		PankakeDraw.drawSmallString(dispText, offsetX + s.getWidth() - 3 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(dispText), centeredY + 2, 0xffffff);
	}
}
