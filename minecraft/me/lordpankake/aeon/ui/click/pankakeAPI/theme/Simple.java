package me.lordpankake.aeon.ui.click.pankakeAPI.theme;

import static org.lwjgl.opengl.GL11.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeDraw;
import me.lordpankake.aeon.ui.click.pankakeAPI.ThemeBase;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ModuleButton;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ModuleSlider;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Button;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Component;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Frame;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Slider;
import me.lordpankake.aeon.util.RenderUtils;
import net.minecraft.client.Minecraft;

public class Simple extends ThemeBase
{
	public Simple()
	{
		frameGripHeight = 14;
		buttonHeight = 13;
		frameWidth = 72;
		
			this.setFonts(new Font("Segoe UI Semilight", Font.PLAIN, 19), new Font("Segoe UI", Font.PLAIN, 20), new Font("Segoe UI", Font.PLAIN, 16));
		
	}

	@Override
	public void onFrameRender(Frame f)
	{
		PankakeDraw.enableNoTextures();
		RenderUtils.setColor(f.compColor);
		glBegin(GL_QUADS);
		{
			glVertex2d(f.getArea().x, f.getArea().y);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y + f.getArea().height);
			glVertex2d(f.getArea().x, f.getArea().y + f.getArea().height);
		}
		glEnd();
		RenderUtils.setColor(f.compOutlineColor);
		glColor4f(0f, 0f, 0f, 1f);
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(f.getX() + f.getWidth() - this.toggleRect.x, f.getArea().y);
			glVertex2d(f.getX() + f.getWidth() - this.toggleRect.x - this.toggleRect.width, f.getArea().y);
			glVertex2d(f.getX() + f.getWidth() - this.toggleRect.x - this.toggleRect.width, f.getArea().y + 14);
			glVertex2d(f.getX() + f.getWidth() - this.toggleRect.x, f.getArea().y + 14);
		}
		glEnd();
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(f.getArea().x, f.getArea().y);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y + f.getArea().height);
			glVertex2d(f.getArea().x, f.getArea().y + f.getArea().height);
		}
		glEnd();
		glBegin(GL_LINES);
		{
			glVertex2d(f.getArea().x, f.getArea().y + frameGripHeight);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y + frameGripHeight);
		}
		glEnd();
		PankakeDraw.disableNoTextures();
		PankakeDraw.drawString(f.getName(), f.getX() + 2, f.getY() + 3, 0xffffff, false);
		if (f.getOpened())
		{
			PankakeDraw.drawString("O", f.getX() + f.getWidth() - this.toggleRect.x - this.toggleRect.width + 2, f.getY() + 3, 0xffffff, false);
		} else
		{
			PankakeDraw.drawString("_", f.getX() + f.getWidth() - this.toggleRect.x - this.toggleRect.width + 4, f.getY() - 1, 0xffffff, false);
		}
	}

	@Override
	protected void onButtonRender(Button b)
	{
		int offsetX = b.getParent().getX() + b.getX();
		int offsetY = b.getParent().getY();
		PankakeDraw.enableNoTextures();
		RenderUtils.setColor(b.compColor);
		glBegin(GL_QUADS);
		{
			glVertex2d(offsetX, offsetY + b.getArea().y);
			glVertex2d(offsetX + b.getArea().width, offsetY + b.getArea().y);
			glVertex2d(offsetX + b.getArea().width, offsetY + b.getArea().y + b.getArea().height);
			glVertex2d(offsetX, offsetY + b.getArea().y + b.getArea().height);
		}
		glEnd();
		glColor4f(0f, 0f, 0f, 1f);
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(offsetX, offsetY + b.getArea().y);
			glVertex2d(offsetX + b.getArea().width, offsetY + b.getArea().y);
			glVertex2d(offsetX + b.getArea().width, offsetY + b.getArea().y + b.getArea().height);
			glVertex2d(offsetX, offsetY + b.getArea().y + b.getArea().height);
		}
		glEnd();
		final int centeredX = offsetX + b.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getStringWidth(b.getName()) / 2;
		final int centeredY = b.getY() + offsetY + b.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		PankakeDraw.drawString(b.getName(), centeredX, centeredY, 0xffffff, false);
	}

	@Override
	protected void onSliderRender(Slider s)
	{
		int offsetX = s.getParent().getX();
		int offsetY = s.getParent().getY();
		PankakeDraw.enableNoTextures();
		RenderUtils.setColor(s.compColor);
		glBegin(GL_QUADS);
		{
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y + s.getArea().height);
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y + s.getArea().height);
		}
		glEnd();
		final double sliderPercentage = (s.getValue() - s.getMin()) / (s.getMax() - s.getMin());
		RenderUtils.setColor(s.slidColor);
		glBegin(GL_QUADS);
		{
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + (s.getArea().width * sliderPercentage), offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + (s.getArea().width * sliderPercentage), offsetY + s.getArea().y + s.getArea().height);
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y + s.getArea().height);
		}
		glEnd();
		glColor4f(0f, 0f, 0f, 1f);
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y + s.getArea().height);
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y + s.getArea().height);
		}
		glEnd();
		final int centeredX = s.getX() + offsetX + s.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(s.getName()) / 2;
		final int centeredY = s.getY() + (-1) + offsetY + s.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		if (s instanceof ModuleSlider)
		{
			PankakeDraw.drawSmallString(((ModuleSlider) s).getModule().getName() + ":" + s.getName(), offsetX + 3, centeredY + 2, 0xffffff);
		} else
		{
			PankakeDraw.drawSmallString(s.getName(), offsetX + 2, centeredY + 2, 0xffffff);
		}
		String dispText = ((s.getValue() - s.getMin()) / (s.getMax() - s.getMin())) * 100 + "";
		if (s.drawPercent && dispText.length() > 5)
		{
			dispText = dispText.substring(0, 5) + "%";
		} else
		{
			dispText = s.getValue() + "";
			if (dispText.length() > 5)
			{
				dispText = dispText.substring(0, 5);
				if (dispText.substring(dispText.indexOf(".")).length() > 1)
				{
					dispText = dispText.substring(0, dispText.indexOf(".") + 2);
				}
			}
			
			
		}
		PankakeDraw.drawSmallString(dispText, offsetX + s.getWidth() - 3 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(dispText), centeredY + 2, 0xffffff);
	}
}
