package me.lordpankake.aeon.ui.click.pankakeAPI.theme;

import static org.lwjgl.opengl.GL11.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeDraw;
import me.lordpankake.aeon.ui.click.pankakeAPI.ThemeBase;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ModuleButton;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ModuleSlider;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Button;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Frame;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Slider;
import me.lordpankake.aeon.util.RenderUtils;
import me.lordpankake.aeon.util.ScreenUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class Falcon extends ThemeBase
{
	public Falcon()
	{
		frameGripHeight = 15;
		buttonHeight = 15;
		frameWidth = 67;
		toggleRect = new Rectangle(2, 2, frameGripHeight - 2, frameGripHeight - 2);
		this.setFonts(new Font("Segoe UI", Font.PLAIN, 18), new Font("Segoe UI", Font.PLAIN, 20), new Font("Segoe UI", Font.PLAIN, 15));
	}

	@Override
	public void onFrameRender(Frame f)
	{
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("Aeon/Falcon.png"));
		ScreenUtils util = Aeon.getInstance().getUtils().getuScreen();
		if (f.getOpened())
		{
			util.drawTexturedModalRectD(-1 + f.getX(), 3 + f.getY(), 95, 0, f.getWidth() + 1, f.getHeight());// middle
			util.drawTexturedModalRect(-1 + f.getX(), f.getY() + f.getHeight() - 11, 0, 43, f.getWidth() + 1, 23);// bottom
			util.drawTexturedModalRect(-1 + f.getX(), f.getY(), 0, 0, f.getWidth() + 1, 27);// top
			util.drawTexturedModalRect(f.getX() + f.getWidth() - this.toggleRect.width - this.toggleRect.x, f.getY() + this.toggleRect.y, 69, 0, 13, 23);
		} else
		{
			util.drawTexturedModalRect(-1 + f.getX(), f.getY(), 0, 0, f.getWidth() + 1, this.frameGripHeight + 2);// top
			util.drawTexturedModalRect(f.getX() + f.getWidth() - this.toggleRect.width - this.toggleRect.x, f.getY() + this.toggleRect.y, 82, 0, 13, 23);
		}
		PankakeDraw.drawLargeString(f.getName(), f.getX() + 2.5F, f.getY() + 4.5F, 0x000000);
		PankakeDraw.drawLargeString(f.getName(), f.getX() + 2, f.getY() + 4, 0x999999);
	}

	@Override
	protected void onButtonRender(Button b)
	{
		if (true)
		{
			// return;
		}
		int offsetX = b.getParent().getX() + b.getX() - 1;
		int offsetY = b.getParent().getY() + b.getY();
		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("Aeon/Falcon.png"));
		ScreenUtils util = Aeon.getInstance().getUtils().getuScreen();
		final int centeredX = 4 + offsetX + b.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getStringWidth(b.getName()) / 2;
		final int centeredY = offsetY + b.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		if (b.getState())
		{
			util.drawTexturedModalRect(offsetX, offsetY - 1, 0, 70, b.getWidth() - 8, 16);
			util.drawTexturedModalRect(offsetX + 12, offsetY - 1, 14, 70, b.getWidth() - 8, 16);
			PankakeDraw.drawString(b.getName(), offsetX + 3F, centeredY + 1F, RenderUtils.toRGBA(new Color(0, 0, 0, 244)), false);
			PankakeDraw.drawString(b.getName(), offsetX + 2.5F, centeredY + 0.5F, RenderUtils.toRGBA(new Color(222, 222, 222, 244)), false);
		} else
		{
			util.drawTexturedModalRect(offsetX, offsetY - 1, 0, 87, b.getWidth() - 8, 16);
			util.drawTexturedModalRect(offsetX + 12, offsetY - 1, 14, 87, b.getWidth() - 8, 16);
			PankakeDraw.drawString(b.getName(), offsetX + 3F, centeredY + 1F, RenderUtils.toRGBA(new Color(0, 0, 0, 244)), false);
			PankakeDraw.drawString(b.getName(), offsetX + 2.5F, centeredY + 0.5F, RenderUtils.toRGBA(new Color(88, 88, 88, 244)), false);
		}
	}

	@Override
	protected void onSliderRender(Slider s)
	{
		int offsetX = s.getParent().getX();
		int offsetY = s.getParent().getY();
		PankakeDraw.enableNoTextures();
		RenderUtils.setColor(new Color(44, 44, 44, 255));
		glBegin(GL_QUADS);
		{
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y + s.getArea().height);
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y + s.getArea().height);
		}
		glEnd();
		final double sliderPercentage = (s.getValue() - s.getMin()) / (s.getMax() - s.getMin());
		RenderUtils.setColor(new Color(66, 66, 66, 255));
		glBegin(GL_QUADS);
		{
			glVertex2d(offsetX + 0.5F + s.getArea().x, offsetY + s.getArea().y + 0.5F);
			glVertex2d(offsetX + s.getArea().x + (s.getArea().width * sliderPercentage), offsetY + s.getArea().y + 0.5F);
			glVertex2d(offsetX + s.getArea().x + (s.getArea().width * sliderPercentage), offsetY + s.getArea().y + s.getArea().height - 0.5F);
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y + s.getArea().height - 0.5F);
		}
		glEnd();
		RenderUtils.setColor(new Color(111, 111, 111, 255));
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(offsetX + 0.5F + s.getArea().x, offsetY + s.getArea().y + 0.5F);
			glVertex2d(offsetX + s.getArea().x + (s.getArea().width * sliderPercentage) - 0.5F, offsetY + s.getArea().y + 0.5F);
			glVertex2d(offsetX + s.getArea().x + (s.getArea().width * sliderPercentage) - 0.5F, offsetY + s.getArea().y + s.getArea().height - 0.5F);
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y + s.getArea().height - 0.5F);
		}
		glEnd();
		glColor4f(0f, 0f, 0f, 1f);
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y);
			glVertex2d(offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y + s.getArea().height);
			glVertex2d(offsetX + s.getArea().x, offsetY + s.getArea().y + s.getArea().height);
		}
		glEnd();
		final int centeredX = s.getX() + offsetX + s.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(s.getName()) / 2;
		final int centeredY = s.getY() + (-1) + offsetY + s.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		if (s instanceof ModuleSlider)
		{
			PankakeDraw.drawSmallString(((ModuleSlider) s).getModule().getName() + ":" + s.getName(), offsetX + 3, centeredY + 2, 0xffffff);
		} else
		{
			PankakeDraw.drawSmallString(s.getName(), offsetX + 2, centeredY + 2, 0xffffff);
		}
		String dispText = ((s.getValue() - s.getMin()) / (s.getMax() - s.getMin())) * 100 + "";
		if (s.drawPercent && dispText.length() > 5)
		{
			dispText = dispText.substring(0, 5) + "%";
		} else
		{
			dispText = s.getValue() + "";
			if (dispText.length() > 5)
			{
				dispText = dispText.substring(0, 5);
				if (dispText.substring(dispText.indexOf(".")).length() > 1)
				{
					dispText = dispText.substring(0, dispText.indexOf(".") + 2);
				}
			}
		}
		PankakeDraw.drawSmallString(dispText, offsetX + s.getWidth() - 3 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(dispText), centeredY + 2, 0xffffff);
	}
}
