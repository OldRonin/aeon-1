package me.lordpankake.aeon.ui.click.pankakeAPI.theme;

import static org.lwjgl.opengl.GL11.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import org.lwjgl.opengl.GL11;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeDraw;
import me.lordpankake.aeon.ui.click.pankakeAPI.ThemeBase;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ModuleSlider;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Button;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Frame;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Slider;
import me.lordpankake.aeon.util.RenderUtils;
import me.lordpankake.aeon.util.ScreenUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class Blackshades extends ThemeBase
{
	public Blackshades()
	{
		frameGripHeight = 15;
		buttonHeight = 15;
		frameWidth = 80;
		toggleRect = new Rectangle(2, 2, frameGripHeight - 2, frameGripHeight - 2);
		this.setFonts(new Font("Segoe UI", Font.PLAIN, 18), new Font("Segoe UI", Font.PLAIN, 20), new Font("Segoe UI", Font.PLAIN, 15));
	}

	@Override
	public void onFrameRender(Frame f)
	{
		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("Aeon/blackshades.png"));
		ScreenUtils util = Aeon.getInstance().getUtils().getuScreen();
		if (f.getOpened())
		{
			util.drawTexturedModalRectD(-1 + f.getX(), 3 + f.getY(), 108, 0, 10, f.getHeight() - 12);// middle
			util.drawTexturedModalRectD(-1 + f.getX() + 7.5, 3 + f.getY(), 115, 0, f.getWidth()+ 1 , f.getHeight() - 12);// middle

			util.drawTexturedModalRect(-1 + f.getX(), f.getY() + f.getHeight() - 21, 0, 43, f.getWidth() + 2, 23);// bottom
			util.drawTexturedModalRect(-1 + f.getX(), f.getY(), 0, 0, f.getWidth() + 1, 27);// top
		
			util.drawTexturedModalRect(f.getX() + f.getWidth() - this.toggleRect.width - this.toggleRect.x, f.getY() + this.toggleRect.y, 82, 3, 13, 23);

		} else
		{
			util.drawTexturedModalRect(-1+ f.getX(),  f.getY(), 0, 0, f.getWidth() + 1, this.frameGripHeight);// top
			util.drawTexturedModalRect(-1+ f.getX(),  f.getY()+this.frameGripHeight, 0, 54, f.getWidth() + 1, this.frameGripHeight - 3);// bottom
		
			util.drawTexturedModalRect(f.getX() + f.getWidth() - this.toggleRect.width - this.toggleRect.x, f.getY() + this.toggleRect.y, 95, 3, 13, 23);

		}
		PankakeDraw.drawLargeString(f.getName(), f.getX() + 2.5F, f.getY() + 3.5F, 0x222222);
		PankakeDraw.drawLargeString(f.getName(), f.getX() + 2, f.getY() + 3, 0xffffff);
	}

	@Override
	protected void onButtonRender(Button b)
	{
		int offsetX = b.getParent().getX() + b.getX();
		int offsetY = b.getParent().getY() + b.getY();
		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("Aeon/blackshades.png"));
		ScreenUtils util = Aeon.getInstance().getUtils().getuScreen();
		final int centeredX = offsetX + b.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getStringWidth(b.getName()) / 2;
		final int centeredY = offsetY + b.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		if (b.getState())
		{
			util.drawTexturedModalRect(offsetX , offsetY - 1, 0, 67, 20, 16);
			util.drawTexturedModalRect(offsetX + 12, offsetY - 1, 14, 67, 70, 16);
			PankakeDraw.drawString(b.getName(), centeredX + 0.5F, centeredY + 0.5F, RenderUtils.toRGBA(new Color(0, 0, 0, 244)), false);
			PankakeDraw.drawString(b.getName(), centeredX, centeredY, RenderUtils.toRGBA(new Color(222, 222, 222, 244)), false);
		} else
		{
			util.drawTexturedModalRect(offsetX, offsetY- 1, 0, 83, b.getWidth() - 8, 16);
			util.drawTexturedModalRect(offsetX + 12, offsetY- 1, 14, 83, b.getWidth() - 8, 16);
			PankakeDraw.drawString(b.getName(), centeredX + 0.5F, centeredY + 0.5F, RenderUtils.toRGBA(new Color(0, 0, 0, 244)), false);
			PankakeDraw.drawString(b.getName(), centeredX, centeredY, RenderUtils.toRGBA(new Color(155, 155, 155, 244)), false);
		}
	}

	@Override
	protected void onSliderRender(Slider s)
	{
		int offsetX = s.getParent().getX();
		int offsetY = s.getParent().getY();
		PankakeDraw.enableNoTextures();
		final double sliderPercentage = (s.getValue() - s.getMin()) / (s.getMax() - s.getMin());
		PankakeDraw.fillRoundedRect(offsetX + s.getArea().x, offsetY + s.getArea().y, (float) (offsetX + s.getArea().x + (s.getArea().width * sliderPercentage)), offsetY + s.getArea().y + s.getArea().height, 2, new Color(62, 67, 69, 255));
		Color glowColor = new Color(33, 33, 33, 255);
		Color glowColorTrans = new Color(33, 33, 33, 122);
		PankakeDraw.drawRoundedRect(offsetX + s.getArea().x - 0.5F, offsetY + s.getArea().y, offsetX + s.getArea().x + s.getArea().width + 1, offsetY + s.getArea().y + s.getArea().height, 2, glowColor);
		PankakeDraw.drawRoundedRect(offsetX + s.getArea().x, offsetY + s.getArea().y + 0.5F, offsetX + s.getArea().x + s.getArea().width + 0.5F, offsetY + s.getArea().y + s.getArea().height - 0.5F, 2, glowColorTrans);
		PankakeDraw.drawRoundedRect(offsetX + s.getArea().x - 1F, offsetY + s.getArea().y - 0.5F, offsetX + s.getArea().x + s.getArea().width + 1.5F, offsetY + s.getArea().y + s.getArea().height + 0.5F, 2, glowColorTrans);
		final int centeredX = s.getX() + offsetX + s.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(s.getName()) / 2;
		final int centeredY = s.getY() + (-1) + offsetY + s.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		if (s instanceof ModuleSlider)
		{
			PankakeDraw.drawSmallString(((ModuleSlider) s).getModule().getName() + ":" + s.getName(), offsetX + 3, centeredY + 2, 0xffffff);
		} else
		{
			PankakeDraw.drawSmallString(s.getName(), offsetX + 2, centeredY + 2, 0xffffff);
		}
		// String percent = ((this.value / this.max) * 100) + "";
		String dispText = ((s.getValue() - s.getMin()) / (s.getMax() - s.getMin())) * 100 + "";
		if (s.drawPercent && dispText.length() > 5)
		{
			dispText = dispText.substring(0, 5) + "%";
		} else
		{
			dispText = s.getValue() + "";
			if (dispText.length() > 5)
			{
				dispText = dispText.substring(0, 5);
			}
		}
		PankakeDraw.drawSmallString(dispText, offsetX + s.getWidth() - 3 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(dispText), centeredY + 2, 0xffffff);
	}
}
