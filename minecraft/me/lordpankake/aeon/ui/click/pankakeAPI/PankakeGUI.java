package me.lordpankake.aeon.ui.click.pankakeAPI;

import java.awt.Rectangle;
import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.core.handling.module.Initlizer;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.annotations.IncludeGUI;
import me.lordpankake.aeon.modules.mod.player.Flight;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.*;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.*;
import me.lordpankake.aeon.ui.click.pankakeAPI.theme.*;
import me.lordpankake.aeon.util.FileUtils;
import me.lordpankake.aeon.util.ScreenUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.ResourceLocation;

public class PankakeGUI extends GuiScreen
{
	public static ArrayList<Frame> frames = new ArrayList<Frame>();
	public static ArrayList<ThemeBase> themes = new ArrayList<ThemeBase>();
	public static ThemeBase theme = new Simple();
	public static boolean needsRebuild = true;

	public PankakeGUI()
	{
		Log.write("Adding Themes");
		rebuildGUI(theme);
		loadFrames();
	}

	public void rebuildGUI(ThemeBase theme)
	{
		this.theme = theme;
		frames.clear();
		int y = 0;
		y = addModuleFrames(0, y);
		y = addValuesFrame(0, y);
		y = addThemesFrame(0, y);
		for (final Frame f : frames)
		{
			f.setOpen(false);
		}
		loadFrames();
		needsRebuild = false;
	}

	private int addThemesFrame(int x, int y)
	{
		final int frameWidth = theme.frameWidth;
		final int buttonHeight = theme.buttonHeight;
		int i = 0;
		frames.add(0, new Frame(new Rectangle(x, y, frameWidth, theme.frameGripHeight + 2), "Themes"));
		for (ThemeBase tb : themes)
		{
			frames.get(0).addComponent(new ThemeButton(new Rectangle(2, theme.frameGripHeight + 2 + i, frameWidth - 4, buttonHeight), tb.getClass().getSimpleName(), frames.get(0), tb));
			i += buttonHeight + 2;
		}
		return y;
	}

	private int addModuleFrames(int x, int y)
	{
		final int buttonHeight = theme.buttonHeight;
		final int frameWidth = theme.frameWidth;
		for (ModuleType m : ModuleType.values())
		{
			if (m != ModuleType.Auto)
			{
				frames.add(new Frame(new Rectangle(0, y, frameWidth, theme.frameGripHeight + 2), m.toString()));
				x += frameWidth + 1;
				y += theme.frameGripHeight + 5;
			}
		}
		for (final Frame f : frames)
		{
			int i = 0;
			for (final Module m : Aeon.getInstance().getUtils().getHack().getModules())
			{
				if (m.getType().toString().equalsIgnoreCase(f.getName()) && m.shouldDrawToUI())
				{
					final ModuleButton mb = new ModuleButton(new Rectangle(2, theme.frameGripHeight + 2 + i, f.getWidth() - 4, buttonHeight), m.getName(), f, m);
					f.addComponent(mb);
					i += buttonHeight + 2;
				}
			}
		}
		return y;
	}

	private int addValuesFrame(int x, int y)
	{
		final int buttonHeight = theme.buttonHeight;
		final int frameWidth = theme.frameWidth;
		frames.add(0, new Frame(new Rectangle(x, y, frameWidth, theme.frameGripHeight + 2), "Values"));
		for (final Module m : Aeon.getInstance().getUtils().getHack().getModules())
		{
			if (m.getClass().getDeclaredFields().length > 0)
			{
				for (final Field field : m.getClass().getDeclaredFields())
				{
					field.setAccessible(true);
					if (field.isAnnotationPresent(IncludeGUI.class) && (field.getType().equals(int.class) || field.getType().equals(double.class) || field.getType().equals(float.class)) && field.isAccessible())
					{
						Object value;
						try
						{
							value = field.get(m);
							float min = field.getAnnotation(IncludeGUI.class).min();
							float max = field.getAnnotation(IncludeGUI.class).max();
							ModuleSlider ms = new ModuleSlider(new Rectangle(2, frames.get(0).getHeight(), frames.get(0).getWidth() - 4, theme.sliderHeight), field.getName(), frames.get(0), min, max, (Float) value, m, field);
							// ModuleSlider ms = new ModuleSlider(new
							// Rectangle(2, frames.get(0).getHeight(),
							// frames.get(0).getWidth() - 4, 12),
							// field.getName(), frames.get(0), min, max, (Float)
							// value, m, field);
							frames.get(0).addComponent(ms);
						} catch (IllegalArgumentException e)
						{
							e.printStackTrace();
						} catch (IllegalAccessException e)
						{
							e.printStackTrace();
						} catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		}
		y += theme.frameGripHeight + 5;
		return y;
	}

	@Override
	public void mouseClicked(int x, int y, int mode)
	{
		int i = -1;
		boolean found = false;
		for (final Frame f : frames)
		{
			if (f.isMouseOver())
			{
				if (frames.indexOf(f) == frames.size() - 1)
				{
					found = true;
					for (final Frame f2 : frames)
					{
						if (f2 != f)
						{
							f2.isTopFrame = false;
							f2.isMouseOnGrip = false;
						}
					}
					f.isTopFrame = true;
					f.sendClick(x, y);
					// return; //replace found boolean with return statement?
				} else
				{
					i = frames.indexOf(f);
				}
			}
		}
		Frame temp = null;
		if (i != -1 && !found)
		{
			temp = frames.get(i);
			frames.remove(i);
			frames.add(temp);
			frames.get(frames.size() - 1).isTopFrame = true;
			frames.get(frames.size() - 1).sendClick(x, y);
		}
		super.mouseClicked(x, y, mode);
	}

	public static void loadFrames()
	{
		Log.write("Loading frames");
		final ArrayList<String> penis = (ArrayList<String>) FileUtils.read(new File("Aeon" + File.separator + "GUI" + File.separator + "Frames.txt"));
		for (final String line : penis)
		{
			try
			{
				final String[] splitInfo = line.toLowerCase().split(":");
				final String frameName = splitInfo[0];
				final int frameX = Integer.parseInt(splitInfo[1]);
				final int frameY = Integer.parseInt(splitInfo[2]);
				final boolean isOpen = Boolean.parseBoolean(splitInfo[3]);
				for (final Frame f : frames)
				{
					if (f.getName().equalsIgnoreCase(frameName))
					{
						f.setXY(frameX, frameY);
						f.setOpen(isOpen);
					}
				}
				if (frameY != 0)
				{
					needsRebuild = false;
				}
			} catch (final Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void saveFrames()
	{
		if (frames.size() > 4)
		{
			Log.write("Saving frames");
			final File saveFile = new File("Aeon" + File.separator + "GUI" + File.separator + "Frames.txt");
			final ArrayList<String> content = new ArrayList<String>();
			for (final Frame f : frames)
			{
				content.add(f.getName() + ":" + f.getX() + ":" + f.getY() + ":" + f.getOpened());
			}
			FileUtils.write(saveFile, content, true);
		}
	}
	
	public static void addThemes()
	{
		themes.add(new Blackshades());
		themes.add(new Falcon());
		themes.add(new DDong());
		themes.add(new Vanity());
		themes.add(new Simple());
		themes.add(new Conversion());
	}
}
