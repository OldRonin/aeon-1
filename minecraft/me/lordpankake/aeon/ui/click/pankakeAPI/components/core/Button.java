package me.lordpankake.aeon.ui.click.pankakeAPI.components.core;

import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2d;
import java.awt.Color;
import java.awt.Rectangle;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeDraw;
import me.lordpankake.aeon.ui.click.pankakeAPI.ThemeBase;
import me.lordpankake.aeon.ui.click.pankakeAPI.theme.Simple;
import me.lordpankake.aeon.util.RenderUtils;
import net.minecraft.client.Minecraft;

public class Button extends Component
{
	private boolean state;

	public Button(Rectangle rekt, String name, Component parent)
	{
		super(rekt, name, parent);
	}

	@Override
	public void render()
	{
	}

	protected void onClicked()
	{
		toggle();
		// Log.write(this.name + " has been clicked! State: " + state);
	}

	public void setState(boolean state)
	{
		this.state = state;
	}

	public boolean getState()
	{
		return this.state;
	}

	protected void toggle()
	{
		state = !state;
		if (state)
		{
			this.setColors(new Color(22, 111, 222, 88), this.compOutlineColor);
		} else
		{
			this.setColors(new Color(34, 38, 40, 111), this.compOutlineColor);
		}
	}
}
