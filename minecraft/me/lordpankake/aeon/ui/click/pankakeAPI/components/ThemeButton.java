package me.lordpankake.aeon.ui.click.pankakeAPI.components;

import java.awt.Rectangle;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.ui.click.pankakeAPI.ThemeBase;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Button;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Component;

public class ThemeButton extends Button
{
	private ThemeBase theme;
	public ThemeButton(Rectangle rekt, String name, Component parent, ThemeBase t)
	{
		super(rekt, name, parent);
		this.theme = t;
	}

	@Override
	protected void onClicked()
	{
		Aeon.getInstance().getUtils().kek.rebuildGUI(this.theme);
		super.onClicked();
	}
}
