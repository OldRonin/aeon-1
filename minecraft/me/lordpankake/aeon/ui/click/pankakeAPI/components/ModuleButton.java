package me.lordpankake.aeon.ui.click.pankakeAPI.components;

import java.awt.Rectangle;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Mouse;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeDraw;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Button;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Component;

public class ModuleButton extends Button
{
	public final Module mod;

	public ModuleButton(Rectangle rekt, String name, Component parent, Module m)
	{
		super(rekt, name, parent);
		this.mod = m;
		this.setState(m.getState());
	}

	@Override
	protected void onClicked()
	{
		super.onClicked();
		mod.toggle();
	}

	@Override
	protected void update()
	{
		if (this.mod.getState() != this.getState())
		{
			this.toggle();
		}
	}

	@Override
	public void render()
	{
		if (this instanceof ModuleButton && Aeon.getInstance().getUtils().getHack().getLoader().desc.getState())
		{
			if (isMouseOver())
			{
				final int mX = Mouse.getX() * Minecraft.getMinecraft().currentScreen.width / Minecraft.getMinecraft().displayWidth;
				final int mY = Minecraft.getMinecraft().currentScreen.height - Mouse.getY() * Minecraft.getMinecraft().currentScreen.height / Minecraft.getMinecraft().displayHeight;
				PankakeDraw.drawString(this.mod.getDescription(), 1 + this.getX() + this.getWidth() + this.parent.getX() + 0.5F, mY + 0.5F, 0x000000, false);
				PankakeDraw.drawString(this.mod.getDescription(), 1 + this.getX() + this.getWidth() + this.parent.getX() - 0.5F, mY - 0.5F, 0x000000, false);
				PankakeDraw.drawString(this.mod.getDescription(), 1 + this.getX() + this.getWidth() + this.parent.getX() - 0.5F, mY + 0.5F, 0x000000, false);
				PankakeDraw.drawString(this.mod.getDescription(), 1 + this.getX() + this.getWidth() + this.parent.getX() + 0.5F, mY - 0.5F, 0x000000, false);
				PankakeDraw.drawString(this.mod.getDescription(), 1 + this.getX() + this.getWidth() + this.parent.getX(), mY, 0xffffff, false);
			}
		}
	}
}
