package me.lordpankake.aeon.ui.click.pankakeAPI.components;

import java.awt.Rectangle;
import java.lang.reflect.Field;
import net.minecraft.client.Minecraft;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeDraw;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Component;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Frame;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Slider;
import me.lordpankake.aeon.util.RenderUtils;

public class ModuleSlider extends Slider
{
	public final Field field;
	public final Module mod;

	public ModuleSlider(Rectangle rekt, String name, Component parent, float min, float max, float value, Module c, Field f)
	{
		super(rekt, name, parent, min, max, value);
		this.field = f;
		this.mod = c;
	}

	public ModuleSlider(Rectangle rekt, String name, Component parent, double min, double max, double value, Module c, Field f)
	{
		super(rekt, name, parent, Float.parseFloat(min + ""), Float.parseFloat(max + ""), Float.parseFloat(value + ""));
		this.field = f;
		this.mod = c;
	}

	public Field getField()
	{
		return this.field;
	}

	public Module getModule()
	{
		return this.mod;
	}

	@Override
	protected void valueChanged(Object val)
	{
		try
		{
			Object value = val;
			field.setAccessible(true);
			field.set(mod, val);
		} catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		} catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
}
