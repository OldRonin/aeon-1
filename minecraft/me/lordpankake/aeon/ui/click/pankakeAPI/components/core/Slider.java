package me.lordpankake.aeon.ui.click.pankakeAPI.components.core;

import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2d;
import java.awt.Color;
import java.awt.Rectangle;
import org.lwjgl.input.Mouse;
import net.minecraft.client.Minecraft;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.util.RenderUtils;

public class Slider extends Component
{
	public boolean drawPercent = false;
	public boolean update;
	public boolean sliding;
	protected float min, max, value;
	public Color slidColor = new Color(255, 255, 255, 60);

	public Slider(Rectangle rekt, String name, Component parent, float min, float max, float value)
	{
		super(rekt, name, parent);
		this.min = min;
		this.max = max;
		this.value = value;
	}

	@Override
	protected void update()
	{
		if (!Mouse.isButtonDown(0) || !update)
		{
			((Frame)parent).childInUse = false;
			sliding = false;
			return;
		}
		
		int offsetX = 0;
		int offsetY = 0;
		if (parent != null)
		{
			offsetX = parent.getX();
			offsetY = parent.getY();
		}
		final int mX = (Mouse.getEventX() * Minecraft.getMinecraft().currentScreen.width / Minecraft.getMinecraft().displayWidth);
		final int mY = (Minecraft.getMinecraft().currentScreen.height - Mouse.getEventY() * Minecraft.getMinecraft().currentScreen.height / Minecraft.getMinecraft().displayHeight - 1) - this.getY();
		if (sliding)
		{
			((Frame)parent).childInUse = true;
			float mouse = (mX - offsetX - this.getX());
			valueChanged(this.value);
			this.value = (float) (min + mouse * (max - min) / this.getWidth());
			
			if (this.value > this.max)
			{
				this.value = this.max;
			}
			if (this.value < this.min)
			{
				this.value = this.min;
			}
		}
		else
		{
			((Frame)parent).childInUse = false;
		}
	}
	
	protected void valueChanged(Object val)
	{
		
	}

	public float getValue()
	{		
		return this.value;
	}
	
	public float getMin()
	{		
		return this.min;
	}
	
	public float getMax()
	{		
		return this.max;
	}
}
