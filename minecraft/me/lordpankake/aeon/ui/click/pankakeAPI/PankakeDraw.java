package me.lordpankake.aeon.ui.click.pankakeAPI;

import static org.lwjgl.opengl.GL11.*;
import java.awt.Color;
import org.lwjgl.opengl.GL11;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.ui.font.FontStyle;
import net.minecraft.client.Minecraft;

public class PankakeDraw
{
	private final static Color textBGColor = new Color(11, 11, 11, 111);

	public static void enableNoTextures()
	{
		//glPushMatrix();
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);
		glDisable(GL_ALPHA_TEST);
		glDisable(GL_TEXTURE_2D);
		 glShadeModel(GL_SMOOTH);
	}

	public static void disableNoTextures()
	{
		glDisable(GL_BLEND);
		glEnable(GL_CULL_FACE);
		glEnable(GL_ALPHA_TEST);
		glEnable(GL_TEXTURE_2D);
		//glPopMatrix();
	}

	public static void fillRoundedRect(float x, float y, float x2, float y2, int radius, Color color)
	{
		float newX = Math.abs(x + radius);
		float newY = Math.abs(y + radius);
		float newX1 = Math.abs(x2 - radius);
		float newY1 = Math.abs(y2 - radius);
		
		fillRect(newX, newY, newX1, newY1, color);
		fillRect(x, newY, newX, newY1, color);
		fillRect(newX1, newY, x2, newY1, color);
		fillRect(newX, y, newX1, newY, color);
		fillRect(newX, newY1, newX1, y2, color);
		
		
		fillArc(newX, newY, radius, 0, color);
		fillArc(newX, newY1, radius, 1, color);
		fillArc(newX1, newY1, radius, 2, color);
		fillArc(newX1, newY, radius, 3, color);
		
	}

	public static void drawRoundedRect(float x, float y, float x2, float y2, int radius, Color color)
	{
		float newX = Math.abs(x + radius);
		float newY = Math.abs(y + radius);
		float newX2 = Math.abs(x2 - radius);
		float newY2 = Math.abs(y2 - radius);
		
		drawLine(newX, y, newX2, y, color);
		drawLine(newX, y2, newX2, y2, color);
		drawLine(x, newY, x, newY2, color);
		drawLine(x2, newY, x2, newY2, color);
		
		drawArc(newX, newY, radius, 0, color);
		drawArc(newX, newY2, radius, 1, color);
		drawArc(newX2 - 0.5F, newY2 , radius, 2, color);
		drawArc(newX2 - 0.5F, newY, radius, 3, color);
	
	}
	
	public static void fillRect(float x, float y, float x2, float y2, Color color)
	{
		setColor(color);
		glBegin(GL_QUADS);
		{
			glVertex2d(x, y);
			glVertex2d(x2, y);
			glVertex2d(x2, y2);
			glVertex2d(x, y2);
		}
		glEnd();
	}
	
	public static void fillGradientRect(float x, float y, float x2, float y2, Color col1, Color col2)
	{
		
		glBegin(GL_QUADS);
		{
			setColor(col1);
			glVertex2d(x, y);
			glVertex2d(x2, y);
			setColor(col2);
			glVertex2d(x2, y2);
			glVertex2d(x, y2);
		}
		glEnd();
	}
	
	public static void setColor(Color c)
	{
		glColor4f(c.getRed() / 255f, c.getGreen() / 255f, c.getBlue() / 255f, c.getAlpha() / 255f);
	}

	public static void drawRect(float x, float y, float x2, float y2, Color color)
	{
		setColor(color);
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(x, y);
			glVertex2d(x2, y);
			glVertex2d(x2, y2);
			glVertex2d(x, y2);
		}
		glEnd();
	}

	public static void drawLine(float x, float y, float x2, float y2, Color color)
	{
		setColor(color);
		glBegin(GL_LINES);
		{
			glVertex2d(x, y);
			glVertex2d(x2,y2);
		}
		glEnd();
	}

	public static void drawArc(float x, float y, float radius, int mode, Color color)
	{
		setColor(color);
		int arcangle = 0;
		int startangle = 0;
		if (mode == 0)
		{
			startangle = 0;
			arcangle = 90;
		}
		if (mode == 1)
		{
			startangle = 90;
			arcangle = 180;
		}
		if (mode == 2)
		{
			startangle = 180;
			arcangle = 270;

		}
		if (mode == 3)
		{
			startangle = 270;
			arcangle = 360;
		}

		GL11.glBegin(GL11.GL_LINE_STRIP);
		for (int i = startangle; i <= arcangle; i++)
		{
			GL11.glVertex2d(x + (Math.sin((i * 3.141526D / 180)) * (radius * -1)), y + (Math.cos((i * 3.141526D / 180)) * (radius * -1)));
		}
		GL11.glEnd();
	}

	public static void fillArc(float x, float y, float radius, int mode, Color color)
	{
		setColor(color);
		int startangle = 0;
		int arcangle = 90;
		if (mode == 0)
		{
			startangle = 0;
			arcangle = 90;
		} else if (mode == 1)
		{
			startangle = 90;
			arcangle = 180;
		} else if (mode == 2)
		{
			startangle = 180;
			arcangle = 270;
		} else if (mode == 3)
		{
			startangle = 270;
			arcangle = 360;
		}
		GL11.glBegin(GL11.GL_POLYGON);
		GL11.glVertex2d(x, y);
		for (int i = startangle; i <= arcangle; i++)
		{
			GL11.glVertex2d(x + (Math.sin((i * 3.141526D / 180)) * (radius * -1)), y + (Math.cos((i * 3.141526D / 180)) * (radius * -1)));
		}
		GL11.glEnd();
	}

	public static void fillArc2(float x, float y, float radius, float mode, Color color)
	{
		setColor(color);
		GL11.glBegin(GL11.GL_POLYGON);
		GL11.glVertex2d(x, y);
		int startangle = 0;
		int arcangle = 90;
		if (mode == 0)
		{
			startangle = 0;
			arcangle = 90;
		} else if (mode == 1)
		{
			startangle = 90;
			arcangle = 180;
		} else if (mode == 2)
		{
			startangle = 180;
			arcangle = 270;
		} else if (mode == 3)
		{
			startangle = 270;
			arcangle = 360;
		}
		for (int i = startangle; i <= arcangle; i++)
		{
			GL11.glVertex2d(x + (Math.sin((i * 3.141526D / 180)) * (radius * -1)), y + (Math.cos((i * 3.141526D / 180)) * (radius * -1)));
		}
		GL11.glEnd();
	}

	public static void drawString(String text, float x, float y, int color, boolean box)
	{
		if (box)
		{
			x = x - 1;
			y = y - 1;
			enableNoTextures();
			setColor(textBGColor);
			glBegin(GL_QUADS);
			{
				glVertex2d(x, y);
				glVertex2d(x + Minecraft.getMinecraft().fontRenderer.getGuiStringWidth(text) + 3, y);
				glVertex2d(x + Minecraft.getMinecraft().fontRenderer.getGuiStringWidth(text) + 3, y + Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT + 2);
				glVertex2d(x, y + Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT + 2);
			}
			glEnd();
			disableNoTextures();
			x = x + 1;
			y = y + 1;
		}
		Aeon.getInstance().getUtils().kek.theme.guiFont.drawString(text, x, y, FontStyle.NORMAL, color, 0xFF000000, true);
	}
	
	public static void drawLargeString(String text, float x, float y, int color)
	{
		Aeon.getInstance().getUtils().kek.theme.guiLargeFont.drawString(text, x, y, FontStyle.NORMAL, color, 0xFF000000, true);
	}

	public static void drawSmallString(String text, float x, float y, int color)
	{
		Aeon.getInstance().getUtils().kek.theme.guiSmallFont.drawString(text, x, y, FontStyle.NORMAL, color, 0xFF000000, true);
	}
}
