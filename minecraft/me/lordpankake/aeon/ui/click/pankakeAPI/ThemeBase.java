package me.lordpankake.aeon.ui.click.pankakeAPI;

import static org.lwjgl.opengl.GL11.*;
import java.awt.Font;
import java.awt.Rectangle;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ModuleButton;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ThemeButton;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Button;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Component;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Frame;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Slider;
import me.lordpankake.aeon.ui.font.CustomFontRenderer;

public class ThemeBase
{
	public CustomFontRenderer guiFont;// = new CustomFontRenderer("Segoe UI",
										// 20, 0, true, false);
	public CustomFontRenderer guiSmallFont;// = new
											// CustomFontRenderer("Segoe UI",
											// 15, 0, true, false);
	public CustomFontRenderer guiLargeFont; // = new
											// CustomFontRenderer("Arial Bold",
											// 25, 0, true, false);
	public int buttonHeight;
	public int sliderHeight;
	public int frameGripHeight;
	public int frameWidth;
	public Rectangle toggleRect;

	public ThemeBase()
	{
		frameGripHeight = 14;
		sliderHeight = 14;
		buttonHeight = 13;
		frameWidth = 72;
		toggleRect = new Rectangle(0, 0, frameGripHeight, frameGripHeight);
	}

	protected void setFonts(Font normal, Font big, Font small)
	{
		this.guiFont = new CustomFontRenderer(normal.getName(), normal.getSize(), 0, true, false);
		this.guiLargeFont = new CustomFontRenderer(big.getName(), big.getSize(), 0, true, false);
		this.guiSmallFont = new CustomFontRenderer(small.getName(), small.getSize(), 0, true, false);
	}

	public void onRender(Frame f)
	{
		glEnable(GL_BLEND);
		onFrameRender(f);
		if (f.getOpened())
		{
			for (Component c : f.components)
			{
				if (c instanceof ModuleButton)
				{
					c.render();
					onButtonRender((Button) c);
				} else if (c instanceof ThemeButton)
				{
					onButtonRender((Button) c);
				} else if (c instanceof Slider)
				{
					onSliderRender((Slider) c);
				}
			}
		}
		glDisable(GL_BLEND);
	}

	public void onFrameRender(Frame f)
	{
	}

	protected void onButtonRender(Button b)
	{
	}

	protected void onSliderRender(Slider s)
	{
	}
}
