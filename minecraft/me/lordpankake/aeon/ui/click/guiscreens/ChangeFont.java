package me.lordpankake.aeon.ui.click.guiscreens;

import java.awt.Font;
import me.lordpankake.aeon.core.AeonSettings;
import me.lordpankake.aeon.ui.font.FontManager;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;

public class ChangeFont extends GuiScreen
{
	public GuiTextField fontBox;
	public GuiTextField sizeBox;
	public GuiScreen backScreen;

	public ChangeFont(GuiScreen parent)
	{
		this.backScreen = parent;
	}

	public FontRenderer getLocalFontRenderer()
	{
		return this.fontRendererObj;
	}

	@Override
	public void initGui()
	{
		this.buttonList.clear();
		this.fontBox = new GuiTextField(fontRendererObj, width / 2 - 99, height / 2, 168, 20);
		this.fontBox.setMaxStringLength(50);
		this.fontBox.setText(AeonSettings.font.getFontName());
		this.sizeBox = new GuiTextField(fontRendererObj, this.fontBox.getX() + this.fontBox.getWidth() + 4, height / 2, 25, 20);
		this.sizeBox.setMaxStringLength(2);
		this.sizeBox.setText(AeonSettings.fontSize + "");
		this.buttonList.add(new GuiButton(0, fontBox.getX() - 42, height / 2, 40, 20, "Back"));
		this.buttonList.add(new GuiButton(1, sizeBox.getX() + sizeBox.getHeight() + 8, height / 2, 40, 20, "Set"));
		this.buttonList.add(new GuiButton(2, this.width / 2 - 100,  height / 2 + 22, "Toggle Global TTF"));
	}

	@Override
	public void actionPerformed(GuiButton button)
	{
		super.actionPerformed(button);
		if (button.buttonID == 0)
		{
			mc.displayGuiScreen(backScreen);
		}
		int fontSize = (int) AeonSettings.fontSize;
		if (button.buttonID == 1)
		{
			
			if (this.sizeBox.getText().length() > 0)
			{
				fontSize = Integer.parseInt(this.sizeBox.getText());
				AeonSettings.fontSize = fontSize;
			}
			if (this.fontBox.getText().length() > 0)
			{
				mc.fontRenderer.setFont(new Font(this.fontBox.getText(), Font.PLAIN, fontSize));
			}
		}
		if (button.buttonID == 2)
		{
			FontManager.instance().setGlobalFont(!FontManager.instance().isGlobalFont());
		}
	}

	@Override
	public void updateScreen()
	{
		this.fontBox.updateCursorCounter();
		this.sizeBox.updateCursorCounter();
		super.updateScreen();
	}

	@Override
	protected void keyTyped(char c, int i)
	{
		this.fontBox.textboxKeyTyped(c, i);
		this.sizeBox.textboxKeyTyped(c, i);
		if (c == '\r')
		{
			actionPerformed((GuiButton) buttonList.get(1));
		}
	}

	@Override
	public void mouseClicked(int x, int y, int b)
	{
		this.fontBox.mouseClicked(x, y, b);
		this.sizeBox.mouseClicked(x, y, b);
		super.mouseClicked(x, y, b);
	}

	@Override
	public void drawScreen(int i, int j, float f)
	{
		this.drawDefaultBackground();
		this.drawString(this.fontRendererObj, "Type a font and click bind.", this.width / 2 - 65, height / 2 - 20, 0xffffff);
		try
		{
			this.sizeBox.setText(this.sizeBox.filterText(this.sizeBox.getText(), "1234567890"));
			this.fontBox.drawTextBox();
			this.sizeBox.drawTextBox();
		} catch (final Exception err)
		{
			err.printStackTrace();
		}
		super.drawScreen(i, j, f);
	}
}