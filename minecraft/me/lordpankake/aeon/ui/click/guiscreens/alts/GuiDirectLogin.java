package me.lordpankake.aeon.ui.click.guiscreens.alts;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.Session;
import org.lwjgl.input.Keyboard;

public class GuiDirectLogin extends GuiScreen
{
	public GuiScreen parent;
	public GuiTextField usernameBox;
	public PasswordField passwordBox;

	public GuiDirectLogin(GuiScreen paramScreen)
	{
		this.parent = paramScreen;
	}

	@Override
	public void initGui()
	{
		Keyboard.enableRepeatEvents(true);
		buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 96 + 12, "Login"));
		buttonList.add(new GuiButton(2, width / 2 - 100, height / 4 + 96 + 36, "Back"));
		usernameBox = new GuiTextField(fontRendererObj, width / 2 - 100, 76 - 25, 200, 20);
		passwordBox = new PasswordField(fontRendererObj, width / 2 - 100, 116 - 25, 200, 20);
		usernameBox.setMaxStringLength(200);
		passwordBox.setMaxStringLength(120);
	}

	@Override
	public void onGuiClosed()
	{
		Keyboard.enableRepeatEvents(false);
	}

	@Override
	public void updateScreen()
	{
		usernameBox.updateCursorCounter();
		passwordBox.updateCursorCounter();
	}

	@Override
	public void mouseClicked(int x, int y, int b)
	{
		usernameBox.mouseClicked(x, y, b);
		passwordBox.mouseClicked(x, y, b);
		super.mouseClicked(x, y, b);
	}

	@Override
	public void actionPerformed(GuiButton button)
	{
		if (button.buttonID == 1)
		{
			if (!usernameBox.getText().trim().isEmpty() && !passwordBox.getText().trim().isEmpty())
			{
				try
				{
					mc.session = Session.loginPassword(usernameBox.getText().trim(), passwordBox.getText().trim());
					Manager.altScreen.dispErrorString = "";
				} catch (final Exception error)
				{
					Manager.altScreen.dispErrorString = "".concat("\247cBad Login \2477(").concat(usernameBox.getText()).concat(")");
				}
			} else if (!usernameBox.getText().trim().isEmpty())
			{
				mc.session = new Session(usernameBox.getText().trim(), "-", "-", "Legacy");
				Manager.altScreen.dispErrorString = "";
			}
			mc.displayGuiScreen(parent);
		} else if (button.buttonID == 2)
		{
			mc.displayGuiScreen(parent);
		}
	}

	@Override
	protected void keyTyped(char c, int i)
	{
		usernameBox.textboxKeyTyped(c, i);
		passwordBox.textboxKeyTyped(c, i);
		if (c == '\t')
		{
			if (usernameBox.isFocused())
			{
				usernameBox.setFocused(false);
				passwordBox.setFocused(true);
			} else if (passwordBox.isFocused())
			{
				usernameBox.setFocused(false);
				passwordBox.setFocused(false);
			}
		}
		if (c == '\r')
		{
			actionPerformed((GuiButton) buttonList.get(0));
		}
	}

	@Override
	public void drawScreen(int x, int y, float f)
	{
		drawDefaultBackground();
		drawString(fontRendererObj, "Username", width / 2 - 100, 63 - 25, 0xA0A0A0);
		// drawString(fontrenderer, "\2474*", width / 2 - 106, 63 - 25,
		// 0xA0A0A0);
		drawString(fontRendererObj, "Password", width / 2 - 100, 104 - 25, 0xA0A0A0);
		try
		{
			usernameBox.drawTextBox();
			passwordBox.drawTextBox();
		} catch (final Exception err)
		{
			err.printStackTrace();
		}
		super.drawScreen(x, y, f);
	}
}
