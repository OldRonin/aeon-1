package me.lordpankake.aeon.ui.click.guiscreens.alts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import javax.net.ssl.HttpsURLConnection;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.hook.MainMenuHook;

public class Manager
{
	public static ArrayList<Alt> altList = new ArrayList<Alt>();
	public static GuiAltList altScreen = new GuiAltList(MainMenuHook.mainMenu);
	public static final int slotHeight = 25;

	public static void addAlt(Alt paramAlt)
	{
		altList.add(paramAlt);
	}

	public static void addAlt(String username)
	{
		altList.add(new Alt(username));
	}

	public static void addAlt(String username, String password)
	{
		altList.add(new Alt(username, password));
	}

	public static String makePassChar(String regex)
	{
		return regex.replaceAll("(?s).", "*");
	}

	public static ArrayList<Alt> getList()
	{
		return altList;
	}

	public static String excutePost(String s, String s1)
	{
		HttpsURLConnection httpsurlconnection = null;
		try
		{
			try
			{
				final URL url = new URL(s);
				httpsurlconnection = (HttpsURLConnection) url.openConnection();
				httpsurlconnection.setRequestMethod("POST");
				httpsurlconnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				httpsurlconnection.setRequestProperty("Content-Type", Integer.toString(s1.getBytes().length));
				httpsurlconnection.setRequestProperty("Content-Language", "en-US");
				httpsurlconnection.setUseCaches(false);
				httpsurlconnection.setDoInput(true);
				httpsurlconnection.setDoOutput(true);
				httpsurlconnection.connect();
				final DataOutputStream dataoutputstream = new DataOutputStream(httpsurlconnection.getOutputStream());
				dataoutputstream.writeBytes(s1);
				dataoutputstream.flush();
				dataoutputstream.close();
				final InputStream inputstream = httpsurlconnection.getInputStream();
				final BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputstream));
				final StringBuffer stringbuffer = new StringBuffer();
				String s2;
				while ((s2 = bufferedreader.readLine()) != null)
				{
					stringbuffer.append(s2);
					stringbuffer.append('\r');
				}
				bufferedreader.close();
				final String s3 = stringbuffer.toString();
				final String s4 = s3;
				return s3;
			} catch (final Exception exception)
			{
				exception.printStackTrace();
			}
			return null;
		} finally
		{
			if (httpsurlconnection != null)
			{
				httpsurlconnection.disconnect();
			}
		}
	}

	public static void saveAlts()
	{
		try
		{
			Log.write("Saving alts...");
			final File file = new File("Aeon" + File.separator + "Alts");
			if (!file.exists())
			{
				file.mkdirs();
			}
			final FileWriter fwriter = new FileWriter(file + File.separator + "alts.txt");
			final BufferedWriter writer = new BufferedWriter(fwriter);
			for (final Alt alt : altList)
			{
				writer.write(alt.getFileLine());
				writer.newLine();
			}
			writer.close();
		} catch (final Exception error)
		{
			Log.write("Could not save alts.");
			error.printStackTrace();
		}
	}

	public static void loadAlts()
	{
		try
		{
			Log.write("Loading alts...");
			final File file = new File("Aeon" + File.separator + "Alts");
			if (file.exists())
			{
				altList.clear();
				Log.write("Alt folder found!");
				final BufferedReader bufferedReader = new BufferedReader(new FileReader(file + File.separator + "alts.txt"));
				String rline = "";
				while ((rline = bufferedReader.readLine()) != null)
				{
					final String curLine = rline;
					try
					{
						if (curLine.contains(":") && !curLine.trim().endsWith(":"))
						{
							final String[] altInfo = curLine.split(":");
							final Alt theAlt = new Alt(altInfo[0], altInfo[1]);
							if (!altList.contains(theAlt))
							{
								altList.add(theAlt);
							}
						} else if (!curLine.isEmpty() && curLine != null && !curLine.trim().isEmpty())
						{
							final Alt theAlt = new Alt(curLine.replace(":", "").trim());
							if (!altList.contains(theAlt))
							{
								altList.add(theAlt);
							}
						}
					} catch (final Exception error)
					{
						error.printStackTrace();
					}
				}
				bufferedReader.close();
			} else
			{
				file.mkdirs();
			}
		} catch (final Exception error)
		{
			error.printStackTrace();
		}
	}
}
