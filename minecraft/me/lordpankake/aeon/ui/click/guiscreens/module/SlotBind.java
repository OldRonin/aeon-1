package me.lordpankake.aeon.ui.click.guiscreens.module;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiSlot;
import net.minecraft.client.renderer.Tessellator;

public class SlotBind extends GuiSlot
{
	private final ManageModules chngBinds;
	private Module mod;
	private static int slotHeight = 45;
	private int selected;

	public SlotBind(Minecraft theMinecraft, ManageModules aList)
	{
		super(theMinecraft, aList.width, aList.height, 10, aList.height - 25, slotHeight);
		this.chngBinds = aList;
		this.selected = 0;
	}

	@Override
	protected int getContentHeight()
	{
		return this.getSize() * slotHeight;
	}

	@Override
	protected int getSize()
	{
		return Aeon.getInstance().getUtils().getHack().getModules().size();
	}

	@Override
	protected void elementClicked(int var1, boolean var2, int idunno, int idunnoeither)
	{
		this.selected = var1;
	}

	@Override
	protected boolean isSelected(int var1)
	{
		return this.selected == var1;
	}

	protected int getSelected()
	{
		return this.selected;
	}

	@Override
	protected void drawBackground()
	{
		chngBinds.drawDefaultBackground();
	}

	@Override
	protected void drawSlot(int slotIndex, int x, int y, int var4, Tessellator var5, int var6, int var7)
	{
		try
		{
			final Module m = Aeon.getInstance().getUtils().getHack().getModules().get(slotIndex);
			this.chngBinds.drawString(chngBinds.fontRendererObj, m.getName(), x, y, 0xFFFFFF);
			this.chngBinds.drawString(chngBinds.fontRendererObj, "Current Keybind: " + Aeon.getInstance().getUtils().getKey().getKeyFromInteger(m.getKey()), x, y + 11, 0x808080);
			this.chngBinds.drawString(chngBinds.fontRendererObj, "Module Page: " + m.getType(), x, y + 21, 0x808080);
			this.chngBinds.drawString(chngBinds.fontRendererObj, "UI visibility: " + m.shouldDrawToUI(), x, y + 31, 0x808080);
		} catch (final Exception e)
		{
			e.printStackTrace();
		}
	}
}
