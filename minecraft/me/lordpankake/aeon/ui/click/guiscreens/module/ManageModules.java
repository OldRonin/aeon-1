package me.lordpankake.aeon.ui.click.guiscreens.module;

import org.lwjgl.input.Keyboard;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.util.ScreenUtils;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

public class ManageModules extends GuiScreen
{
	public GuiTextField keybindBox;
	public GuiScreen backScreen;
	private SlotBind tSlot;

	public ManageModules(GuiScreen parent)
	{
		this.backScreen = parent;
	}

	public FontRenderer getLocalFontRenderer()
	{
		return this.fontRendererObj;
	}

	@Override
	public void initGui()
	{
		this.buttonList.clear();
		this.buttonList.add(new GuiButton(0, this.width / 2 - 131, height - 23, 40, 20, "Back"));
		this.buttonList.add(new GuiButton(1, this.width / 2 - 90, height - 23, 40, 20, "Bind"));
		this.buttonList.add(new GuiButton(2, this.width / 2 - 24, height - 23, 60, 20, "Reset Bind"));
		this.buttonList.add(new GuiButton(3, this.width / 2 + 37, height - 23, 96, 20, "Toggle UI visability"));
		this.keybindBox = new GuiTextField(fontRendererObj, width / 2 - 48, height - 23, 22, 20);
		this.keybindBox.setMaxStringLength(1);
		this.tSlot = new SlotBind(mc, this);
		this.tSlot.registerScrollButtons(7, 8);
	}

	@Override
	public void actionPerformed(GuiButton button)
	{
		super.actionPerformed(button);
		if (button.buttonID == 0)
		{
			mc.displayGuiScreen(backScreen);
		}
		if (button.buttonID == 1)
		{
			if (this.keybindBox.getText().length() > 0)
			{
				Aeon.getInstance().getUtils().getHack().getModules().get(this.tSlot.getSelected()).setKey(Keyboard.getKeyIndex(this.keybindBox.getText().toUpperCase()));
				Aeon.getInstance().getUtils().getKey().writeSettings();
				Aeon.getInstance().getUtils().getKey().loadSettings();
			}
		}
		if (button.buttonID == 2)
		{
			Aeon.getInstance().getUtils().getHack().getModules().get(this.tSlot.getSelected()).setKey(Keyboard.CHAR_NONE);
			Aeon.getInstance().getUtils().getKey().writeSettings();
			Aeon.getInstance().getUtils().getKey().loadSettings();
		}
		if (button.buttonID == 3)
		{
			final Module temp = Aeon.getInstance().getUtils().getHack().getModules().get(this.tSlot.getSelected());
			temp.setDrawToUI(!temp.shouldDrawToUI());
			Aeon.getInstance().getUtils().getHack().replaceModule(temp);
			Aeon.getInstance().getUtils().getuScreen();
			Aeon.getInstance().getUtils().kek.rebuildGUI(Aeon.getInstance().getUtils().kek.theme);
			Aeon.getInstance().getUtils().getKey().writeSettings();
			Aeon.getInstance().getUtils().getKey().loadSettings();
		}
	}

	@Override
	public void updateScreen()
	{
		this.keybindBox.updateCursorCounter();
		super.updateScreen();
	}

	@Override
	protected void keyTyped(char c, int i)
	{
		this.keybindBox.textboxKeyTyped(c, i);
		if (c == '\r')
		{
			actionPerformed((GuiButton) buttonList.get(1));
		}
	}

	@Override
	public void mouseClicked(int x, int y, int b)
	{
		this.keybindBox.mouseClicked(x, y, b);
		super.mouseClicked(x, y, b);
	}

	@Override
	public void drawScreen(int i, int j, float f)
	{
		tSlot.drawScreen(i, j, f);
		try
		{
			this.keybindBox.drawTextBox();
		} catch (final Exception err)
		{
			err.printStackTrace();
		}
		super.drawScreen(i, j, f);
	}
}