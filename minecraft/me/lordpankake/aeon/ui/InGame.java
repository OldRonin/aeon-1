package me.lordpankake.aeon.ui;

import java.io.File;
import java.util.ArrayList;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.ui.click.guiscreens.GuiCreator;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeGUI;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Frame;
import me.lordpankake.aeon.util.BlockItemUtils;
import me.lordpankake.aeon.util.FileUtils;
import me.lordpankake.aeon.util.ScreenUtils;
import me.lordpankake.aeon.util.StringUtils;
import org.lwjgl.input.Keyboard;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.ResourceLocation;

public class InGame extends GuiScreen
{
	public Minecraft mc = Minecraft.getMinecraft();
	private static final ResourceLocation pagePlayer = new ResourceLocation("Aeon/Pages/Player.png");
	private static final ResourceLocation pageRender = new ResourceLocation("Aeon/Pages/Render.png");
	private static final ResourceLocation pageCombat = new ResourceLocation("Aeon/Pages/Combat.png");
	private static final ResourceLocation pageWorld = new ResourceLocation("Aeon/Pages/World.png");
	public static boolean first = true;
	public static int page = 1;
	public static int left;
	public static int right;
	public static int toggle;
	public static int openUI;
	public static boolean doRender = true;
	public static boolean drawPage = true;
	public static boolean monitorState = false;
	public static final Object monitor = new Object();

	public InGame()
	{
		Log.write("Initial keybinds for 'Screen' loaded.");
		left = Keyboard.KEY_LEFT;
		right = Keyboard.KEY_RIGHT;
		toggle = Keyboard.KEY_UP;
		openUI = Keyboard.KEY_RSHIFT;
		writeSettings("Initial");
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				load();
			}

			private void load()
			{
				monitorState = true;
				while (monitorState)
				{
					synchronized (monitor)
					{
						try
						{
							monitor.wait();
						} catch (final Exception e)
						{
						}
					}
				}
				loadSettings();
			}
		}).start();
	}

	public static void unlockMonitor()
	{
		synchronized (monitor)
		{
			monitorState = false;
			monitor.notifyAll();
		}
	}

	public void draw()
	{
		handleGUI();
		if (!Aeon.getInstance().getUtils().getHack().getLoader().legit.getState())
		{
			if (mc.currentScreen != null)
			{
				if (!(mc.currentScreen instanceof GuiChat) && !(mc.currentScreen instanceof PankakeGUI))
				{
					return;
				}
			}
			final FontRenderer fr = mc.fontRenderer;
			drawWelcomeMessage(fr);
			drawPages(fr);
			drawArraylist(fr);
			// drawTestingPurposes(fr);
		}
	}

	private void drawTestingPurposes(FontRenderer fr)
	{
		if (mc.objectMouseOver != null)
		{
			if (mc.objectMouseOver.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK)
			{
				final int x = mc.objectMouseOver.blockX;
				final int y = mc.objectMouseOver.blockY;
				final int z = mc.objectMouseOver.blockZ;
				final BlockItemUtils utils = Aeon.getInstance().getUtils().getBlock();
				fr.drawString("Block:" + mc.theWorld.getBlock(x, y, z).textureName + "|" + utils.getBlockID(mc.theWorld.getBlock(x, y, z)), 2, 35, 0xffffff);
			}
		}
	}

	private void drawWelcomeMessage(FontRenderer fr)
	{
		if (first)
		{
			fr.drawStringWithShadow("Click " + Aeon.getInstance().getUtils().getKey().getKeyFromInteger(openUI) + " to open the GUI", 1, 11, 0xffffff);
			fr.drawStringWithShadow("Chat '-help' for help", 1, 21, 0xffffff);
		}
	}

	private void drawPages(FontRenderer fr)
	{
		if (drawPage)
		{
			final Tessellator var4 = Tessellator.instance;
			switch (page)
			{
			case 1:
				fr.drawStringWithShadow("[Player]", 1, 1, 0xffffff);
				break;
			case 2:
				fr.drawStringWithShadow("[Combat]", 1, 1, 0xffffff);
				break;
			case 3:
				fr.drawStringWithShadow("[Render]", 1, 1, 0xffffff);
				break;
			case 4:
				fr.drawStringWithShadow("[World]", 1, 1, 0xffffff);
				break;
			case 5:
				fr.drawStringWithShadow("[Misc]", 1, 1, 0xffffff);
				break;
			}
		}
	}

	private void drawArraylist(FontRenderer fr)
	{
	}

	public void writeSettings(String instance)
	{
		final File saveFile = new File("Aeon" + File.separator + "Keybindings" + File.separator + "screen.txt");
		final ArrayList<String> content = new ArrayList<String>();
		content.add("Left:" + left);
		content.add("Right:" + right);
		content.add("Click Toggle:" + openUI);
		content.add("CurrentPage:" + page);
		FileUtils.write(saveFile, content, true);
	}

	public void loadSettings()
	{
		final File settings = new File("Aeon" + File.separator + "Keybindings" + File.separator + "screen.txt");
		Aeon.getInstance().getUtils().getFile();
		final ArrayList<String> content = (ArrayList<String>) FileUtils.read(settings);
		for (final String s : content)
		{
			if (s.startsWith("Left"))
			{
				final String info[] = s.split(":");
				if (StringUtils.isInteger(info[1]))
				{
					left = Integer.parseInt(info[1]);
				}
			}
			if (s.startsWith("Right"))
			{
				final String info[] = s.split(":");
				if (StringUtils.isInteger(info[1]))
				{
					right = Integer.parseInt(info[1]);
				}
			}
			if (s.startsWith("Click Toggle"))
			{
				final String info[] = s.split(":");
				if (StringUtils.isInteger(info[1]))
				{
					openUI = Integer.parseInt(info[1]);
				}
			}
			if (s.startsWith("Toggle"))
			{
				final String info[] = s.split(":");
				if (StringUtils.isInteger(info[1]))
				{
					toggle = Integer.parseInt(info[1]);
				}
			}
			if (s.startsWith("CurrentPage"))
			{
				final String info[] = s.split(":");
				if (StringUtils.isInteger(info[1]))
				{
					page = Integer.parseInt(info[1]);
				}
			}
		}
	}

	public static String getEnum()
	{
		String type = "Player";
		switch (page)
		{
		case 0:
			type = "World";
		case 1:
			type = "Player";
			break;
		case 2:
			type = "Combat";
			break;
		case 3:
			type = "Render";
			break;
		case 4:
			type = "World";
		case 5:
			type = "World";
			break;
		}
		return type;
	}

	private void handleGUI()
	{
		if ((Minecraft.getMinecraft().currentScreen != null || mc.currentScreen instanceof GuiCreator) && Aeon.getInstance().getUtils().kek != null)
		{
			if (Minecraft.getMinecraft().currentScreen.equals(Aeon.getInstance().getUtils().kek))
			{
				for (final Frame f : PankakeGUI.frames)
				{
					f.update();
				}
			}
		}
	}
}
