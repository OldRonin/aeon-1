package me.lordpankake.aeon.core;

import me.lordpankake.aeon.core.handling.Keys;
import me.lordpankake.aeon.core.handling.module.Handler;
import me.lordpankake.aeon.modules.eventapi.EventManager;
import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.ui.InGame;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeGUI;
import me.lordpankake.aeon.util.BlockItemUtils;
import me.lordpankake.aeon.util.EntityUtils;
import me.lordpankake.aeon.util.FileUtils;
import me.lordpankake.aeon.util.FriendUtils;
import me.lordpankake.aeon.util.InventoryUtils;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.ReflectionUtils;
import me.lordpankake.aeon.util.RenderUtils;
import me.lordpankake.aeon.util.ScreenUtils;
import me.lordpankake.aeon.util.StringUtils;
import me.lordpankake.aeon.util.TimerUtils;

public class ClientUtils
{
	public Handler hackhandler;
	public Keys keyhandler;
	public InGame screen;
	public PankakeGUI kek;
	private final BlockItemUtils block;
	private final EntityUtils entity;
	private final FileUtils files;
	private final FriendUtils fucknugget;
	private final InventoryUtils inv;
	private final PlayerUtils player;
	private final ReflectionUtils reflect;
	private final RenderUtils render;
	private final ScreenUtils uScreen;
	private final StringUtils string;
	private final TimerUtils timer;

	public ClientUtils()
	{
		Log.write("Attempting to load Handlers & Utils");
		screen = new InGame();
		block = new BlockItemUtils();
		entity = new EntityUtils();
		files = new FileUtils();
		fucknugget = new FriendUtils();
		inv = new InventoryUtils();
		player = new PlayerUtils();
		reflect = new ReflectionUtils();
		render = new RenderUtils();
		uScreen = new ScreenUtils();
		string = new StringUtils();
		timer = new TimerUtils();
		hackhandler = new Handler();
		keyhandler = new Keys();
		delayedInitilization();
		Log.write("Handlers & Utils Loaded");
	}

	// handlers
	public Keys getKey()
	{
		return keyhandler;
	}

	public Handler getHack()
	{
		return hackhandler;
	}

	public InGame getScreen()
	{
		return screen;
	}

	public BlockItemUtils getBlock()
	{
		return block;
	}

	public EntityUtils getEntity()
	{
		return entity;
	}

	public FileUtils getFile()
	{
		return files;
	}

	public FriendUtils getFriend()
	{
		return fucknugget;
	}

	public InventoryUtils getInventory()
	{
		return inv;
	}

	public PlayerUtils getPlayer()
	{
		return player;
	}

	public ReflectionUtils getReflect()
	{
		return reflect;
	}

	public RenderUtils getRender()
	{
		return render;
	}

	public ScreenUtils getuScreen()
	{
		return uScreen;
	}

	public StringUtils getString()
	{
		return string;
	}

	public TimerUtils getTimer()
	{
		return timer;
	}

	public void tick()
	{
		if (InGame.doRender)
		{
			getScreen().draw();
		}
		EventManager.eventTick.call();
	}

	public void reload()
	{
		getKey().loadSettings();
		getHack().getLoader().blockoverlay.loadSettings();
		getHack().getLoader().search.loadSettings();
		if (!getHack().getLoader().clickfriends.getState())
		{
			getHack().getLoader().clickfriends.toggle();
		}
		if (!getHack().getLoader().arraylist.getState())
		{
			getHack().getLoader().arraylist.toggle();
		}
		if (!getHack().getLoader().commands.getState())
		{
			getHack().getLoader().commands.toggle();
		}
		getScreen().loadSettings();
		getFriend().loadFriends();
	}

	public void onClientStart()
	{
		reload();
		Command.unlockMonitor();
		InGame.unlockMonitor();
		unlockMonitor();
		kek.addThemes();
		kek.rebuildGUI(kek.theme);
	}
	private static final Object monitor = new Object();
	private static boolean monitorState = false;

	private static void unlockMonitor()
	{
		synchronized (monitor)
		{
			monitorState = false;
			monitor.notifyAll();
		}
	}

	private void delayedInitilization()
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				monitorState = true;
				while (monitorState)
				{
					synchronized (monitor)
					{
						try
						{
							monitor.wait();
						} catch (final Exception e)
						{
						}
					}
				}
				kek = new PankakeGUI();
			}
		}).start();
	}
}
