package me.lordpankake.aeon.core;

import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import me.lordpankake.aeon.hook.FontRendererHook;
import me.lordpankake.aeon.hook.RenderGlobalHook;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class Aeon
{
	//export with libraries in subfolder. Run AeonClient obfuscation, done.
	
	// DO NOT EVER PUT THE FOLLOWING IN TEH MINECRAFT.JAR
	// bgq.class
	// bgp.class
	// bgw.class
	// bgt.class
	// ky.class
	// kw.class
	// lj.class
	// la.class
	// anythin in the /net/ folder
	// delete /Meta INF/Mojang.RSA
	// ONLY THE RSA
	private static Aeon aeon;
	private final ClientUtils util;

	public Aeon()
	{
		Log.write("Starting Client...");
		aeon = this;
		util = new ClientUtils();
		final File settings = new File("Aeon" + File.separator + "Hacks");
		final FileReader fstream;
		if (!settings.exists())
		{
			settings.mkdirs();
		}
	}

	public static final Aeon getInstance()
	{
		return aeon;
	}

	public final ClientUtils getUtils()
	{
		return util;
	}
}
