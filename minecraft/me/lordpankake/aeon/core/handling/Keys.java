package me.lordpankake.aeon.core.handling;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.ui.InGame;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeGUI;
import org.lwjgl.input.Keyboard;
import net.minecraft.client.Minecraft;

public class Keys
{
	public Minecraft mc = Minecraft.getMinecraft();

	public void onKeyPressed()
	{
		final int key = Keyboard.getEventKey();
		handlePages(key);
		if (key == InGame.openUI)
		{
			if (InGame.first)
			{
				Aeon.getInstance().getUtils().kek.rebuildGUI(Aeon.getInstance().getUtils().kek.theme);
			}
			InGame.first = false;
			mc.displayGuiScreen(Aeon.getInstance().getUtils().kek);
		}
		for (final Module t : Aeon.getInstance().getUtils().getHack().getModules())
		{
			if (t.getType().toString() == InGame.getEnum() && t.getKey() == key && key != Keyboard.CHAR_NONE)
			{
				t.toggle();
				t.buttons(t.getState());
			}
		}
	}

	private void handlePages(int key)
	{
		if (key == InGame.left)
		{
			InGame.page = InGame.page - 1;
			if (InGame.page == 0)
			{
				InGame.page = 4;
			}
		}
		if (key == InGame.right)
		{
			InGame.page = InGame.page + 1;
			if (InGame.page == 5)
			{
				InGame.page = 1;
			}
		}
	}

	public int getKeyFromString(String key)
	{
		final String input = key.toUpperCase();
		int keyToBe = 9001;
		keyToBe = Keyboard.getKeyIndex(input);
		if (keyToBe != 9001)
		{
			return keyToBe;
		} else
		{
			return Keyboard.CHAR_NONE;
		}
	}

	public String getKeyFromInteger(int key)
	{
		return Keyboard.getKeyName(key);
	}

	public void writeSettings()
	{
		final File settings = new File("Aeon" + File.separator + "Keybindings");
		FileWriter fwriter;
		if (!settings.exists())
		{
			settings.mkdirs();
		}
		if (settings.exists())
		{
			try
			{
				fwriter = new FileWriter(settings.toString() + File.separator + "settings.txt");
				final BufferedWriter write = new BufferedWriter(fwriter);
				for (final Module t : Aeon.getInstance().getUtils().getHack().getModules())
				{
					if (!t.getName().contains("Aeon"))
					{
						write.write(t.getName() + ":" + this.getKeyFromInteger(t.getKey()) + ":" + t.shouldDrawToUI());
						write.newLine();
					}
				}
				write.close();
				Log.write("Keybinds written sucsessfully.");
			} catch (final Exception e)
			{
				e.printStackTrace();
				Log.write("Keybinds writing crashed!");
			}
		}
	}

	public void loadSettings()
	{
		Log.write("Searching for Keybinds...");
		final File settings = new File("Aeon" + File.separator + "Keybindings");
		final File settingsFile = new File("Aeon" + File.separator + "Keybindings" + File.separator + "settings.txt");
		FileReader fstream;
		if (settingsFile.exists())
		{
			Log.write("Keybinds found!");
			Log.write("Attempting to load Keybinds");
			try
			{
				fstream = new FileReader(settings.toString() + File.separator + "settings.txt");
				final BufferedReader in = new BufferedReader(fstream);
				String s;
				while ((s = in.readLine()) != null)
				{
					final String[] info = s.split(":");
					for (final Module t : Aeon.getInstance().getUtils().getHack().getModules())
					{
						if (s.contains(t.getName()))
						{
							// Log.write("(s.contains(t.getName()))" + " | " +
							// info[0] + ":" + t.getName());
							if (info[0].contains(t.getName()))
							{
								// Log.write(t.getName() + " key set to " + "["
								// + this.getKey().getKeyFromString(info[1]) +
								// "]");
								t.setKey(this.getKeyFromString(info[1]));
								t.setDrawToUI(Boolean.parseBoolean(info[2]));
							}
						}
					}
				}
				in.close();
				Log.write("Keybinds loaded!");
			} catch (final Exception e)
			{
				e.printStackTrace();
				Log.write("Error on loading keybinds");
			}
		} else
		{
			settings.mkdirs();
			Log.write("Keybind file missing!");
			writeSettings();
		}
	}
}