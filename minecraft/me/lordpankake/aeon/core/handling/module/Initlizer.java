package me.lordpankake.aeon.core.handling.module;

import java.io.File;
import java.util.ArrayList;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.mod.chat.CommandListener;
import me.lordpankake.aeon.modules.mod.combat.*;
import me.lordpankake.aeon.modules.mod.misc.*;
import me.lordpankake.aeon.modules.mod.player.*;
import me.lordpankake.aeon.modules.mod.render.*;
import me.lordpankake.aeon.modules.mod.render.overlay.*;
import me.lordpankake.aeon.modules.mod.world.*;
import me.lordpankake.aeon.modules.mod.world.radar.Radar;
import org.lwjgl.input.Keyboard;

public class Initlizer
{
	public ArrayList hackList = new ArrayList<Module>();
	public ArrayList hackRegisteredList = new ArrayList<Module>();
	// ------------------------------------------------------------//
	public Armor armor = new Armor("Armor", Keyboard.KEY_NONE, ModuleType.Render, "Shows your worn armor.");
	public AntiFire antifire = new AntiFire("AntiFire", Keyboard.CHAR_NONE, ModuleType.Player, "Gets rid of fire faster.");
	public AntiHurtcam antihurtcam = new AntiHurtcam("AntiHurtcam", Keyboard.CHAR_NONE, ModuleType.Render, "Disables the camera from shaking.");
	public AntiVelocity antivelocity = new AntiVelocity("AntiVelocity", Keyboard.KEY_M, ModuleType.Combat, "Stops velocity events (Like knockback).");
	public AntiWeather antiweather = new AntiWeather("AntiWeather", Keyboard.CHAR_NONE, ModuleType.Render, "Stops weather rendering.");
	public Arraylist arraylist = new Arraylist("Arraylist", Keyboard.CHAR_NONE, ModuleType.Misc, "Shows enabled mods.");
	public Aura aura = new Aura("Aura", Keyboard.KEY_R, ModuleType.Combat, "Attacks entities around you.");
	public AutoArmor autoarmor = new AutoArmor("AutoArmor", Keyboard.KEY_O, ModuleType.Combat, "Automatically puts on armor.");
	public AutoBlock autoblock = new AutoBlock("AutoBlock", Keyboard.CHAR_NONE, ModuleType.Combat, "Automatically blocks with a sword.");
	public AutoBow autobow = new AutoBow("AutoBow", Keyboard.KEY_COMMA, ModuleType.Combat, "Shoots the bow.");
	public AutoEat autoeat = new AutoEat("AutoEat", Keyboard.CHAR_NONE, ModuleType.Player, "Eats food when hungry.");
	public AutoFarm autofarm = new AutoFarm("AutoFarm", Keyboard.KEY_I, ModuleType.World, "Breaks fully grown crops.");
	public AutoFish autofish = new AutoFish("AutoFish", Keyboard.CHAR_NONE, ModuleType.Player, "Fishes in water.");
	public AutoDisconnect autoquit = new AutoDisconnect("AutoQuit", Keyboard.CHAR_NONE, ModuleType.Combat, "Quits when low on health.");
	public AutoTool autotool = new AutoTool("AutoTool", Keyboard.KEY_J, ModuleType.World, "Chooses the best tool for the block.");
	public AutoWeapon autoweapon = new AutoWeapon("AutoWeapon", Keyboard.KEY_L, ModuleType.Combat, "Chooses a weapon when you click.");
	public Blink blink = new Blink("Blink", Keyboard.CHAR_NONE, ModuleType.Player, "Stops sending packets and resends when disabled.");
	public BlockOverlay blockoverlay = new BlockOverlay("BlockOverlay", Keyboard.CHAR_NONE, ModuleType.Render, "Draws a highlight around the selected block.");
	public BowAssist bowassist = new BowAssist("BowAssist", Keyboard.KEY_U, ModuleType.Combat, "Aims the bow at nearby enemies");
	public Build build = new Build("Build", Keyboard.KEY_B, ModuleType.World, "Builds things in one click. (Poles,walls)");
	public Bright bright = new Bright("Bright", Keyboard.KEY_B, ModuleType.Render, "Makes the world a brighter place. (Not metaphorically)");
	public ClickFriends clickfriends = new ClickFriends("ClickFriends", Keyboard.CHAR_NONE, ModuleType.Misc, "Lets  you middle click entities to friend them.");
	public Climb climb = new Climb("Climb", Keyboard.CHAR_NONE, ModuleType.Player, "Climbs up walls.");
	public ChestESP chest = new ChestESP("ChestESP", Keyboard.KEY_Y, ModuleType.Render, "Draws boxes around chests.");
	public CommandListener commands = new CommandListener("ChatComms", Keyboard.KEY_NONE, ModuleType.Misc, "Lets you use chat commands.");
	public Compass compass = new Compass("Compass", Keyboard.KEY_NONE, ModuleType.Render, "Draws a compass to your screen.");
	public Clock clock = new Clock("Clock", Keyboard.KEY_NONE, ModuleType.Render, "Draws a block to your screen.");
	public Criticals crits = new Criticals("Criticals", Keyboard.KEY_G, ModuleType.Combat, "Forces critical hits. (Best when Nocheat is enabled)");
	public ModDescriptions desc = new ModDescriptions("Descriptions", Keyboard.CHAR_NONE, ModuleType.Misc, "Shows module descriptions when hovered over.");
	public Dolphin dolphin = new Dolphin("Dolphin", Keyboard.CHAR_NONE, ModuleType.Player, "SONIC'S MY NAME SPEEDS MY NAME! GOTTA GO FAST!");
	public Drop drop = new Drop("Drop", Keyboard.CHAR_NONE, ModuleType.Player, "Drops inventory.");
	public Erratic erratic = new Erratic("Erratic", Keyboard.KEY_M, ModuleType.Combat, "Some stupid Dragon Ball Z shit.");
	public ESP esp = new ESP("ESP", Keyboard.KEY_J, ModuleType.Render, "Draws boxes around entities.");
	public Fastmine fastmine = new Fastmine("Fastmine", Keyboard.KEY_V, ModuleType.Player, "Mines blocks faster.");
	public Fastplace fastplace = new Fastplace("Fastplace", Keyboard.KEY_H, ModuleType.Player, "Places blocks faster.");
	public Flight flight = new Flight("Flight", Keyboard.KEY_F, ModuleType.Player, "Flies.");
	public Freecam freecam = new Freecam("Freecam", Keyboard.KEY_U, ModuleType.Player, "Walk around or noclip if flight is enabled.");
	public Freeze freeze = new Freeze("Freeze", Keyboard.CHAR_NONE, ModuleType.Combat, "Stop in your tracks and take less damage. (Buggy)");
	public Glide glide = new Glide("Glide", Keyboard.CHAR_NONE, ModuleType.Player, "Slowly fall.");
	public HorseJump horsejump = new HorseJump("HorseJump", Keyboard.CHAR_NONE, ModuleType.Player, "Jump when the horse gauge is full.");
	public InstaEat instaeat = new InstaEat("InstaEat", Keyboard.CHAR_NONE, ModuleType.Player, "Instantly eat food.");
	public Jesus jeezus = new Jesus("Jesus", Keyboard.CHAR_NONE, ModuleType.Player, "Main character of the series 'Return of the Jew'");
	public Keybinds keybinds = new Keybinds("Keybinds", Keyboard.CHAR_NONE, ModuleType.Misc, "Shows what keys are bound to mods.");
	public Legit legit = new Legit("Legit", Keyboard.CHAR_NONE, ModuleType.Misc, "Disabled in-game overlays (text).");
	public LightLevel lightlevel = new LightLevel("LightLevel", Keyboard.CHAR_NONE, ModuleType.Render, "Shows what blocks bad shit spawns on.");
	public Lockview lockview = new Lockview("Lockview", Keyboard.KEY_H, ModuleType.Combat, "Locks the camera on a target.");
	public NameProtect nameprotect = new NameProtect("NameProtect", Keyboard.CHAR_NONE, ModuleType.Render, "Replaces names (requires chat commands).");
	public Nametags nametags = new Nametags("Nametags", Keyboard.CHAR_NONE, ModuleType.Render, "Makes people's names bigger for those who have 60/20 vision.");
	public Nuker nuke = new Nuker("Nuker", Keyboard.KEY_N, ModuleType.World, "Destroys blocks around you (chat commands to modify) (Id/tick/click [Can be combined])");
	public NoCheat nocheat = new NoCheat("NoCheat", Keyboard.CHAR_NONE, ModuleType.Misc, "Makes modules more strict. Does not apply to every module.");
	public NoFall nofall = new NoFall("NoFall", Keyboard.KEY_N, ModuleType.Player, "Disables fall damage.");
	public NoRender norender = new NoRender("NoRender", Keyboard.CHAR_NONE, ModuleType.Render, "Stops large portion of rendering.");
	public NoSlowdown noslowdown = new NoSlowdown("NoSlowdown", Keyboard.CHAR_NONE, ModuleType.Player, "Ain't nothin gonna slow you down. Fuck cobwebs. Fuck water.");
	public PermanentTime permtime = new PermanentTime("PermanentTime", Keyboard.CHAR_NONE, ModuleType.World, "Shit broke.");
	public Pillager pillager = new Pillager("Looter", Keyboard.CHAR_NONE, ModuleType.Player, "Steals items from chests");
	public Phase phase = new Phase("Phase", Keyboard.CHAR_NONE, ModuleType.Player, "Glitches the fuck out with a rare chance of going through thin blocks.");
	// public Player player = new Player("Player", Keyboard.KEY_NONE,
	// ModuleType.Render,"");
	public Radar radar = new Radar("Radar", Keyboard.CHAR_NONE, ModuleType.World, "Draws a block radar.");
	public Regeneration regen = new Regeneration("Regen", Keyboard.KEY_Y, ModuleType.Combat, "Regenerates health faster (uses hunger faster)");
	public Replace replace = new Replace("Replace", Keyboard.CHAR_NONE, ModuleType.World, "Replaces blocks you break with whatever you have in your hand.");
	public Respawn respawn = new Respawn("Respawn", Keyboard.KEY_I, ModuleType.Combat, "Respawns instantly once you die.");
	public Search search = new Search("Search", Keyboard.KEY_H, ModuleType.Render, "Alternate to xray. Draws colored blocks around blocks.");
	public Soup soup = new Soup("Soup", Keyboard.KEY_F, ModuleType.Combat, "Eats soup on KitPvP servers.");
	public Sprint sprint = new Sprint("Sprint", Keyboard.KEY_G, ModuleType.Player, "Sprints for you.");
	public Sneak sneak = new Sneak("Sneak", Keyboard.KEY_Z, ModuleType.Player, "Sneaks for you.");
	public Step step = new Step("Step", Keyboard.CHAR_NONE, ModuleType.Player, "Steps up blocks.");
	public TorchPlacer torchplacer = new TorchPlacer("TorchPlacer", Keyboard.CHAR_NONE, ModuleType.Player, "Places torches when right clicking with any tool (If you have torches)");
	public Timer timer = new Timer("Timer", Keyboard.CHAR_NONE, ModuleType.World, "Speeds up the game.");
	public TPAccept tpaccept = new TPAccept("TPAccept", Keyboard.CHAR_NONE, ModuleType.World, "Accepts TPA's from friends.");
	public Tracer tracer = new Tracer("Tracers", Keyboard.KEY_I, ModuleType.Render, "Draws lines to entities (chat commands [mob/player]).");
	public Trail trail = new Trail("Trail", Keyboard.KEY_K, ModuleType.Render, "Draws a path behind you.");
	public Trajectories trajectories = new Trajectories("Trajectories", Keyboard.KEY_R, ModuleType.Render, "Shows where projectiles will land.");
	public TriggerClick triggerclick = new TriggerClick("TriggerClick", Keyboard.CHAR_NONE, ModuleType.Combat, "Clicks if you hover over an entity.");
	public Waypoints waypoints = new Waypoints("Waypoints", Keyboard.CHAR_NONE, ModuleType.Render, "Shit broke.");
	public Walk walk = new Walk("Walk", Keyboard.CHAR_NONE, ModuleType.Player, "Walks for your lazy fucking ass.");
	public Xray xray = new Xray("Xray", Keyboard.KEY_X, ModuleType.Render, "Shows ores and stuff. Customizable in settings file.");

	public Initlizer()
	{
		onLoad();
	}

	public void onLoad()
	{
		Log.write("Attempting to register hacks.");
		addHack(armor);
		addHack(antifire);
		addHack(antihurtcam);
		addHack(antivelocity);
		addHack(antiweather);
		addHack(arraylist);
		addHack(aura);
		addHack(autoarmor);
		addHack(autobow);
		addHack(autoblock);
		addHack(autoeat);
		addHack(autofarm);
		addHack(autofish);
		addHack(autoquit);
		addHack(autotool);
		addHack(autoweapon);
		addHack(blink);
		addHack(blockoverlay);
		addHack(bowassist);
		addHack(build);
		addHack(bright);
		addHack(climb);
		addHack(chest);
		addHack(compass);
		addHack(clock);
		addHack(crits);
		addHack(desc);
		addHack(dolphin);
		addHack(drop);
		addHack(erratic);
		addHack(esp);
		addHack(fastmine);
		addHack(fastplace);
		addHack(flight);
		addHack(freecam);
		addHack(freeze);
		addHack(glide);
		addHack(horsejump);
		addHack(instaeat);
		addHack(jeezus);
		addHack(keybinds);
		addHack(legit);
		addHack(lightlevel);
		addHack(lockview);
		addHack(nofall);
		addHack(norender);
		addHack(noslowdown);
		// addHack(permtime);
		addHack(pillager);
	 addHack(phase);
		addHack(nameprotect);
		addHack(nametags);
		addHack(nuke);
		// addHack(player);
		addHack(radar);
		addHack(regen);
		addHack(replace);
		addHack(respawn);
		addHack(search);
		addHack(soup);
		addHack(sprint);
		addHack(sneak);
		addHack(step);
		addHack(torchplacer);
		addHack(timer);
		addHack(tpaccept);
		addHack(tracer);
		addHack(trail);
		addHack(trajectories);
		addHack(triggerclick);
		// addHack(waypoints);
		addHack(walk);
		addHack(xray);
		addHack(commands);
		addHack(nocheat);
		addHack(clickfriends);
		Log.write("Hack list populated with " + hackList.size() + " modules!");
	}

	private void addHack(Module hack)
	{
		hackList.add(hack);
		final File saveFile = new File("Aeon" + File.separator + "Hacks" + File.separator + hack.getClass().getSimpleName() + ".txt");
		if (saveFile.exists())
		{
			hack.loadSettings();
		} else
		{
			hack.writeSettings();
		}
	}
}
