package me.lordpankake.aeon.core;

import java.awt.Font;

public class AeonSettings
{
	public static int proto = 5;
	public static String mcVersion = "1.7.10";
	public static String version = "1.064";
	public static Font font = new Font("Segoe UI", Font.PLAIN, 20);
	public static float fontSize = 20;
}
