package me.lordpankake.aeon.core;

import java.sql.Timestamp;

public class Log
{
	public static void write(String s)
	{
		final java.util.Date date = new java.util.Date();
		final String message = "[Aeon][" + new Timestamp(date.getTime()) + "] " + s;
		System.out.println(message);
	}
}
