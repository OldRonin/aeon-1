package me.lordpankake.aeon.util;

import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;

public class InventoryUtils extends UtilBase
{
	public Slot getWearingArmor(int armorType)
	{
		return mc.thePlayer.inventoryContainer.getSlot(5 + armorType);
	}

	public void clickSlot(int slot, int mouseButton, boolean shiftClick)
	{
		mc.playerController.windowClick(mc.thePlayer.inventoryContainer.windowId, slot, mouseButton, shiftClick ? 1 : 0, mc.thePlayer);
	}

	public int findHotbarItem(int itemID)
	{
		for (int o = 0; o < 9; o++)
		{
			final ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
			if (item != null && Item.getIdFromItem(item.getItem()) == itemID)
			{
				return o;
			}
		}
		return -1;
	}

	public int findHotbarFood()
	{
		for (int o = 0; o < 9; o++)
		{
			final ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
			if (item != null && item.getItem() instanceof ItemFood)
			{
				return o;
			}
		}
		return -1;
	}

	public int findInventoryItem(int itemID)
	{
		for (int o = 9; o < 36; o++)
		{
			if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack())
			{
				final ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
				if (item != null && Item.getIdFromItem(item.getItem()) == itemID)
				{
					return o;
				}
			}
		}
		return -1;
	}

	public int findAvailableSlotInventory(int[] itemIDs)
	{
		for (int o = 36; o < 45; o++)
		{
			final ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
			if (item == null)
			{
				return o;
			}
			for (final int i : itemIDs)
			{
				if (Item.getIdFromItem(item.getItem()) == i)
				{
					return o;
				}
			}
		}
		return -1;
	}

	public int getItemCountHotbar(int itemID)
	{
		int count = 0;
		for (int o = 0; o < 9; o++)
		{
			final ItemStack item = mc.thePlayer.inventory.getStackInSlot(o);
			if (item != null && Item.getIdFromItem(item.getItem()) == itemID)
			{
				count += item.stackSize;
			}
		}
		return count;
	}

	public int getItemCountInventory(int itemID)
	{
		int count = 0;
		for (int o = 9; o < 36; o++)
		{
			if (mc.thePlayer.inventoryContainer.getSlot(o).getHasStack())
			{
				final ItemStack item = mc.thePlayer.inventoryContainer.getSlot(o).getStack();
				if (item != null && Item.getIdFromItem(item.getItem()) == itemID)
				{
					count += item.stackSize;
				}
			}
		}
		return count;
	}

	public static boolean shouldStackItem(Container inventoryContainer, Item item)
	{
		return stacksOfItemWithRoom(inventoryContainer, item) > 1;
	}

	public static int stacksOfItemWithRoom(Container inventoryContainer, Item item)
	{
		final int maxStackSize = item.getItemStackLimit();
		final int itemId = Item.getIdFromItem(item);
		int stacksWithRoom = 0;
		for (int i = 0; i <= inventoryContainer.inventorySlots.size() - 1; i++)
		{
			final ItemStack checkedStack = inventoryContainer.getSlot(i).getStack();
			if (checkedStack == null)
			{
				continue;
			}
			if (Item.getIdFromItem(checkedStack.getItem()) == itemId && checkedStack.stackSize < maxStackSize)
			{
				stacksWithRoom++;
			}
		}
		return stacksWithRoom;
	}

	public static boolean isAreaFull(Container inventoryContainer, int startSlot, int endSlot)
	{
		if (startSlot < 0 || startSlot > inventoryContainer.inventorySlots.size() - 1 || endSlot > inventoryContainer.inventorySlots.size() - 1 || startSlot > endSlot)
		{
			return true;
		}
		for (int i = startSlot; i <= endSlot; i++)
		{
			final ItemStack checkedStack = inventoryContainer.getSlot(i).getStack();
			if (checkedStack == null)
			{
				return false;
			}
		}
		return true;
	}

	public void swap(int itemIndex)
	{
		mc.thePlayer.inventory.currentItem = itemIndex;
	}

	public void useItem(ItemStack item)
	{
		mc.playerController.sendUseItem(mc.thePlayer, mc.theWorld, item);
	}

	public static int smallestStackInRange(Container inventoryContainer, Item item, int start, int end)
	{
		if (start < 0 || start > inventoryContainer.inventorySlots.size() - 1 || end > inventoryContainer.inventorySlots.size() - 1 || start > end)
		{
			return -1;
		}
		final int maxStackSize = item.getItemStackLimit();
		final int itemId = Item.getIdFromItem(item);
		int smallestStackSize = Integer.MAX_VALUE;
		int smallestSlot = -1;
		for (int i = start; i <= end; i++)
		{
			final ItemStack checkedStack = inventoryContainer.getSlot(i).getStack();
			if (checkedStack == null)
			{
				continue;
			}
			if (Item.getIdFromItem(checkedStack.getItem()) == itemId && checkedStack.stackSize < smallestStackSize)
			{
				smallestStackSize = checkedStack.stackSize;
				smallestSlot = i;
			}
		}
		return smallestSlot;
	}

	public static void clickSlot(int slot, boolean shiftClick)
	{
		mc.playerController.windowClick(mc.thePlayer.inventoryContainer.windowId, slot, 0, shiftClick ? 1 : 0, mc.thePlayer);
	}

	public void doubleClickSlot(int slot)
	{
		mc.playerController.windowClick(mc.thePlayer.inventoryContainer.windowId, slot, 0, 0, mc.thePlayer);
		mc.playerController.windowClick(mc.thePlayer.inventoryContainer.windowId, slot, 0, 6, mc.thePlayer);
		mc.playerController.windowClick(mc.thePlayer.inventoryContainer.windowId, slot, 0, 0, mc.thePlayer);
	}

	public static boolean areaHasItem(Container inventoryContainer, Item item, int startSlot, int endSlot)
	{
		for (int i = startSlot; i <= endSlot; i++)
		{
			final ItemStack checkedStack = inventoryContainer.getSlot(i).getStack();
			if (checkedStack == null)
			{
				continue;
			}
			if (Item.getIdFromItem(checkedStack.getItem()) == Item.getIdFromItem(item))
			{
				return true;
			}
		}
		return false;
	}

	public static int findFirstInstanceOfItemInRange(Container inventoryContainer, Item item, int startSlot, int stopSlot)
	{
		if (startSlot < 0 || startSlot > inventoryContainer.inventorySlots.size() - 1 || stopSlot > inventoryContainer.inventorySlots.size() - 1 || startSlot > stopSlot)
		{
			return -1;
		}
		for (int i = startSlot; i <= stopSlot; i++)
		{
			final ItemStack checkedStack = inventoryContainer.getSlot(i).getStack();
			if (checkedStack == null)
			{
				continue;
			}
			if (Item.getIdFromItem(checkedStack.getItem()) == Item.getIdFromItem(item))
			{
				return i;
			}
		}
		return -1;
	}
}
