package me.lordpankake.aeon.util;

import java.util.Iterator;
import me.lordpankake.aeon.util.other.BlockPlaceholder;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.util.MathHelper;

public class BlockItemUtils extends UtilBase
{
	private final Block[] blocks = new Block[4096];
	private final Item[] items = new Item[32000];

	public BlockItemUtils()
	{
		setupBlocks();
		setupItems();
	}

	public void setupBlocks()
	{
		for (final Iterator localIterator = Block.blockRegistry.iterator(); localIterator.hasNext();)
		{
			final Object o = localIterator.next();
			final Block block = (Block) o;
			if (block != null)
			{
				this.blocks[Block.getIdFromBlock(block)] = block;
			}
		}
	}

	public void setupItems()
	{
		for (final Iterator localIterator = Item.itemRegistry.iterator(); localIterator.hasNext();)
		{
			final Object o = localIterator.next();
			final Item i = (Item) o;
			if (i != null)
			{
				this.items[Item.getIdFromItem(i)] = i;
			}
		}
	}

	public void faceBlock(double posX, double posY, double posZ)
	{
		final double diffX = posX - mc.thePlayer.posX;
		final double diffZ = posZ - mc.thePlayer.posZ;
		final double diffY = posY - (mc.thePlayer.posY + mc.thePlayer.getEyeHeight());
		final double dist = MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
		final float yaw = (float) (Math.atan2(diffZ, diffX) * 180.0D / 3.141592653589793D) - 90.0F;
		final float pitch = (float) -(Math.atan2(diffY, dist) * 180.0D / 3.141592653589793D);
		mc.thePlayer.rotationPitch += MathHelper.wrapAngleTo180_float(pitch - mc.thePlayer.rotationPitch);
		mc.thePlayer.rotationYaw += MathHelper.wrapAngleTo180_float(yaw - mc.thePlayer.rotationYaw);
	}

	public Block getBlock(int id)
	{
		return this.blocks[id];
	}

	public int getBlockID(Block block)
	{
		for (int i = 0; i < this.blocks.length; i++)
		{
			if (this.blocks[i] != null && this.blocks[i] == block)
			{
				return i;
			}
		}
		return 0;
	}

	public int getItemID(Item item)
	{
		for (int i = 0; i < this.items.length; i++)
		{
			if (this.items[i] != null && this.items[i] == item)
			{
				return i;
			}
		}
		return 0;
	}

	public double getDistance(BlockPlaceholder blockVector)
	{
		return getDistance(blockVector.getX(), blockVector.getX(), blockVector.getX());
	}

	private double getDistance(double x, double y, double z)
	{
		final float xDiff = (float) (mc.thePlayer.posX - x);
		final float yDiff = (float) (mc.thePlayer.posY - y);
		final float zDiff = (float) (mc.thePlayer.posZ - z);
		return MathHelper.sqrt_float(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
	}

	public boolean isBlockSolid(int x, int y, int z)
	{
		if (mc.theWorld.getBlock(x, y, z).getMaterial() == Material.air)
		{
			return false;
		}
		return true;
	}

	public BlockPlaceholder findClosestBlock(int radius)
	{
		BlockPlaceholder closestBlock = null;
		for (int i = radius; i >= -radius; i--)
		{
			for (int j = -radius; j <= radius; j++)
			{
				for (int k = radius; k >= -radius; k--)
				{
					final int possibleX = (int) (mc.thePlayer.posX + i);
					final int possibleY = (int) (mc.thePlayer.posY + j);
					final int possibleZ = (int) (mc.thePlayer.posZ + k);
					final Block block = mc.theWorld.getBlock(possibleX, possibleY, possibleZ);
					final BlockPlaceholder hrvsBlock = new BlockPlaceholder(possibleX, possibleY, possibleZ, mc.theWorld.getBlock(possibleX, possibleY, possibleZ), mc.theWorld.getBlock(possibleX, possibleY, possibleZ).textureName);
					if (block != null && canReach(hrvsBlock, radius))
					{
						if (closestBlock == null)
						{
							closestBlock = hrvsBlock;
						} else if (mc.thePlayer.getDistance(hrvsBlock.getX(), hrvsBlock.getY(), hrvsBlock.getZ()) < mc.thePlayer.getDistance(closestBlock.getX(), closestBlock.getY(), closestBlock.getZ()))
						{
							closestBlock = hrvsBlock;
						}
					}
				}
			}
		}
		return closestBlock;
	}

	public boolean canReach(BlockPlaceholder hrvBlock, float distance)
	{
		return canReach(hrvBlock.getX(), hrvBlock.getY(), hrvBlock.getZ(), distance);
	}

	public boolean canReach(double posX, double posY, double posZ, float distance)
	{
		final double blockDistance = getDistance(posX, posY, posZ);
		return blockDistance < distance && blockDistance > -distance;
	}

	public void breakBlock(BlockPlaceholder block, int mode, int side)
	{
		mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(mode, block.getX(), block.getY(), block.getZ(), side));
	}
}
