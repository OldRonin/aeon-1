package me.lordpankake.aeon.util;

import java.util.ArrayList;

public class StringUtils
{
	public String getAfter(String text, String splitter, int index)
	{
		final String[] words = text.split(splitter);
		if (words.length < index)
		{
			return null;
		}
		int splitIndex = 0;
		for (int i = 0; i < index; i++)
		{
			splitIndex += words[i].length() + 1;
		}
		return text.substring(splitIndex);
	}

	public String getBefore(String text, String splitter, int index)
	{
		final String[] words = text.split(splitter);
		if (words.length < index)
		{
			return null;
		}
		int splitIndex = 0;
		for (int i = 0; i < index; i++)
		{
			splitIndex += words[i].length() + 1;
		}
		return text.substring(0, splitIndex);
	}

	public static boolean isInteger(String text)
	{
		try
		{
			Integer.parseInt(text);
			return true;
		} catch (final Exception e)
		{
		}
		return false;
	}

	public static boolean isDouble(String text)
	{
		try
		{
			Double.parseDouble(text);
			return true;
		} catch (final Exception e)
		{
		}
		return false;
	}

	public static boolean isFloat(String text)
	{
		try
		{
			Float.parseFloat(text);
			return true;
		} catch (final Exception e)
		{
		}
		return false;
	}

	public static boolean isBoolean(String text)
	{
		try
		{
			Boolean.parseBoolean(text);
			if (text.toLowerCase().contains("true") || text.toLowerCase().contains("false"))
			{
				return true;
			}
		} catch (final Exception e)
		{
		}
		return false;
	}

	public static boolean isArraylist(String text)
	{
		if (text.startsWith("[") && text.endsWith("]"))
		{
			return true;
		}
		return false;
	}

	public static ArrayList getArraylist(String text)
	{
		final ArrayList arr = new ArrayList();
		if (isArraylist(text))
		{
			final String items = text.substring(1, text.length() - 1);
			final String items_arr[] = items.split(",");
			for (final String s : items_arr)
			{
				arr.add(s.trim());
			}
			return arr;
		}
		return null;
	}
}
