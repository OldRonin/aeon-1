package me.lordpankake.aeon.util;
public class TimerUtils
{
	private long previousTime;

	public TimerUtils()
	{
		this.previousTime = -1L;
	}

	public boolean check(float milliseconds)
	{
		return getCurrentTime() - this.previousTime >= milliseconds;
	}

	public void reset()
	{
		this.previousTime = getCurrentTime();
	}

	public short convert(float perSecond)
	{
		return (short) (int) (1000.0F / perSecond);
	}

	public long get()
	{
		return this.previousTime;
	}

	public long getCurrentTime()
	{
		return System.currentTimeMillis();
	}
}