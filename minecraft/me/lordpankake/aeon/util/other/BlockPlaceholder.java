package me.lordpankake.aeon.util.other;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;

public class BlockPlaceholder
{
	private final int x;
	private final int y;
	private final int z;
	public String name;
	private final Block block;

	public BlockPlaceholder(int x, int y, int z, Block block, String name)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.block = block;
		this.name = name;
	}

	public Block getBlock()
	{
		return this.block;
	}
	
	public boolean isBlockNull()
	{
		return (Minecraft.getMinecraft().theWorld.getBlock(this.x, this.y, this.z).textureName.contains("air") &&  Minecraft.getMinecraft().theWorld.getBlock(this.x, this.y, this.z).textureName.length() == 3);
	}

	public int getX()
	{
		return this.x;
	}

	public int getY()
	{
		return this.y;
	}

	public int getZ()
	{
		return this.z;
	}
}
