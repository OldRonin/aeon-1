package me.lordpankake.aeon.util;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ReflectionUtils extends UtilBase
{
	public static void findFields(Class c)
	{
		final Field[] fields = c.getDeclaredFields();
		for (int i = 0; i < fields.length; i++)
		{
			System.out.println("Field : " + c.getSimpleName() + " " + i + " : " + fields[i].getType() + " : " + fields[i].getName());
		}
	}

	public static void findMethods(Class c)
	{
		final Method[] method = c.getDeclaredMethods();
		for (int i = 0; i < method.length; i++)
		{
			System.out.println("Field : " + c.getSimpleName() + " " + i + " : " + method[i].getReturnType() + " : " + method[i].getName());
		}
	}

	public static void setField(Class c, Object o, String s, Object val)
	{
		final Field[] fields = c.getDeclaredFields();
		for (int i = 0; i < fields.length; i++)
		{
			if (fields[i].getName().equals(s))
			{
				System.out.println("Fix relflection usage: Use \"" + i + "\" instead of \"" + s + "\"!");
				setField(c, o, i, val);
				return;
			}
		}
		System.out.println("Fix relflection usage: No such field: \"" + s + "\"!");
	}

	public static Class[] getClassesInPackage(String targetPackage)
	{
		final List<Class> classList = new ArrayList<Class>();
		final URL packageResource = Thread.currentThread().getContextClassLoader().getResource(targetPackage.replace(".", "/").trim());
		if (packageResource == null)
		{
			System.out.println("Could not create resource for package " + targetPackage);
			return null;
		}
		final File packageDirectory = new File(packageResource.getFile());
		for (final String filename : packageDirectory.list())
		{
			if (filename.endsWith(".class"))
			{
				final String className = targetPackage + "." + filename;
				try
				{
					classList.add(Class.forName(className.replace(".class", "")));
				} catch (final ClassNotFoundException e)
				{
					System.out.println("Error attempting to load classes from " + targetPackage);
					e.printStackTrace();
				}
			}
		}
		return classList.toArray(new Class[classList.size()]);
	}

	public static int getFieldInt(Class c, Object o, int n)
	{
		final Field f = c.getDeclaredFields()[n];
		try
		{
			return f.getInt(n);
		} catch (final IllegalArgumentException e)
		{
			e.printStackTrace();
		} catch (final IllegalAccessException e)
		{
			e.printStackTrace();
		}
		return 0;
	}

	public static int getFieldIntS(Class c, Object o, String s)
	{
		try
		{
			final Field f = c.getDeclaredField(s);
			return f.getInt(s);
		} catch (final IllegalArgumentException e)
		{
			e.printStackTrace();
		} catch (final IllegalAccessException e)
		{
			e.printStackTrace();
		} catch (final NoSuchFieldException e)
		{
			e.printStackTrace();
		} catch (final SecurityException e)
		{
			e.printStackTrace();
		}
		return 0;
	}

	public static void setField(Class c, Object o, int n, Object val)
	{
		try
		{
			final Field f = c.getDeclaredFields()[n];
			f.setAccessible(true);
			final Field modifiers = f.getClass().getDeclaredField("modifiers");
			modifiers.setAccessible(true);
			modifiers.setInt(f, f.getModifiers() & ~Modifier.FINAL);
			f.set(o, val);
		} catch (final Exception ex)
		{
			System.out.println(ex);
		}
	}
}
